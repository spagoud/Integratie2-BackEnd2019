package be.kdg.integratie2backend2019.security_test.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

// the term “test suite” means all tests run in the same JVMM
// for example, all tests run from an Ant, Maven, or Gradle
// build for a given project or module.
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ChangePasswordIntegrationTest.class,
        TokenExpirationIntegrationTest.class,
        RegistrationControllerIntegrationTest.class,
        GetLoggedUsersIntegrationTest.class,
        UserServiceIntegrationTest.class,
        UserIntegrationTest.class,
        SpringSecurityRolesIntegrationTest.class,
})
public class IntegrationSuite {

}
