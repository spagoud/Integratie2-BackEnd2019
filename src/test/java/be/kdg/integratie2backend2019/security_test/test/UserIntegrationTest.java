package be.kdg.integratie2backend2019.security_test.test;

import be.kdg.integratie2backend2019.Integratie2Backend2019Application;
import be.kdg.integratie2backend2019.security.config.ServiceConfig;
import be.kdg.integratie2backend2019.security.persistence.dao.UserRepository;
import be.kdg.integratie2backend2019.security.persistence.dao.VerificationTokenRepository;
import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.security.persistence.model.VerificationToken;
import be.kdg.integratie2backend2019.security.validation.EmailExistsException;
import be.kdg.integratie2backend2019.security_test.config.TestDbConfig;
import be.kdg.integratie2backend2019.security_test.config.TestIntegrationConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.UUID;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@TestPropertySource(
        properties = {"management.health.mail.enabled=false"})
@SpringBootTest(classes = {Integratie2Backend2019Application.class, TestDbConfig.class, ServiceConfig.class, TestIntegrationConfig.class})
@Transactional
public class UserIntegrationTest {

    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @MockBean
    private JavaMailSender javaMailSender;

    private Long tokenId;
    private Long userId;

    //

    @Before
    public void givenUserAndVerificationToken() throws EmailExistsException {
        User user = new User();
        user.setEmail("test@example.com");
        user.setPassword("SecretPassword");
        user.setFirstName("First");
        user.setLastName("Last");
        entityManager.persist(user);

        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken(token, user);
        entityManager.persist(verificationToken);

        entityManager.flush();
        entityManager.clear();

        tokenId = verificationToken.getId();
        userId = user.getId();
    }

    @After
    public void flushAfter() {
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void whenContextLoad_thenCorrect() {
        assertTrue(userRepository.count() > 0);
        assertTrue(tokenRepository.count() > 0);
    }

    @Test
    @Ignore("needs to go through the service and get transactional semantics")
    public void whenRemovingUser_thenFkViolationException() {
        userRepository.deleteById(userId);
    }

    @Test
    public void whenRemovingTokenThenUser_thenCorrect() {
        tokenRepository.deleteById(tokenId);
        userRepository.deleteById(userId);
    }

}

