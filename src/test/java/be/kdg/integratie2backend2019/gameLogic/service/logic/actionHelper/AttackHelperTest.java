package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.Integratie2Backend2019Application;
import be.kdg.integratie2backend2019.broadcast.messages.AttackMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.dao.GameMonsterRepository;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import be.kdg.integratie2backend2019.game.persistence.model.items.Weapon;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.AttackAction;
import be.kdg.integratie2backend2019.gameLogic.service.WeaponService;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestGame;
import be.kdg.integratie2backend2019.gameLogic.web.errors.NotInRangeException;
import be.kdg.integratie2backend2019.security_test.config.TestDbConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest(classes = {Integratie2Backend2019Application.class, TestDbConfig.class})
@RunWith(SpringRunner.class)
public class AttackHelperTest {

    @Autowired
    TestGame testGame;

    @Autowired
    AttackHelper attackHelper;

    @MockBean
    BroadcastService broadcastService;

    @MockBean
    WeaponService weaponService;

    @MockBean
    GameMonsterRepository gameMonsterRepository;


    private Game game;
    private GameHero hero;
    private GameLobby lobby;
    private AttackAction attackAction;
    private GameMonster gameMonster;

    @Before
    public void setUp() throws Exception {
        game = testGame.getGame();
        List<GameHero> heroes = game.getHeroes();
        hero = heroes.get(0);
        hero.setPosition(new Tile((long) 19,TileType.FLOOR,  19,3,  false,  false, TrapType.NONE, false));
        hero.getEquipment().getWeapon().setId((long)1);
        lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        gameMonster = new GameMonster(new Monster("", "Goblin", "", 1, 5, 100, new ArrayList<>(), new ArrayList<>(), 5));
        gameMonster.setId((long) 1);
        attackAction = new AttackAction(hero.getHeroName(),(long)1);
        attackAction.setItemId((long)1);
        game.getVisibleRooms().get(0).setGameMonsters(new ArrayList<>(Arrays.asList(gameMonster)));
    }

    @Test
    public void performAttack_succes() {
        gameMonster.setPosition(new Tile((long) 18,TileType.FLOOR,  18,3,  false,  false, TrapType.NONE, false));
        when(weaponService.getWeapon((long) 1)).thenReturn((Weapon) hero.getEquipment().getWeapon());
        when(gameMonsterRepository.findById((long)1)).thenReturn(Optional.of(gameMonster));
        attackHelper.performAttack(attackAction,(long) 1, game);
        verify(broadcastService, times(1)).sendEvent(eq(lobby.getId()), any(AttackMessage.class));
    }

    @Test(expected = NotInRangeException.class)
    public void performAttack_fail(){
        gameMonster.setPosition(new Tile((long) 17,TileType.LEFT,  17,3,  false,  false, TrapType.NONE, false));
        when(weaponService.getWeapon((long) 1)).thenReturn((Weapon) hero.getEquipment().getWeapon());
        when(gameMonsterRepository.findById((long)1)).thenReturn(Optional.of(gameMonster));
        attackHelper.performAttack(attackAction,(long) 1, game);
    }
}