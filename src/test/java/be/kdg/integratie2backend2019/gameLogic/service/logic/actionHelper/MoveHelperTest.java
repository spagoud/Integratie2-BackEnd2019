package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.Integratie2Backend2019Application;
import be.kdg.integratie2backend2019.broadcast.messages.MoveMessage;
import be.kdg.integratie2backend2019.broadcast.messages.PotionMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.MoveAction;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestGame;
import be.kdg.integratie2backend2019.security_test.config.TestDbConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {Integratie2Backend2019Application.class, TestDbConfig.class})
@RunWith(SpringRunner.class)
public class MoveHelperTest {

    @Autowired
    MoveHelper moveHelper;

    @Autowired
    TestGame testGame;

    @MockBean
    BroadcastService broadcastService;

    private Game game;
    private GameHero hero;
    private GameLobby lobby;
    private MoveAction moveAction;
    private List<Tile> path;
    private GameMonster gameMonster;


    @Before
    public void setUp() throws Exception {
        game = testGame.getGame();
        lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        path = new ArrayList<>(Arrays.asList(
                new Tile((long) 22,TileType.BOTTOMLEFT,  17,4,  false,  false, TrapType.NONE, false),
                new Tile((long) 23,TileType.BOTTOM,  18,4,  false,  false, TrapType.NONE, false),
                new Tile((long) 24,TileType.BOTTOM,  19,4,  false,  false, TrapType.NONE, false),
                new Tile((long) 25, TileType.BOTTOM,  20,4,  false,  false, TrapType.NONE, true)
        ));
    }

    @Test
    public void moveCharacter_hero() {
        List<GameHero> heroes = game.getHeroes();
        hero = heroes.get(0);
        hero.setPosition(new Tile((long) 25, TileType.BOTTOM,  20,4,  false,  false, TrapType.NONE, true));
        moveAction = new MoveAction(hero.getHeroName(), path);
        moveHelper.moveCharacter(moveAction,lobby.getId(),game);
        verify(broadcastService, times(1)).sendEvent(eq(lobby.getId()), any(MoveMessage.class));
        assertEquals(path.get(path.size()-1),hero.getPosition());
    }

    @Test
    public void moveCharacter_monster(){
        gameMonster = new GameMonster(new Monster("", "Goblin", "", 1, 5, 100, new ArrayList<>(), new ArrayList<>(), 5));
        gameMonster.setId((long) 1);
        gameMonster.setPosition(new Tile((long) 25, TileType.BOTTOM,  20,4,  false,  false, TrapType.NONE, true));
        game.getVisibleRooms().get(0).setGameMonsters(new ArrayList<>(Arrays.asList(gameMonster)));
        moveAction = new MoveAction(gameMonster.getId().toString(), path,true);
        moveHelper.moveCharacter(moveAction,lobby.getId(),game);
        verify(broadcastService, times(1)).sendEvent(eq(lobby.getId()), any(MoveMessage.class));
        assertEquals(path.get(path.size()-1),gameMonster.getPosition());
    }
}