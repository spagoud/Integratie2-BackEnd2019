package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.Integratie2Backend2019Application;
import be.kdg.integratie2backend2019.broadcast.messages.EquipMessage;
import be.kdg.integratie2backend2019.broadcast.messages.PotionMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.items.Potion;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.PotionAction;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestGame;
import be.kdg.integratie2backend2019.security_test.config.TestDbConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {Integratie2Backend2019Application.class, TestDbConfig.class})
@RunWith(SpringRunner.class)
public class PotionHelperTest {

    @Autowired
    PotionHelper potionHelper;

    @Autowired
    TestGame testGame;

    @MockBean
    BroadcastService broadcastService;

    private Game game;
    private GameHero hero;
    private GameLobby lobby;
    private PotionAction potionAction;



    @Before
    public void setUp() throws Exception {
        game = testGame.getGame();
        List<GameHero> heroes = game.getHeroes();
        hero = heroes.get(0);
        hero.setHealthPoints(4);
        lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        hero.setItemList(new ArrayList<>(Arrays.asList(new Potion("Health Potion", "neen","",1,"Potion", "HP +1", 1))));
        potionAction = new PotionAction(hero.getHeroName(), (Potion) hero.getItemList().get(0));
    }

    @Test
    public void usePotion() {
        assertEquals(4,hero.getHealthPoints());
        potionHelper.usePotion(potionAction, (long) 1, game);
        verify(broadcastService, times(1)).sendEvent(eq(lobby.getId()), any(PotionMessage.class));
        assertEquals(5,hero.getHealthPoints() );
    }
}