package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.Integratie2Backend2019Application;
import be.kdg.integratie2backend2019.broadcast.messages.DoorMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.DoorAction;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestGame;
import be.kdg.integratie2backend2019.security_test.config.TestDbConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {Integratie2Backend2019Application.class, TestDbConfig.class})
@RunWith(SpringRunner.class)
public class DoorHelperTest {

    @Autowired
    DoorHelper doorHelper;

    @Autowired
    TestGame testGame;

    @MockBean
    private BroadcastService broadcastService;
    @MockBean
    private GameLobbyService gameLobbyService;

    private Game game;
    private GameHero hero;
    private GameLobby lobby;
    private DoorAction doorAction;


    @Before
    public void setUp() throws Exception {
        game = testGame.getGame();
        List<GameHero> heroes = game.getHeroes();
        hero = heroes.get(0);
        hero.setPosition(new Tile((long) 25,TileType.BOTTOM,  20,4,  false,  false, TrapType.NONE, true));
        lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        doorAction = new DoorAction(hero.getHeroName(),lobby.getGame().getVisibleRooms().get(0).getDoors().get(0));
    }

    @Test
    public void openDoor() {
        doorHelper.openDoor(doorAction,lobby.getId(),game);
        when(gameLobbyService.loadGameLobby(lobby.getId())).thenReturn(lobby);
        verify(broadcastService, times(1)).sendEvent(eq(lobby.getId()), any(DoorMessage.class));
        assertTrue(lobby.getGame().getVisibleRooms().get(0).getDoors().get(0).isEnabled());
    }
}
