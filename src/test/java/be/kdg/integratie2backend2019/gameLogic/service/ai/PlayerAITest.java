package be.kdg.integratie2backend2019.gameLogic.service.ai;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestLobby;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayerAITest {

    @Autowired
    private TestLobby testLobby;
    private GameLobby gameLobby;

    @Before
    public void setUp(){
        gameLobby = testLobby.getTestLobby();
        Game game = gameLobby.getGame();
        game.setHeroes(new ArrayList<>());
        Room room = game.getDungeonRooms().get(1);
        GameMonster gameMonster = new GameMonster(new Monster("", "Goblin", "", 1, 5, 100, new ArrayList<>(), new ArrayList<>(), 5));
        room.setGameMonsters(new ArrayList<>(Arrays.asList(gameMonster)));
        game.addVisibleRoom(room);
        gameLobby.setGame(game);
    }

    @Test
    public void performTurn() {
    }
}
