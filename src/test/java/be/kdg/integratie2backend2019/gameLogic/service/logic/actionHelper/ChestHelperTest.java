package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.Integratie2Backend2019Application;
import be.kdg.integratie2backend2019.broadcast.messages.ChestMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.ChestAction;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestGame;
import be.kdg.integratie2backend2019.security_test.config.TestDbConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {Integratie2Backend2019Application.class, TestDbConfig.class})
@RunWith(SpringRunner.class)
public class ChestHelperTest {

    @Autowired
    TestGame testGame;

    @Autowired
    ChestHelper chestHelper;

    @MockBean
    BroadcastService broadcastService;

    private Game game;
    private GameHero hero;
    private ChestAction chestAction;
    private GameLobby lobby;
    private Tile tile;


    @Before
    public void setUp() throws Exception {
        game = testGame.getGame();
        List<GameHero> heroes = game.getHeroes();
        hero = heroes.get(0);
        tile = new Tile((long) 54, TileType.BOTTOM, 19, 10, false, true, TrapType.NONE, false);
        hero.setPosition(tile);
        lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        chestAction = new ChestAction(hero.getHeroName(), tile, game.getAvailableTreasures().get(0));
    }

    @Test
    public void openChest() {
        chestHelper.openChest(chestAction, (long) 1, game);
        verify(broadcastService, times(1)).sendEvent(eq(lobby.getId()), any(ChestMessage.class));
        assertFalse(hero.getPosition().isChest());
    }
}