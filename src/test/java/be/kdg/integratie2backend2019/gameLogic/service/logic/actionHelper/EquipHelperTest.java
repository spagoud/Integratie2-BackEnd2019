package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.Integratie2Backend2019Application;
import be.kdg.integratie2backend2019.broadcast.messages.EquipMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Equipment;
import be.kdg.integratie2backend2019.game.persistence.model.items.Item;
import be.kdg.integratie2backend2019.game.persistence.model.items.Weapon;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.EquipAction;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestGame;
import be.kdg.integratie2backend2019.security_test.config.TestDbConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {Integratie2Backend2019Application.class, TestDbConfig.class})
@RunWith(SpringRunner.class)
public class EquipHelperTest {

    @Autowired
    EquipHelper equipHelper;

    @Autowired
    TestGame testGame;

    @MockBean
    BroadcastService broadcastService;

    private Game game;
    private GameHero hero;
    private GameLobby lobby;
    private EquipAction equipAction;
    private Weapon weapon;


    @Before
    public void setUp() throws Exception {
        game = testGame.getGame();
        List<GameHero> heroes = game.getHeroes();
        hero = heroes.get(0);
        hero.getEquipment().getWeapon().setId((long)1);
        lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        weapon = new Weapon("Hammer of liberty",  "This weapon has a granite core and is made by dwarfs deep underground.", "", 1,false,"Weapon", false, null, null);
        weapon.setId((long) 2);
        hero.setItemList(new ArrayList<>(Arrays.asList(weapon)));
        equipAction = new EquipAction(hero.getItemList().get(0).getId(),hero.getHeroName());
    }

    @Test
    public void equipItem() {
        assertEquals((long) 1,(long) hero.getEquipment().getWeapon().getId());
        equipHelper.equipItem(equipAction, lobby.getId(),game);
        verify(broadcastService, times(1)).sendEvent(eq(lobby.getId()), any(EquipMessage.class));
        assertEquals((long) 2,(long) hero.getEquipment().getWeapon().getId());
    }
}