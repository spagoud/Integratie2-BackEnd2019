package be.kdg.integratie2backend2019.gameLogic.service.ai.movementAlgorithm;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.gameLogic.service.MovementService;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestAIGame;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestGame;
import org.checkerframework.common.value.qual.IntRangeFromGTENegativeOne;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.Id;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AlgorithmHandlerTest {

    @Autowired
    TestAIGame testAIGame;
    @Autowired
    AlgorithmHandler algorithmHandler;
    @Autowired
    MovementService movementService;
    @MockBean
    GameLobbyService gameLobbyService;

    @Test
    // @Ignore("Weird bug on remote")
    public void executePlayer_withMonsters_inRange_withOneLowestHealth() {
        Game game = testAIGame.getAIGame(true, true);
        List<GameHero> heroes = game.getHeroes();
        GameHero hero = heroes.get(0);
        String heroName = hero.getHeroName();
        hero.setPosition(new Tile((long) 25, TileType.FLOOR,  20,4,  false,  false, TrapType.NONE, true));
        heroes.set(0, hero);
        game.setHeroes(heroes);

        GameLobby lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        when(gameLobbyService.loadGameLobby((long) 1)).thenReturn(lobby);
        List<Tile> range = movementService.getRangePlayerMovement((long) 1, heroName);
        List<Tile> path = algorithmHandler.executePlayer(range, game, hero);
        for (Tile tile : path) {
            System.out.println("X: " + tile.getX() +" - Y: " + tile.getY());
        }
        assertEquals(5, path.size());
    }

    @Test
    // @Ignore("Weird bug on remote")
    public void executePlayer_withMonsters_inRange_withOneLowestHealth_largerMoveRange_HarderCalculation() {
        Game game = testAIGame.getAIGame(true, true);
        List<GameHero> heroes = game.getHeroes();
        GameHero hero = heroes.get(0);
        String heroName = hero.getHeroName();
        hero.setMove(10);
        hero.setPosition(new Tile((long) 22,TileType.BOTTOMLEFT,  17,4,  false,  false, TrapType.NONE, false));
        heroes.set(0, hero);
        game.setHeroes(heroes);

        GameLobby lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        when(gameLobbyService.loadGameLobby((long) 1)).thenReturn(lobby);

        List<Tile> range = movementService.getRangePlayerMovement((long) 1, heroName);
        List<Tile> path = algorithmHandler.executePlayer(range, game, hero);

        for (Tile tile : path) {
            System.out.println("X: " + tile.getX() +" - Y: " + tile.getY());
        }
        assertEquals(8, path.size());
    }

    @Test
    // @Ignore("Weird bug on remote")
    public void executePlayer_withNoMonsters_inRange() {
        Game game = testAIGame.getAIGame(false, false);
        List<GameHero> heroes = game.getHeroes();
        GameHero hero = heroes.get(0);
        String heroName = hero.getHeroName();
        hero.setPosition(new Tile((long) 25,TileType.BOTTOM,  20,4,  false,  false, TrapType.NONE, true));
        heroes.set(0, hero);
        game.setHeroes(heroes);

        GameLobby lobby = new GameLobby();
        lobby.setGame(game);
        lobby.setId((long) 1);
        when(gameLobbyService.loadGameLobby((long) 1)).thenReturn(lobby);

        List<Tile> range = movementService.getRangePlayerMovement((long) 1, heroName);
        List<Tile> path = algorithmHandler.executePlayer(range, game, hero);

        for (Tile tile : path) {
            System.out.println("X: " + tile.getX() +" - Y: " + tile.getY());
        }
    }
}
