//package be.kdg.integratie2backend2019.gameLogic;
//
//import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
//import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.TilePrefab;
//import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
//import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
//import be.kdg.integratie2backend2019.gameLogic.service.MovementService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import static org.junit.Assert.assertNotNull;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class MovementServiceTest {
//
//    @Autowired
//    private MovementService movementService;
//
//    List<TilePrefab> listTilesRoom = new ArrayList<>(Arrays.asList(
//            new TilePrefab(TileType.TOPLEFT, 7, 0),
//            new TilePrefab(TileType.TOP,  8, 0),
//            new TilePrefab(TileType.TOP,  9, 0),
//            new TilePrefab(TileType.TOPRIGHT,  10, 0),
//
//            new TilePrefab(TileType.LEFT, 7, 1),
//            new TilePrefab(TileType.FLOOR,  8, 1),
//            new TilePrefab(TileType.FLOOR,  9, 1),
//            new TilePrefab(TileType.RIGHT,  10, 1),
//
//            new TilePrefab(TileType.LEFT,  7, 2),
//            new TilePrefab(TileType.FLOOR,  8, 2),
//            new TilePrefab(TileType.FLOOR,  9, 2),
//            new TilePrefab(TileType.RIGHT,  10, 2),
//
//            new TilePrefab(TileType.TOPLEFT,  4, 3, true, false),
//            new TilePrefab(TileType.TOP,  5, 3),
//            new TilePrefab(TileType.TOP,  6, 3),
//            new TilePrefab(TileType.FLOOR,  7, 3),
//            new TilePrefab(TileType.FLOOR,  8, 3),
//            new TilePrefab(TileType.FLOOR,  9, 3, TrapType.PIT),
//            new TilePrefab(TileType.RIGHT,  10, 3),
//
//            new TilePrefab(TileType.LEFT, 4, 4),
//            new TilePrefab(TileType.FLOOR,  5, 4),
//            new TilePrefab(TileType.FLOOR,  6, 4),
//            new TilePrefab(TileType.FLOOR,  7, 4, TrapType.PIT),
//            new TilePrefab(TileType.FLOOR,  8, 4),
//            new TilePrefab(TileType.FLOOR,  9, 4),
//            new TilePrefab(TileType.RIGHT,  10, 4),
//
//            new TilePrefab(TileType.LEFT, 4,5),
//            new TilePrefab(TileType.FLOOR,  5,5),
//            new TilePrefab(TileType.FLOOR, 6,5),
//            new TilePrefab(TileType.FLOOR,  7,5),
//            new TilePrefab(TileType.FLOOR,  8,5),
//            new TilePrefab(TileType.FLOOR,  9,5, TrapType.PIT),
//            new TilePrefab(TileType.RIGHT,  10,5),
//
//            new TilePrefab(TileType.BOTTOMLEFT,  4,6, true, false),
//            new TilePrefab(TileType.BOTTOM,  5,6),
//            new TilePrefab(TileType.BOTTOM,  6,6),
//            new TilePrefab(TileType.BOTTOM,  7,6),
//            new TilePrefab(TileType.BOTTOM,  8,6),
//            new TilePrefab(TileType.BOTTOM,  9,6),
//            new TilePrefab(TileType.BOTTOMRIGHT,  10,6)
//    ));
//    private TilePrefab tilePrefab = new TilePrefab(TileType.FLOOR, 5, 3);
//
//    @Test
//    public void testMoveRange() {
//        /*
//        List<Tile> range = movementService.getRange(listTilesRoom, tilePrefab, 3);
//        for (Tile tile : range) {
//            System.out.println("************ x: " + tilePrefab1.getX() + "  y: " + tilePrefab1.getY());
//        }
//        assertNotNull(range);
//        */
//    }
//
//}
