package be.kdg.integratie2backend2019.gameLogic.web.controller;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Door;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.TilePrefab;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.game.service.GameService;
import be.kdg.integratie2backend2019.game.web.dto.TileDTO;
import be.kdg.integratie2backend2019.gameLogic.persistence.dao.ActionRepository;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.DoorAction;
import be.kdg.integratie2backend2019.gameLogic.service.GameLogicService;
import be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject.TestLobby;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.BDDMockito.*;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActionControllerTest {

    private GameLobby lobby;
    @Autowired
    private TestLobby testLobby;
    @Autowired
    private ActionController actionController;
    @Autowired
    private ModelMapper mapper;

    @MockBean
    private ActionRepository actionRepositoryMock;
    @MockBean
    private GameLobbyService lobbyServiceMock;
    @MockBean
    private GameService gameServiceMock;
    @MockBean
    private GameLogicService gameLogicServiceMock;


    public ActionControllerTest() {
    }



    @Before
    public void setUp() {
        lobby = testLobby.getTestLobby();
    }

    @Test
    public void openDoor() {
        // set character in front of door ("Mialee")
        Game game = lobby.getGame();
        List<GameHero> heroes = game.getHeroes();
        GameHero hero = heroes.get(0);
        hero.setPosition(new Tile((long) 25,TileType.BOTTOM,  20,4,  false,  false, TrapType.NONE, true));
        heroes.add(hero);
        game.setHeroes(heroes);
        game.setVisibleRooms(game.getDungeonRooms());
        lobby.setGame(game);

        // dooraction
        Door door = lobby.getGame().getVisibleRooms().get(0).getDoors().get(0);
        DoorAction doorAction = new DoorAction("Mialee", door);

        when(lobbyServiceMock.loadGameLobby((long) 1)).thenReturn(lobby);


        // methode aanroepen uit controller
        ResponseEntity<?> response = actionController.openDoor((long) 1, doorAction);
        assertTrue(response.getStatusCode().is2xxSuccessful());

        // broadcast opvangen

        // persistence van action opvangen
    }


    @Test
    public void move() {

    }

    @Test
    public void attack() {

    }

    @Test
    public void openChest() {
    }

    @Test
    public void potion() {
    }

    @Test
    public void trade() {
    }

    @Test
    public void equip() {
    }

    @Test
    public void getRangeWithoutPillars() {
        List<Tile> range = new ArrayList<>(Arrays.asList(
                new Tile((long) 2, TileType.TOPLEFT,  17,0,  false,  false, TrapType.NONE, false),
                new Tile((long) 3,TileType.TOP,  18,0,  false,  false, TrapType.NONE, false),
                new Tile((long) 4,TileType.TOP,  19,0,  false,  false, TrapType.NONE, false),
                new Tile((long) 5,TileType.TOP,  20,0,  false,  false, TrapType.NONE, false),
                new Tile((long) 7,TileType.LEFT,  17,1,  false,  false, TrapType.NONE, false),
                new Tile((long) 8,TileType.FLOOR,  18,1,  false,  false, TrapType.NONE, false),
                new Tile((long) 9,TileType.FLOOR,  19,1,  false,  false, TrapType.NONE, false),
                new Tile((long) 13,TileType.FLOOR,  18,2,  false,  false, TrapType.NONE, false)
        ));
        List<TileDTO> rangeDTO = range.stream().map(tile -> mapper.map(tile, TileDTO.class)).collect(Collectors.toList());
        Game game = lobby.getGame();
        List<GameHero> heroes = game.getHeroes();
        GameHero hero = heroes.get(0);
        hero.setPosition(new Tile((long) 3,TileType.TOP,  18,0,  false,  false, TrapType.NONE, false));
        heroes.add(hero);
        game.setHeroes(heroes);
        lobby.setGame(game);
        when(lobbyServiceMock.loadGameLobby((long) 1)).thenReturn(lobby);
        ResponseEntity<?> response = actionController.getRange((long) 1, "Mialee");
        assertTrue(response.getStatusCode().is2xxSuccessful());
        List<Tile> calculatedRange = (List<Tile>) response.getBody();
        System.out.println(calculatedRange);
        System.out.println(rangeDTO);
        assertTrue(rangeDTO.size() == calculatedRange.size());
        assertTrue(rangeDTO.containsAll(calculatedRange));

    }

    @Test
    public void getRangeWithPillars(){
        List<Tile> range = new ArrayList<>(Arrays.asList(
                new Tile((long) 28,TileType.TOP,  18,5,  false,  false, TrapType.NONE, false),
                new Tile((long) 29,TileType.TOP,  19,5,  false,  false, TrapType.NONE, false),
                new Tile((long) 30,TileType.TOP,  20,5,  false,  false, TrapType.NONE, true),
                new Tile((long) 32,TileType.LEFT,  17,6,  false,  false, TrapType.NONE, false),
                new Tile((long) 33,TileType.FLOOR,  18,6,  false,  false, TrapType.NONE, false),
                new Tile((long) 34,TileType.FLOOR,  19,6,  false,  false, TrapType.NONE, false),
                new Tile((long) 38,TileType.FLOOR,  18,7,  false,  false, TrapType.NONE, false)
        ));
        List<TileDTO> rangeDTO = range.stream().map(tile -> mapper.map(tile, TileDTO.class)).collect(Collectors.toList());
        Game game = lobby.getGame();
        List<GameHero> heroes = game.getHeroes();
        GameHero hero = heroes.get(0);
        hero.setPosition(new Tile((long) 28,TileType.TOP,  18,5,  false,  false, TrapType.NONE, false));
        heroes.add(hero);
        game.setHeroes(heroes);
        game.setVisibleRooms(game.getDungeonRooms());
        lobby.setGame(game);

        when(lobbyServiceMock.loadGameLobby((long) 1)).thenReturn(lobby);
        ResponseEntity<?> response = actionController.getRange((long) 1, "Mialee");
        assertTrue(response.getStatusCode().is2xxSuccessful());
        List<Tile> calculatedRange = (List<Tile>) response.getBody();
        System.out.println(calculatedRange);
        System.out.println(rangeDTO);
        assertTrue(rangeDTO.size() == calculatedRange.size());
        assertTrue(rangeDTO.containsAll(calculatedRange));
    }
}
