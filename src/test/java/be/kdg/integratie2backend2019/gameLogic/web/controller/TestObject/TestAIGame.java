package be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import be.kdg.integratie2backend2019.game.persistence.model.items.Treasure;
import be.kdg.integratie2backend2019.game.persistence.model.stories.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class TestAIGame {
    private TestGameHero testGameHero;

    public TestAIGame(TestGameHero testGameHero) {
        this.testGameHero = testGameHero;
    }

    public Game getAIGame(boolean monsters, boolean damagedMonster) {
        Game game = new Game();
        game.setVisibleRooms(getTestRooms(monsters, damagedMonster));
        game.setHeroes(testGameHero.getAIHeroes());
        return game;
    }

    private Story getTestStory(){
        Story story = new Story();
        story.setDescription("This is a test story.");
        story.setGoal(null);
        story.setDungeon(getTestDungeon());
        story.setGoalReached(false);
        story.setId((long) 1);
        story.setName("TestStory");
        story.setStoryNumber(1);
        return story;
    }

    private Dungeon getTestDungeon(){
        Dungeon dungeon = new Dungeon();
        dungeon.setId((long) 1);
        dungeon.setName("TestDungeon");
        dungeon.setRoomPrefabs(null);
        return dungeon;
    }

    private List<Room> getTestRooms(boolean monsters, boolean damagedMonster){

        List<Room> dungeonRooms = new ArrayList<>();

        List<Tile> tilesRoom1 = new ArrayList<>(Arrays.asList(
                new Tile((long) 2, TileType.TOPLEFT,  17,0,  false,  false, TrapType.NONE, false),
                new Tile((long) 3,TileType.TOP,  18,0,  false,  false, TrapType.NONE, false),
                new Tile((long) 4,TileType.TOP,  19,0,  false,  false, TrapType.NONE, false),
                new Tile((long) 5,TileType.TOP,  20,0,  false,  false, TrapType.NONE, false),
                new Tile((long) 6,TileType.TOPRIGHT,  21,0,  false,  false, TrapType.NONE, false),

                new Tile((long) 7,TileType.LEFT,  17,1,  false,  false, TrapType.NONE, false),
                new Tile((long) 8,TileType.FLOOR,  18,1,  false,  false, TrapType.NONE, false),
                new Tile((long) 9,TileType.FLOOR,  19,1,  false,  false, TrapType.NONE, false),
                new Tile((long) 10,TileType.FLOOR,  20,1,  false,  false, TrapType.NONE, false),
                new Tile((long) 11,TileType.RIGHT,  21,1,  false,  false, TrapType.NONE, false),

                new Tile((long) 12,TileType.LEFT,  17,2,  false,  false, TrapType.NONE, false),
                new Tile((long) 13,TileType.FLOOR,  18,2,  false,  false, TrapType.NONE, false),
                new Tile((long) 14,TileType.FLOOR,  19,2,  false,  false, TrapType.NONE, false),
                new Tile((long) 15,TileType.FLOOR,  20,2,  false,  false, TrapType.NONE, false),
                new Tile((long) 16,TileType.RIGHT,  21,2,  false,  false, TrapType.NONE, false),

                new Tile((long) 17,TileType.LEFT,  17,3,  false,  false, TrapType.NONE, false),
                new Tile((long) 18,TileType.FLOOR,  18,3,  false,  false, TrapType.NONE, false),
                new Tile((long) 19,TileType.FLOOR,  19,3,  false,  false, TrapType.NONE, false),
                new Tile((long) 20,TileType.FLOOR,  20,3,  false,  false, TrapType.NONE, false),
                new Tile((long) 21,TileType.RIGHT,  21,3,  false,  false, TrapType.NONE, false),

                new Tile((long) 22,TileType.BOTTOMLEFT,  17,4,  false,  false, TrapType.NONE, false),
                new Tile((long) 23,TileType.BOTTOM,  18,4,  false,  false, TrapType.NONE, false),
                new Tile((long) 24,TileType.BOTTOM,  19,4,  false,  false, TrapType.NONE, false),
                new Tile((long) 25,TileType.FLOOR,  20,4,  false,  false, TrapType.NONE, true),
                new Tile((long) 26,TileType.BOTTOMRIGHT,  21,4,  false,  false, TrapType.NONE, false)
        ));

        Room room1 = new Room("startRoom", tilesRoom1);
        room1.setStarterRoom(true);
        room1.setGameMonsters(new ArrayList<>());

        List<Tile> tilesRoom2 = new ArrayList<>(Arrays.asList(
                new Tile((long) 27,TileType.TOPLEFT,  17,5, true, false, TrapType.NONE, false),
                new Tile((long) 28,TileType.TOP,  18,5,  false,  false, TrapType.NONE, false),
                new Tile((long) 29,TileType.TOP,  19,5,  false,  false, TrapType.NONE, false),
                new Tile((long) 30,TileType.FLOOR,  20,5,  false,  false, TrapType.NONE, true),
                new Tile((long) 31,TileType.TOPRIGHT,  21,5, true, false, TrapType.NONE, false),

                new Tile((long) 32,TileType.LEFT,  17,6,  false,  false, TrapType.NONE, false),
                new Tile((long) 33,TileType.FLOOR,  18,6,  false,  false, TrapType.NONE, false),
                new Tile((long) 34,TileType.FLOOR,  19,6,  false,  false, TrapType.NONE, false),
                new Tile((long) 35,TileType.FLOOR,  20,6,  false,  false, TrapType.NONE, false),
                new Tile((long) 36,TileType.RIGHT,  21,6,  false,  false, TrapType.NONE, false),

                new Tile((long) 37,TileType.LEFT, 17,7,  false,  false, TrapType.NONE, false),
                new Tile((long) 38,TileType.FLOOR,  18,7,  false,  false, TrapType.NONE, false),
                new Tile((long) 39,TileType.FLOOR,  19,7,  false,  false, TrapType.NONE, false),
                new Tile((long) 40,TileType.FLOOR,  20,7,  false,  false, TrapType.NONE, false),
                new Tile((long) 41,TileType.RIGHT,  21,7,  false,  false, TrapType.NONE, false),

                new Tile((long) 42,TileType.LEFT,  17,8,  false,  false, TrapType.NONE, false),
                new Tile((long) 43,TileType.FLOOR,  18,8, false, false, TrapType.PIT, false),
                new Tile((long) 44,TileType.FLOOR,  19,8,  false,  false, TrapType.NONE, false),
                new Tile((long) 45,TileType.FLOOR,  20,8, false, false, TrapType.PIT, false),
                new Tile((long) 46,TileType.RIGHT,  21,8,  false,  false, TrapType.NONE, false),

                new Tile((long) 47,TileType.LEFT,  17,9, true, false, TrapType.NONE, false),
                new Tile((long) 48,TileType.FLOOR,  18,9,  false,  false, TrapType.NONE, false),
                new Tile((long) 49,TileType.FLOOR,  19,9,  false,  false, TrapType.NONE, false),
                new Tile((long) 50,TileType.FLOOR,  20,9,  false,  false, TrapType.NONE, false),
                new Tile((long) 51,TileType.RIGHT,  21,9, true, false, TrapType.NONE, false),

                new Tile((long) 52,TileType.BOTTOMLEFT,  17,10,  false,  false, TrapType.NONE, false),
                new Tile((long) 53,TileType.BOTTOM,  18,10,  false,  false, TrapType.NONE, false),
                new Tile((long) 54,TileType.BOTTOM,  19,10, false, true, TrapType.NONE, false),
                new Tile((long) 55,TileType.BOTTOM, 20,10,  false,  false, TrapType.NONE, false),
                new Tile((long) 56,TileType.BOTTOMRIGHT,  21,10,  false,  false, TrapType.NONE, false)
        ));

        Room room2 = new Room("room2", tilesRoom2);

        if(monsters){
            Monster monster = new Monster("", "Goblin", "", 1, 5, 100, new ArrayList<>(), new ArrayList<>(), 5);
            GameMonster gameMonster1 = new GameMonster(monster);
            gameMonster1.setPosition(new Tile((long) 42,TileType.LEFT,  17,7,  false,  false, TrapType.NONE, false));

            if(damagedMonster){
                gameMonster1.setHitPoints(gameMonster1.getHitPoints() - 2);
            }

            GameMonster gameMonster2 = new GameMonster(monster);
            gameMonster1.setPosition(new Tile((long) 46,TileType.RIGHT,  21,8,  false,  false, TrapType.NONE, false));
            room2.setGameMonsters(Arrays.asList(gameMonster1, gameMonster2));
        }
        else{
            room2.setGameMonsters(new ArrayList<>());
        }

        room2.setStarterRoom(false);

        Door door = new Door((long) 1,"R3R6",getDoorTile(tilesRoom1,20,4),getDoorTile(tilesRoom2,20,5),true,false);
        room1.setDoors(new ArrayList<>(Arrays.asList(door)));
        room2.setDoors(new ArrayList<>(Arrays.asList(door)));
        dungeonRooms.add(room1);
        dungeonRooms.add(room2);
        return dungeonRooms;
    }

    private List<Room> getStartRoom(List<Room> dungeonRooms){
        return new ArrayList<>(Arrays.asList(dungeonRooms.get(0)));
    }

    private Tile getDoorTile(List<Tile> roomtiles, int x, int y){
        Optional<Tile> optionalTile = roomtiles.stream().filter(t -> t.getX() == x).filter(t -> t.getY() == y).findAny();
        return optionalTile.orElse(null);
    }

    private List<Treasure> getTestTreasures(){
        List<Treasure> treasures = new ArrayList<>(
                Arrays.asList(

                )
        );
        return treasures;
    }
}
