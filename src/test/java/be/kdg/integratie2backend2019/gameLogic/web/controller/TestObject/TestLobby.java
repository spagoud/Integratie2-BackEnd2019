package be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject;

import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import org.springframework.stereotype.Component;

@Component
public class TestLobby {
    private TestGame testGame;

    public TestLobby(TestGame testGame) {
        this.testGame = testGame;
    }

    public GameLobby getTestLobby(){
        GameLobby gameLobby = new GameLobby();
        gameLobby.setGame(testGame.getGame());
        gameLobby.setViewers(null);
        gameLobby.setName("Test Lobby");
        gameLobby.setLobbyOwner(new UserProfile("TestUser"));
        gameLobby.setInviteOnly(false);
        gameLobby.setId((long) 1);

        return gameLobby;
    }
}
