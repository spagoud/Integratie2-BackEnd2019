package be.kdg.integratie2backend2019.gameLogic.web.controller.TestObject;

import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Equipment;
import be.kdg.integratie2backend2019.game.persistence.model.dices.Dice;
import be.kdg.integratie2backend2019.game.persistence.model.items.Artifact;
import be.kdg.integratie2backend2019.game.persistence.model.items.Spell;
import be.kdg.integratie2backend2019.game.persistence.model.items.Weapon;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.TilePrefab;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class TestGameHero {
    private GameHero gameHero;
    private List<GameHero> heroes = new ArrayList<>();

    private List<GameHero> createGameHeroes(){

        Dice attackDiceY = new Dice("Yellow",0,1, new ArrayList<>(Arrays.asList(1,1,1,1,0,0)));
        Dice attackDiceO = new Dice("Orange",1,2, new ArrayList<>(Arrays.asList(1,1,1,1,2,2)));
        Dice attackDiceR = new Dice("Red",0,3, new ArrayList<>(Arrays.asList(0,1,2,2,2,3)));
        Dice attackDiceP = new Dice("Purple", 2,3, new ArrayList<>(Arrays.asList(2,2,2,2,3,3)));

        Weapon BTD = new Weapon("Balanced Throwing Dagger",  "Description", "image", 1,true,"Weapon",true, Arrays.asList(attackDiceY, attackDiceY), Arrays.asList(attackDiceO,attackDiceO));
        Weapon CoF = new Weapon("Crossbow of faith", "Description", "image", 1,true,"Weapon", true, Arrays.asList(attackDiceY,attackDiceY), null);
        Weapon SBotA = new Weapon("Short bow of the ancients",  "Description", "image", 1,true,"Weapon", true,Arrays.asList(attackDiceY,attackDiceY), null);
        Weapon SHB = new Weapon("Single-handed broadsword",  "Description", "image", 1,true,"Weapon", false,Arrays.asList(attackDiceO), null);
        Spell GR = new Spell("Greater restoration",   "Description", "image", 1,true, "Spell", 4, null, false, true, null, false);
        Spell MM = new Spell("Magic missile",   "Description", "image", 1,true ,"Spell", 2,3,false,true, Arrays.asList(attackDiceY,attackDiceR), false);
        Artifact YA = new Artifact("Yondalla's amulet",  "Description", "image", 1, "Artifact","When you open a chest, choose whether you keep the item. If not, discard it and take another treasure. Repeat up to 3 times. If a Booby trap is opened it activates and your turn ends (NYI)");


        Equipment seRegdar = new Equipment(SHB, null, null);
        Equipment seLidda = new Equipment(BTD, YA, null);
        Equipment seJozan = new Equipment(CoF, null, GR);
        Equipment seMialee = new Equipment(SBotA, null, MM);

        gameHero = new GameHero(
                null,
                5,
                5,
                2,
                null,
                seMialee,
                5,
                5,
                "Mialee",
                null,
                4,
                null,
                2
        );
        gameHero.setPosition(new Tile((long) 3,TileType.TOP,  18,0,  false,  false, TrapType.NONE, false));
        heroes.add(gameHero);
        gameHero = new GameHero(
                null,
                5,
                5,
                2,
                null,
                seRegdar,
                5,
                5,
                "Regdar",
                null,
                4,
                null,
                2
        );
        gameHero.setPosition(new Tile((long) 4,TileType.TOP,  19,0,  false,  false, TrapType.NONE, false));
        heroes.add(gameHero);
        gameHero = new GameHero(
                null,
                5,
                5,
                2,
                null,
                seJozan,
                5,
                5,
                "Jozan",
                null,
                4,
                null,
                2
        );
        gameHero.setPosition(new Tile((long) 5,TileType.TOP,  20,0,  false,  false, TrapType.NONE, false));
        heroes.add(gameHero);
        gameHero = new GameHero(
                null,
                5,
                5,
                2,
                null,
                seLidda,
                5,
                5,
                "Lidda",
                null,
                4,
                null,
                2
        );
        gameHero.setPosition(new Tile((long) 8,TileType.FLOOR,  18,1,  false,  false, TrapType.NONE, false));
        heroes.add(gameHero);
        return heroes;
    }

    private List<GameHero> createAIGameHeroes(){
        GameHero gameHero = new GameHero(
                null,
                5,
                5,
                2,
                null,
                null,
                5,
                5,
                "Mialee",
                null,
                4,
                null,
                5
        );
        return new ArrayList(Arrays.asList(gameHero));
    }

    public List<GameHero> getHeroes(){
        return createGameHeroes();
    }

    public List<GameHero> getAIHeroes(){return createAIGameHeroes();}


}
