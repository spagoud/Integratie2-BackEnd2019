package be.kdg.integratie2backend2019.userProfile.service;

import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.security.service.IUserService;
import be.kdg.integratie2backend2019.userProfile.persistence.dao.UserProfileRepository;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import be.kdg.integratie2backend2019.userProfile.web.dto.UserProfileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserProfileService {

    @Autowired
    private UserProfileRepository repository;

    @Autowired
    private IUserService userService;

    public UserProfile createUserProfile(UserProfileDTO userProfileDTO, User user){
        UserProfile userProfile = new UserProfile(userProfileDTO.getUsername());
        userProfile.setProfilePicture(userProfileDTO.getProfilePicture());
        userProfile.setUser(user);
        userProfile = repository.save(userProfile);
        user.setUserProfile(userProfile);
        userService.saveRegisteredUser(user);
        return userProfile;
    }

    public UserProfile loadUserProfile(String email){
        User user = userService.findUserByEmail(email);
        return user.getUserProfile();
    }

    public UserProfile findUserByUserName(String username){
        return repository.findByUsername(username);
    }


}
