package be.kdg.integratie2backend2019.userProfile.service;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.*;
import be.kdg.integratie2backend2019.userProfile.persistence.dao.CharacterStatisticRepository;
import be.kdg.integratie2backend2019.userProfile.persistence.dao.UserStatisticRepository;
import be.kdg.integratie2backend2019.userProfile.persistence.model.CharacterStatistic;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserStatistic;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserStatisticService {

    @Autowired
    private UserStatisticRepository userStatisticRepository;
    @Autowired
    private CharacterStatisticRepository characterStatisticRepository;
    @Autowired
    private UserProfileService userProfileService;

    public UserStatistic loadUserStatistic(String email){
        UserProfile userProfile = userProfileService.loadUserProfile(email);
        return userStatisticRepository.findByUserProfile(userProfile);
    }

    public void updateStatistics(Game game){
        List<Action> actions = game.getActionsPerformed();
        for (Action action : actions) {

            // update character deaths
            if(StringUtils.isNumeric(action.getHeroName())){
                if(action.getActionType().equals(Action.ActionType.ATTACK)){
                    AttackAction attackAction = (AttackAction) action;
                    for (GameHero hero: game.getHeroes()) {
                        if(hero.getId().equals(attackAction.getTargetId())){
                            CharacterStatistic characterStatistic = hero.getUserProfile().getUserStatistic().getCharacterStats(hero.getHeroName());
                            characterStatistic.updateDamageTaken(attackAction.getDamage());
                            if(attackAction.isDead()) {
                                characterStatistic.updateDeaths(1);
                            }
                        }
                    }
                }
            }

            // update character specific statistics
            if(!action.getHeroName().contains("AI") && !StringUtils.isNumeric(action.getHeroName())){
                for (GameHero hero: game.getHeroes()) {
                    if(hero.getHeroName().equals(action.getHeroName())){
                        CharacterStatistic characterStatistic = hero.getUserProfile().getUserStatistic().getCharacterStats(hero.getHeroName());
                        characterStatistic.setTimePlayed(characterStatistic.getTimePlayed() + game.getTimePlayed());
                        switch (action.getActionType()){
                            case POTION:
                                PotionAction potionAction = (PotionAction) action;
                                break;
                            case ATTACK:
                                AttackAction attackAction = (AttackAction) action;
                                characterStatistic.updateDamageDone(attackAction.getDamage());
                                if(attackAction.isDead()){
                                    characterStatistic.updateMonstersKilled(1);
                                }
                                break;
                            case CHEST:
                                ChestAction chestAction = (ChestAction) action;
                                break;
                            case TRAP:
                                TrapAction trapAction = (TrapAction) action;
                                characterStatistic.updateTrapsActivated(1);
                                break;
                            case MOVE:
                                MoveAction moveAction = (MoveAction) action;
                                characterStatistic.updateTilesMoved(moveAction.getMovementPath().size());
                                break;
                            case DOOR:
                                DoorAction doorAction = (DoorAction) action;
                                characterStatistic.updateDoorsOpenend(1);
                                break;
                            default: break;
                        }
                        characterStatisticRepository.save(characterStatistic);
                    }
                }
            }
        }

        // update general statistics
        for (GameHero hero : game.getHeroes()) {
            if(!hero.getHeroName().contains("AI")){
                // TODO: check if goal was reached
                UserStatistic userStatistic = hero.getUserProfile().getUserStatistic();
                if(userStatistic.getFastestClearingTime() > game.getTimePlayed()){
                    userStatistic.setFastestClearingTime(game.getTimePlayed());
                }

                userStatisticRepository.save(userStatistic);
            }
        }
    }
}
