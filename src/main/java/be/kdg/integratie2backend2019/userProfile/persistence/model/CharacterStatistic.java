package be.kdg.integratie2backend2019.userProfile.persistence.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "character_statistic")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class CharacterStatistic {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String characterName;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private UserStatistic userStatistic;

    private int score = 0;
    private int timePlayed = 0;
    private int monstersKilled = 0;
    private int deaths = 0;
    private int trapsActivated = 0;
    private int trapsDismantled = 0;
    private int bossesKilled = 0;
    private int damageDone = 0;
    private int damageTaken = 0;
    private int tilesMoved = 0;
    private int doorsOpenend = 0;

    public CharacterStatistic(UserStatistic userStatistic, String characterName) {
        this.characterName = characterName;
        this.userStatistic = userStatistic;
    }

    public void updateScore(int score){
        this.score += score;
    }

    public void updateTimePlayed(int timePlayed){
        this.timePlayed += timePlayed;
    }

    public void updateMonstersKilled(int monstersKilled){
        this.monstersKilled += monstersKilled;
    }

    public void updateDeaths(int deaths){
        this.deaths += deaths;
    }

    public void updateTrapsActivated(int trapsActivated){
        this.trapsActivated += trapsActivated;
    }

    public void updateTrapsDismantled(int trapsDismantled){
        this.trapsDismantled += trapsDismantled;
    }

    public void updateBossesKilled(int bossesKilled){
        this.bossesKilled += bossesKilled;
    }

    public void updateDamageDone(int damageDone){
        this.damageDone += damageDone;
    }

    public void updateDamageTaken(int damageTaken){
        this.damageTaken += damageTaken;
    }

    public void updateTilesMoved(int tilesMoved){
        this.tilesMoved += tilesMoved;
    }

    public void updateDoorsOpenend(int doorsOpenend){
        this.doorsOpenend += doorsOpenend;
    }
}
