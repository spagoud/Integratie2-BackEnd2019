package be.kdg.integratie2backend2019.userProfile.persistence.dao;

import be.kdg.integratie2backend2019.userProfile.persistence.model.CharacterStatistic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterStatisticRepository extends JpaRepository<CharacterStatistic, Long> {
}
