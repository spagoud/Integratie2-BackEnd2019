package be.kdg.integratie2backend2019.userProfile.persistence.model;

import be.kdg.integratie2backend2019.security.persistence.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "user_profile")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class UserProfile {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Lob
    @Type(type = "org.hibernate.type.ImageType")
    private byte[] profilePicture;

    /**
    * EXEMPLE CODE TO IMPORT THE PROFILE PICTURE
    *   File file = new File("img/JBDFav300.png");
        byte[] picInBytes = new byte[(int) file.length()];
        FileInputStream fileInputStream = new FileInputStream(file);
        fileInputStream.read(picInBytes);
        fileInputStream.close();
        user.setProfilePic(picInBytes);
    * */

    @OneToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name ="user_id", nullable = false)
    public User user;

    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL)
    private UserStatistic userStatistic;


    public UserProfile(String username) {
        this.username = username;
        this.userStatistic = new UserStatistic(this);
    }

    public void updateUsername() {
        this.username = username + "#" + id;
    }
}
