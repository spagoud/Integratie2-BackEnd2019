package be.kdg.integratie2backend2019.userProfile.persistence.dao;

import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {

    UserProfile findByUser(User user);
    UserProfile findByUsername(String userName);
}
