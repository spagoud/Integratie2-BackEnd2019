package be.kdg.integratie2backend2019.userProfile.persistence.dao;

import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserStatistic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserStatisticRepository extends JpaRepository<UserStatistic, Long> {
    UserStatistic findByUserProfile(UserProfile userProfile);
}
