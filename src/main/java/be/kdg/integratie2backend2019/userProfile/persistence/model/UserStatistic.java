package be.kdg.integratie2backend2019.userProfile.persistence.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "user_statistic")
@Data
@NoArgsConstructor
public class UserStatistic {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "userProfile_id", nullable = false)
    private UserProfile userProfile;

    private String favoriteHero = "unknown";
    private String highestDungeon = "unknown";
    private int fastestClearingTime;
    private int profileLevel = 1;

    // TODO: set maken van characterstatistics en de charactername hierin gebruiken om individueel te kunnen ophalen (sorteren)

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "characterStatistic_id", nullable = false)
    private Collection<CharacterStatistic> characterStatistics;

    public UserStatistic(UserProfile userProfile) {
        this.userProfile = userProfile;
        this.characterStatistics = new ArrayList<>();
        this.characterStatistics.add(new CharacterStatistic(this, "Regdar"));
        this.characterStatistics.add(new CharacterStatistic(this, "Jozan"));
        this.characterStatistics.add(new CharacterStatistic(this, "Lidda"));
        this.characterStatistics.add(new CharacterStatistic(this, "Mialee"));
    }

    public CharacterStatistic getCharacterStats(String characterName){
        for (CharacterStatistic charstat: characterStatistics) {
            if(charstat.getCharacterName().toUpperCase().equals(characterName.toUpperCase())){
                return charstat;
            }
        }
        return null;
    }
}
