package be.kdg.integratie2backend2019.userProfile.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CharacterStatisticDto {
    private String characterName;
    private int timePlayed;
    private int monstersKilled;
    private int deaths;
    private int score;
    private int trapsActivated;
    private int trapsDismantled;
    private int bossesKilled;
    private int damageDone;
    private int damageTaken;
}
