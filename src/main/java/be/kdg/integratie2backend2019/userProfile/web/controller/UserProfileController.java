package be.kdg.integratie2backend2019.userProfile.web.controller;

import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.security.service.UserService;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserStatistic;
import be.kdg.integratie2backend2019.userProfile.service.UserProfileService;
import be.kdg.integratie2backend2019.userProfile.service.UserStatisticService;
import be.kdg.integratie2backend2019.userProfile.web.dto.UserProfileDTO;
import be.kdg.integratie2backend2019.userProfile.web.dto.UserStatisticDto;
import com.netflix.discovery.converters.Auto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/userprofile")
public class UserProfileController {

    @Autowired
    private UserProfileService userProfileService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserStatisticService userStatisticService;
    @Autowired
    private ModelMapper mapper;

    @PostMapping
    public ResponseEntity<?> getUserProfile(@RequestBody String email){
        UserProfile userProfile = userProfileService.loadUserProfile(email);
        if(userProfile == null){
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(mapper.map(userProfile,
                    UserProfileDTO.class), HttpStatus.OK);
        }
    }

    @PostMapping("/newProfile/{email}")
    public ResponseEntity<?> createUserProfile(@RequestBody UserProfileDTO userProfileDTO, @PathVariable String email){
        User user = userService.findUserByEmail(email);
        userProfileService.createUserProfile(userProfileDTO, user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/statistics")
    public ResponseEntity<?> getUserStatistics(@RequestParam String email){
        UserStatistic userStatistic = userStatisticService.loadUserStatistic(email);
        return new ResponseEntity<>(mapper.map(userStatistic, UserStatisticDto.class) ,HttpStatus.OK);
    }

}
