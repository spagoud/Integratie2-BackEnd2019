package be.kdg.integratie2backend2019.userProfile.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserStatisticDto {
    private int score;
    private int fastestClearingTime;
    private int profileLevel = 1;
    private Collection<CharacterStatisticDto> characterStatistics;
}
