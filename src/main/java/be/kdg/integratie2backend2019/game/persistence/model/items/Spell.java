package be.kdg.integratie2backend2019.game.persistence.model.items;

import be.kdg.integratie2backend2019.game.persistence.model.dices.Dice;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table
public class Spell extends Item {
    private Integer mageSpellCost;
    private Integer clericSpellCost;
    private boolean melee;
    private boolean ranged;
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(name = "attack_spell_dice",
            joinColumns = @JoinColumn(name = "spell_id"),
            inverseJoinColumns = @JoinColumn(name = "dice_id")
    )
    private List<Dice> attackDices;
    private boolean onDead = false;

    public Spell(String name, String description, String img, int level, boolean startersEquipment, String type, Integer mageSpellCost, Integer clericSpellCost, boolean melee, boolean ranged, List<Dice> attackDices, boolean onDead) {
        super(name, description, img, level, startersEquipment, type, TreasureType.SPELL);
        this.mageSpellCost = mageSpellCost;
        this.clericSpellCost = clericSpellCost;
        this.melee = melee;
        this.ranged = ranged;
        this.attackDices = attackDices;
        this.onDead = onDead;
    }
}
