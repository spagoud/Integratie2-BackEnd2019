package be.kdg.integratie2backend2019.game.persistence.model.stories;

import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Room {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ElementCollection
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Tile> tiles;
    @ElementCollection
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<GameMonster> gameMonsters;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "Room_door",
            joinColumns = @JoinColumn(name = "room_id"),
            inverseJoinColumns = @JoinColumn(name = "door_id")
    )
    private List<Door> doors;
    private boolean starterRoom;

    public Room(String name, List<Tile> tiles) {
        this.name = name;
        this.tiles = tiles;
    }
}
