package be.kdg.integratie2backend2019.game.persistence.model;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Story;
import be.kdg.integratie2backend2019.game.persistence.model.items.Treasure;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.Action;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Bram Verlinden on 7/02/2019.
 */
@Data
@NoArgsConstructor
@Entity
@Table
public class Game {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ElementCollection
    private List<GameHero> heroes;
    @OneToOne(fetch = FetchType.EAGER)
    private Story story;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "game_treasure",
    joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "treasure_id"))
    private List<Treasure> availableTreasures;
    private String turnSequence;
    private String currentPlayer;
    private boolean lastMove;
    @ElementCollection
    private List<Room> dungeonRooms;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "game_visiblerooms",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "room_id"))
    private List<Room> visibleRooms;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "game_action_messages",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "action_message_id"))
    private List<Action> actionsPerformed;
    @Lob
    private String initialGameDataJson;
    private Date gameStarted;

    private Date lastUpdated;
    private int timePlayed;
    private GameStateType state;
    private Long playerNotReadyAmount; // decreasing number

    public int getPlayerCount() {
        return heroes.size();
    }

    public enum GameStateType{ACTIVE, PAUZED, ENDED};

    public Game(Story story) {
        this.story = story;
    }

    public Game(List<GameHero> heroes, Story story, List<Treasure> availableTreasures) {
        this.heroes = heroes;
        this.story = story;
        this.availableTreasures = availableTreasures;
    }

    public void addGameHero(GameHero gameHero){
        this.heroes.add(gameHero);
    }

    public void removeGameHero(GameHero gameHero){
        this.heroes.remove(gameHero);
    }

    public void addVisibleRoom(Room room){
        this.visibleRooms.add(room);
    }

    public void addAction(Action action){
        this.actionsPerformed.add(action);
    }
}
