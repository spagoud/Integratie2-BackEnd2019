package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomPrefab;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomPrefabRepository extends JpaRepository<RoomPrefab, Long> {
    RoomPrefab findByName(String name);
}
