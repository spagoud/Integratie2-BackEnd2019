package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<Game,Long> {
}
