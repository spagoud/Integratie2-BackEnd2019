package be.kdg.integratie2backend2019.game.persistence.model.characters;

import be.kdg.integratie2backend2019.game.persistence.model.items.Item;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table
public class Equipment {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Item weapon;
    @ManyToOne(fetch = FetchType.EAGER)
    private Item artifact;
    @ManyToOne(fetch = FetchType.EAGER)
    private Item extra;

    public Equipment(Item weapon, Item artifact, Item extra) {
        this.weapon = weapon;
        this.artifact = artifact;
        this.extra = extra;
    }
}
