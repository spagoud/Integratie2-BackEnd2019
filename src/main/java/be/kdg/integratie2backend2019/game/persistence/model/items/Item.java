package be.kdg.integratie2backend2019.game.persistence.model.items;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table
public class Item extends Treasure {
    private String type;

    public Item(String name, String description, String img, int level, String type, TreasureType treasureType) {
        super(name, description, img, level, treasureType);
        this.type = type;
    }

    public Item(String name, String description, String img, int level, boolean startersEquipment, String type, TreasureType treasureType) {
        super(name, description, img, level, startersEquipment, treasureType);
        this.type = type;
    }
}
