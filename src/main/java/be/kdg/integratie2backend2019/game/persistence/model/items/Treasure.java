package be.kdg.integratie2backend2019.game.persistence.model.items;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@NoArgsConstructor
@Table
public class Treasure {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;
    private String img;
    private int level;
    private boolean startersEquipment;
    private TreasureType treasureType;

    public Treasure(String name, String description, String img, int level, TreasureType treasureType) {
        this.name = name;
        this.description = description;
        this.img = img;
        this.level = level;
        this.startersEquipment = false;
        this.treasureType = treasureType;
    }

    public Treasure(String name, String description, String img, int level, boolean startersEquipment, TreasureType treasureType) {
        this.name = name;
        this.description = description;
        this.img = img;
        this.level = level;
        this.startersEquipment = startersEquipment;
        this.treasureType = treasureType;
    }

    public enum TreasureType{
        ARTIFACT,
        BOOBYTRAP,
        POTION,
        SPELL,
        WEAPON
    }
}
