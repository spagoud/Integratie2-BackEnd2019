package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.dices.Dice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiceRepository extends JpaRepository<Dice, Long> {
    Dice findDiceByType(String type);
    List<Dice> findAllByType(String type);
}
