package be.kdg.integratie2backend2019.game.persistence.model;

import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bram Verlinden on 7/02/2019.
 */

@Data
@NoArgsConstructor
@Entity
@Table
public class GameLobby {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    private String name;
    @NonNull
    @OneToOne(fetch = FetchType.EAGER)
    private UserProfile lobbyOwner;
    @OneToOne(fetch = FetchType.EAGER)
    private Game game;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "gameLobby_viewer",
            joinColumns = @JoinColumn(name = "gameLobby_id"),
            inverseJoinColumns = @JoinColumn(name = "userprofile_id"))
    private List<UserProfile> viewers;


    public GameLobby(String name, Game game) {
        this.name = name;
        this.game = game;
    }
    private boolean inviteOnly;

    public GameLobby(String name, @NonNull UserProfile lobbyOwner, Game game, boolean inviteOnly) {
        this.name = name;
        this.lobbyOwner = lobbyOwner;
        this.game = game;
        this.viewers = new ArrayList<>();
        this.inviteOnly = inviteOnly;
    }

    public void addViewer(UserProfile userProfile) {
        this.viewers.add(userProfile);
    }

    public void removeViewer(UserProfile userProfile) {
        this.viewers.remove(userProfile);
    }

    public int getPlayerCount(){
        return game.getPlayerCount();
    }
}
