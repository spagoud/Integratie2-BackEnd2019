package be.kdg.integratie2backend2019.game.persistence.model.stories;

import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.DoorPrefab;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.TilePrefab;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Door {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToOne(cascade = CascadeType.ALL)
    private Tile tile1;
    @OneToOne(cascade = CascadeType.ALL)
    private Tile tile2;

    private boolean enabled;
    private boolean locked;

    public Door(String name, Tile tile1, Tile tile2, boolean enabled, boolean locked) {
        this.name = name;
        this.tile1 = tile1;
        this.tile2 = tile2;
        this.enabled = enabled;
        this.locked = locked;
    }
}
