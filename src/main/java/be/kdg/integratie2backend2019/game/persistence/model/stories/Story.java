package be.kdg.integratie2backend2019.game.persistence.model.stories;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Story {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true, nullable = false)
    private int storyNumber;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String description;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Goal goal;
    private boolean goalReached;
    @OneToOne(fetch = FetchType.EAGER)
    private Dungeon dungeon;

    public Story(int storyNumber, String name, String description, Goal goal, boolean goalReached, Dungeon dungeon) {
        this.storyNumber = storyNumber;
        this.name = name;
        this.description = description;
        this.goal = goal;
        this.goalReached = goalReached;
        this.dungeon = dungeon;
    }
}
