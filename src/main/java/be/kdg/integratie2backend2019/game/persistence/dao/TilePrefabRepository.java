package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.TilePrefab;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TilePrefabRepository extends JpaRepository<TilePrefab, Long> {

}
