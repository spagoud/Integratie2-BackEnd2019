package be.kdg.integratie2backend2019.game.persistence.model;

import be.kdg.integratie2backend2019.game.persistence.model.characters.Equipment;
import be.kdg.integratie2backend2019.game.persistence.model.items.Item;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Bram Verlinden on 7/02/2019.
 */
@Data
@NoArgsConstructor
@Entity
@Table
@AllArgsConstructor
public class GameHero {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne(fetch = FetchType.LAZY)
    private UserProfile userProfile;
    private int magicPoints;
    private int healthPoints;
    private int armor;
    @ElementCollection
    private List<Item> itemList;
    @OneToOne(fetch = FetchType.EAGER)
    private Equipment equipment;
    private int maxMagicPoints;
    private int maxHealthPoints;
    private String heroName;
    private String heroImage;
    private int inventorySpace;
    private String type;
    @OneToOne(fetch = FetchType.EAGER)
    private Tile position;
    private int move;
    private boolean dead = false;

    public GameHero(UserProfile userProfile, int magicPoints, int healthPoints, int armor, List<Item> itemList,
                    Equipment equipment, int maxMagicPoints, int maxHealthPoints, String heroName, String heroImage,
                    int inventorySpace, String type, int move) {
        this.userProfile = userProfile;
        this.magicPoints = magicPoints;
        this.armor = armor;
        this.healthPoints = healthPoints;
        this.itemList = itemList;
        this.equipment = equipment;
        this.maxMagicPoints = maxMagicPoints;
        this.maxHealthPoints = maxHealthPoints;
        this.heroName = heroName;
        this.heroImage = heroImage;
        this.inventorySpace = inventorySpace;
        this.type = type;
        this.move = move;
    }
}
