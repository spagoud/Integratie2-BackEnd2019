package be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs;

import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomPrefab;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table
public class RoomMonster {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private Monster monster;
    private int amount;

    public RoomMonster(String name, Monster monster, int amount) {
        this.name = name;
        this.monster = monster;
        this.amount = amount;
    }
}
