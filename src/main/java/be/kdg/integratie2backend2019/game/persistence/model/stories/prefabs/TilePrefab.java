package be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs;

import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class TilePrefab {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private TileType tileType;
    private int x;
    private int y;
    private boolean pillar;
    private boolean chest;
    private TrapType trapType;
    private boolean door;


    public TilePrefab(TileType tileType, int x, int y) {
        this(tileType, x, y, false, false);
    }

    public TilePrefab(TileType tileType, int x, int y, boolean pillar, boolean chest) {
        this.tileType = tileType;
        this.x = x;
        this.y = y;
        this.pillar = pillar;
        this.chest = chest;
        this.trapType = TrapType.NONE;
    }

    public TilePrefab(TileType tileType, int x, int y, TrapType trapType) {
        this.tileType = tileType;
        this.x = x;
        this.y = y;
        this.trapType = trapType;
    }

    public TilePrefab(TileType tileType, int x, int y, boolean pillar, boolean chest, boolean door) {
        this(tileType, x, y, false, false);
        this.door = door;
    }
}
