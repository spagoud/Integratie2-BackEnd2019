package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Goal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoalRepository extends JpaRepository<Goal, Long> {
}
