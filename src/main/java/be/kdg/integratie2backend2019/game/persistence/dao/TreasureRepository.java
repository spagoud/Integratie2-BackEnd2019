package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.items.Treasure;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TreasureRepository extends JpaRepository<Treasure, Long> {
    List<Treasure> findAllByStartersEquipmentFalse();

    Treasure findByName(String name);
}
