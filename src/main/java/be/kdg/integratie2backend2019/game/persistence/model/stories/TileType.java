package be.kdg.integratie2backend2019.game.persistence.model.stories;

public enum  TileType {
    TOP, LEFT, RIGHT, BOTTOM,
    TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, FLOOR
}
