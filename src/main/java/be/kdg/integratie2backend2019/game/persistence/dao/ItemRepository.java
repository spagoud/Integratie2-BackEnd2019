package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.items.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Long> {
}
