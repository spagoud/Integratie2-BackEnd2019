package be.kdg.integratie2backend2019.game.persistence.model;

import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import be.kdg.integratie2backend2019.game.persistence.model.dices.Dice;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table
@NoArgsConstructor
public class GameMonster {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Lob
    private String symbolImg;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String description;
    private int armor;
    private int move;
    private int undeadScore;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "GameMonster_MeleeDices",
            joinColumns = @JoinColumn(name = "monster_id"),
            inverseJoinColumns = @JoinColumn(name = "dice_id")
    )
    private List<Dice> meleeDices;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "GameMonster_RangedDices",
            joinColumns = @JoinColumn(name = "monster_id"),
            inverseJoinColumns = @JoinColumn(name = "dice_id")
    )
    private List<Dice> rangedDices;
    private int hitPoints;
    private int maxHitPoints;
    @OneToOne(fetch = FetchType.EAGER)
    private Tile position;
    private boolean dead = false;

    public GameMonster(Monster monster) {
        this.symbolImg = monster.getSymbolImg();
        this.name = monster.getName();
        this.description = monster.getDescription();
        this.armor = monster.getArmor();
        this.move = monster.getMove();
        this.undeadScore = monster.getUndeadScore();
        this.meleeDices = new ArrayList<>(monster.getMeleeDices());
        this.rangedDices = new ArrayList<>(monster.getRangedDices());
        this.hitPoints = monster.getHitPoints();
        this.maxHitPoints = monster.getHitPoints();
    }
}
