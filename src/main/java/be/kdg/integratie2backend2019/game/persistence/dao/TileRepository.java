package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TileRepository extends JpaRepository<Tile, Long> {
}
