package be.kdg.integratie2backend2019.game.persistence.model.dices;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table

public class Dice {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String type;
    private int minimumRoll;
    private int maximumRoll;
    @ElementCollection
    private List<Integer> faces;

    public Dice(String type, int minimumRoll, int maximumRoll, List<Integer> faces) {
        this.type = type;
        this.minimumRoll = minimumRoll;
        this.maximumRoll = maximumRoll;
        this.faces = faces;
    }
}
