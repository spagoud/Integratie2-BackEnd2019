package be.kdg.integratie2backend2019.game.persistence.model.items;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table
public class Artifact extends Item {
    private String effect;
    //TODO fix effect


    public Artifact(String name, String description, String img, int level, String type, String effect) {
        super(name, description, img, level, type, TreasureType.ARTIFACT);
        this.effect = effect;
    }
}
