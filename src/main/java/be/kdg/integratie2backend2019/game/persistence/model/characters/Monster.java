package be.kdg.integratie2backend2019.game.persistence.model.characters;

import be.kdg.integratie2backend2019.game.persistence.model.dices.Dice;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table
public class Monster extends GameCharacter{
    private int undeadScore;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "Monster_MeleeDices",
            joinColumns = @JoinColumn(name = "monster_id"),
            inverseJoinColumns = @JoinColumn(name = "dice_id")
    )
    private List<Dice> meleeDices;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "Monster_RangedDices",
            joinColumns = @JoinColumn(name = "monster_id"),
            inverseJoinColumns = @JoinColumn(name = "dice_id")
    )
    private List<Dice> rangedDices;
    private int hitPoints;

    public Monster(String symbolImg, String name, String description, int armor, int move, int undeadScore, List<Dice> meleeDices, List<Dice> rangedDices, int hitPoints) {
        super(symbolImg, name, description, armor, move);
        this.undeadScore = undeadScore;
        this.meleeDices = meleeDices;
        this.rangedDices = rangedDices;
        this.hitPoints = hitPoints;
    }
}
