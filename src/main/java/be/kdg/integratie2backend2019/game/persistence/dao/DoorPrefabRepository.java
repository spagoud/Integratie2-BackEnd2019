package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.DoorPrefab;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoorPrefabRepository extends JpaRepository<DoorPrefab, Long> {
    DoorPrefab findByName(String name);
}
