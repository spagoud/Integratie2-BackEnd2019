package be.kdg.integratie2backend2019.game.persistence.model.stories;

import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomPrefab;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Dungeon {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "dungeon_room",
            joinColumns = @JoinColumn(name = "dungeon_id"),
            inverseJoinColumns = @JoinColumn(name = "room_id")
    )
    private List<RoomPrefab> roomPrefabs;

    public Dungeon(List<RoomPrefab> roomPrefabs) {
        this.roomPrefabs = roomPrefabs;
    }

    public Dungeon(String name, List<RoomPrefab> roomPrefabs) {
        this.name = name;
        this.roomPrefabs = roomPrefabs;
    }
}
