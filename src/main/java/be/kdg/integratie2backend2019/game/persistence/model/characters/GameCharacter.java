package be.kdg.integratie2backend2019.game.persistence.model.characters;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@NoArgsConstructor
@Table
public class GameCharacter {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String symbolImg;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String description;
    private int armor;
    private int move;

    public GameCharacter(String symbolImg, String name, String description, int armor, int move) {
        this.symbolImg = symbolImg;
        this.name = name;
        this.description = description;
        this.armor = armor;
        this.move = move;
    }
}
