package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.characters.Equipment;
import be.kdg.integratie2backend2019.game.persistence.model.items.Weapon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    Equipment findByWeapon(Weapon weapon);
}
