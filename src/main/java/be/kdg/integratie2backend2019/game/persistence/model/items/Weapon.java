package be.kdg.integratie2backend2019.game.persistence.model.items;

import be.kdg.integratie2backend2019.game.persistence.model.dices.Dice;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table
public class Weapon extends Item {
    private boolean ranged;
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(name = "attack_weapon_dice",
            joinColumns = @JoinColumn(name = "weapon_id"),
            inverseJoinColumns = @JoinColumn(name = "dice_id")
    )
    private List<Dice> attackDices;
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(name = "heavy_weapon_dice",
            joinColumns = @JoinColumn(name = "weapon_id"),
            inverseJoinColumns = @JoinColumn(name = "dice_id")
    )
    private List<Dice> heavyAttackDices;
    // TODO: implement special

    public Weapon(String name, String description, String img, int level, boolean startersEquipment, String type, boolean ranged, List<Dice> attackDices, List<Dice> heavyAttackDices) {
        super(name, description, img, level, startersEquipment, type, TreasureType.WEAPON);
        this.ranged = ranged;
        this.attackDices = attackDices;
        this.heavyAttackDices = heavyAttackDices;
    }
}
