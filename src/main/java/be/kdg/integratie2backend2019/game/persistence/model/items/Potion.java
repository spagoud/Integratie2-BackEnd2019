package be.kdg.integratie2backend2019.game.persistence.model.items;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table
public class Potion extends Item {
    private String effect;
    private int amount;

    public Potion(String name, String description, String img, int level, String type, String effect, int amount) {
        super(name, description, img, level, type, TreasureType.POTION);
        this.effect = effect;
        this.amount = amount;
    }
}
