package be.kdg.integratie2backend2019.game.persistence.model.characters;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table
public class Hero extends GameCharacter {
    private int maxHealthPoints;
    private int maxMagicPoints;
    private String type;
    private int inventorySpace;
    @OneToOne(fetch = FetchType.EAGER)
    private Equipment startEquipment;

    public Hero(String symbolImg, String name, String description, int armor, int move, int maxHealthPoints, int maxMagicPoints, String type, int inventorySpace, Equipment startEquipment) {
        super(symbolImg, name, description, armor, move);
        this.maxHealthPoints = maxHealthPoints;
        this.maxMagicPoints = maxMagicPoints;
        this.type = type;
        this.inventorySpace = inventorySpace;
        this.startEquipment = startEquipment;
    }
}

