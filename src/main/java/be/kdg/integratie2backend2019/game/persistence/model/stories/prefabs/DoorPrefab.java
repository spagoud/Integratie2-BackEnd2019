package be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class DoorPrefab {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToOne(cascade = CascadeType.MERGE)
    private TilePrefab tilePrefab1;
    @OneToOne(cascade = CascadeType.MERGE)
    private TilePrefab tilePrefab2;

    private boolean enabled;
    private boolean locked;

    public DoorPrefab(TilePrefab tilePrefab1, TilePrefab tilePrefab2, boolean enabled, boolean locked) {
        this.tilePrefab1 = tilePrefab1;
        this.tilePrefab2 = tilePrefab2;
        this.enabled = enabled;
        this.locked = locked;
    }

    public DoorPrefab(String name, TilePrefab tilePrefab1, TilePrefab tilePrefab2, boolean enabled, boolean locked) {
        this.name = name;
        this.tilePrefab1 = tilePrefab1;
        this.tilePrefab2 = tilePrefab2;
        this.enabled = enabled;
        this.locked = locked;
    }
}
