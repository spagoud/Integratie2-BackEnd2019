package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameMonsterRepository extends JpaRepository<GameMonster, Long> {
}
