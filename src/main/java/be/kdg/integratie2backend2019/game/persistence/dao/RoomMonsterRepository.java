package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomPrefab;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomMonster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomMonsterRepository extends JpaRepository<RoomMonster,Long> {
    RoomMonster findByName(String name);
}
