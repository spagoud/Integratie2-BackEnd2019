package be.kdg.integratie2backend2019.game.persistence.model.items;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Table
public class BoobyTrap extends Treasure {
    private String effect;

    public BoobyTrap(String name, String description, String img, int level, String effect) {
        super(name, description, img, level, TreasureType.BOOBYTRAP);
        this.effect = effect;
    }
}
