package be.kdg.integratie2backend2019.game.persistence.model.stories;

import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Goal {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Monster boss;
    private String description;

    public Goal(Monster boss, String description) {
        this.boss = boss;
        this.description = description;
    }
}
