package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameHeroRepository extends JpaRepository<GameHero,Long> {
}
