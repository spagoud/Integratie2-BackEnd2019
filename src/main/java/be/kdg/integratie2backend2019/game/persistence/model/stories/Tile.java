package be.kdg.integratie2backend2019.game.persistence.model.stories;

import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.TilePrefab;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class Tile {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private TileType tileType;
    private int x;
    private int y;
    private boolean pillar;
    private boolean chest;
    private TrapType trapType;
    private boolean door;


    public Tile(TilePrefab tilePrefab) {
        this.tileType = tilePrefab.getTileType();
        this.x = tilePrefab.getX();
        this.y = tilePrefab.getY();
        this.pillar = tilePrefab.isPillar();
        this.chest = tilePrefab.isChest();
        this.trapType = tilePrefab.getTrapType();
        this.door = tilePrefab.isDoor();
    }
}
