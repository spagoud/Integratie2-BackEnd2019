package be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table
public class RoomPrefab {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<TilePrefab> tilePrefabs;
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<RoomMonster> RoomMonsters;
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "RoomPrefab_doorPrefab",
            joinColumns = @JoinColumn(name = "roomPrefab_id"),
            inverseJoinColumns = @JoinColumn(name = "doorPrefab_id")
    )
    private List<DoorPrefab> doorPrefabs;
    private boolean startRoom;


    public RoomPrefab(String name) {
        this.name = name;
    }

    public RoomPrefab(String name, List<TilePrefab> tilePrefabs) {
        this.name = name;
        this.tilePrefabs = tilePrefabs;
        startRoom = false;
    }

    public RoomPrefab(String name, List<TilePrefab> tilePrefabs, List<DoorPrefab> doorPrefabs) {
        this.name = name;
        this.tilePrefabs = tilePrefabs;
        this.doorPrefabs = doorPrefabs;
        startRoom = false;
    }
    public RoomPrefab(String name, List<TilePrefab> tilePrefabs, List<DoorPrefab> doorPrefabs, boolean startRoom) {
        this.name = name;
        this.tilePrefabs = tilePrefabs;
        this.doorPrefabs = doorPrefabs;
        this.startRoom = startRoom;
    }
}
