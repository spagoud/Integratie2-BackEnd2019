package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.items.BoobyTrap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoobyTrapRepository extends JpaRepository<BoobyTrap, Long> {
    BoobyTrap findByName(String name);
}
