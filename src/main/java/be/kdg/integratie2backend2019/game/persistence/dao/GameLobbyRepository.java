package be.kdg.integratie2backend2019.game.persistence.dao;

import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameLobbyRepository extends JpaRepository<GameLobby,Long> {
    GameLobby findByName(String lobbyName);
}
