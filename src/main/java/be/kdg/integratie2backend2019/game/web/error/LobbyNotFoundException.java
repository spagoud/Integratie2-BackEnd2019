package be.kdg.integratie2backend2019.game.web.error;

public class LobbyNotFoundException extends RuntimeException {
    public LobbyNotFoundException() {
    }

    public LobbyNotFoundException(String message) {
        super(message);
    }

    public LobbyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public LobbyNotFoundException(Throwable cause) {
        super(cause);
    }

    public LobbyNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
