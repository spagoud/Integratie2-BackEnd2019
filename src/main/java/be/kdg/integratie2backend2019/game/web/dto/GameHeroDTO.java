package be.kdg.integratie2backend2019.game.web.dto;

import be.kdg.integratie2backend2019.userProfile.web.dto.UserProfileDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameHeroDTO {
    private Long id;
    private UserProfileDTO userProfile;
    private int magicPoints;
    private int healthPoints;
    private List<ItemDTO> itemList;
    private EquipmentDTO equipment;
    private int maxMagicPoints;
    private int maxHealthPoints;
    private String heroName;
    private String heroImage;
    private int inventorySpace;
    private String type;
    private TileDTO position;
    private int move;
}
