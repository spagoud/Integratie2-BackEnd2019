package be.kdg.integratie2backend2019.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JoinLobbyDTO {
  private String heroName;
  private String email;
}
