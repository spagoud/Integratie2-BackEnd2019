package be.kdg.integratie2backend2019.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameMonsterDTO {
    private Long id;
    private String symbolImg;
    private String name;
    private String description;
    private int armor;
    private int move;
    private int undeadScore;
    private int hitPoints;
    private int maxHitPoints;
    private TileDTO position;
}
