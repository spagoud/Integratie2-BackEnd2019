package be.kdg.integratie2backend2019.game.web.controller;

import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomPrefab;
import be.kdg.integratie2backend2019.game.web.dto.RoomDTO;
import be.kdg.integratie2backend2019.game.service.RoomService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/room")
public class RoomController {
    private final RoomService roomService;
    private final ModelMapper modelMapper;

    public RoomController(RoomService roomService, ModelMapper modelMapper) {
        this.roomService = roomService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/testRoom")
    public ResponseEntity<RoomDTO> loadRoom() throws Exception{
        RoomPrefab roomPrefab = roomService.getRoom("D1R1");
        return new ResponseEntity<>(modelMapper.map(roomPrefab, RoomDTO.class), HttpStatus.OK);
    }
}
