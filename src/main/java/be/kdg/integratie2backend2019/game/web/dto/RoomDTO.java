package be.kdg.integratie2backend2019.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomDTO {
    private String name;
    private List<TileDTO> tiles;
    private List<GameMonsterDTO> gameMonsters;
    private List<DoorDTO> doors;
    private boolean starterRoom;
}
