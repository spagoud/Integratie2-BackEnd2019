package be.kdg.integratie2backend2019.game.web.dto;

import be.kdg.integratie2backend2019.broadcast.messages.ActionMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReplayGameDTO {
    private String initialGameDataJson;
    private List<ActionMessage> actionMessages;
}
