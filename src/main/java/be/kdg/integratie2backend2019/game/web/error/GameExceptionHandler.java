package be.kdg.integratie2backend2019.game.web.error;

import be.kdg.integratie2backend2019.security.web.util.GenericResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GameExceptionHandler extends ResponseEntityExceptionHandler {

    public GameExceptionHandler() {
        super();
    }

    @ExceptionHandler({LobbyNotFoundException.class})
    public ResponseEntity<Object> handleLobbyNotFoundException(final LobbyNotFoundException ex){
        logger.error("400 Status Error", ex);
        final GenericResponse bodyOfResponse = new GenericResponse("The lobby with id: " + ex.getMessage() + "you are trying to reach does not exist");
        return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }


}
