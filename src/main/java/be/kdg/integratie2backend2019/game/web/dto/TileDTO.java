package be.kdg.integratie2backend2019.game.web.dto;


import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TileDTO {
    private Long id;
    private TileType tileType;
    private int x;
    private int y;
    private boolean pillar;
    private boolean chest;
    private TrapType trapType;
    private boolean door;
}
