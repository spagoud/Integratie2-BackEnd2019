package be.kdg.integratie2backend2019.game.web.dto;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import be.kdg.integratie2backend2019.userProfile.web.dto.UserProfileDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class GameLobbyDTO {
    private long Id;
    private String name;
    private String lobbyOwner;
    private List<LobbyHeroDTO> heroes;
    private List<String> viewers;
    private boolean inviteOnly;
    private int playerCount;
    private Game.GameStateType gameState;

    public GameLobbyDTO(GameLobby gameLobby) {
        this.Id = gameLobby.getId();
        this.name = gameLobby.getName();
        this.lobbyOwner = gameLobby.getLobbyOwner().getUsername();
        this.heroes = new ArrayList<>();
        for (GameHero hero :
                gameLobby.getGame().getHeroes()) {
            String username;
            byte[] profilePicture;
            if(hero.getHeroName().contains("AI")){
                username = "AI";
                profilePicture = new byte[]{};
            }
            else{
                username = hero.getUserProfile().getUsername();
                profilePicture = hero.getUserProfile().getProfilePicture();
            }
            LobbyHeroDTO lobbyHeroDTO = new LobbyHeroDTO(new UserProfileDTO(username, profilePicture), hero.getHeroName(), hero.getHeroImage(), hero.getType());
            this.heroes.add(lobbyHeroDTO);
        }
        this.viewers = new ArrayList<>();
        for (UserProfile viewer : gameLobby.getViewers()
        ) {
            this.viewers.add(viewer.getUsername());
        }
        this.inviteOnly = gameLobby.isInviteOnly();
        this.playerCount = gameLobby.getPlayerCount();
        this.gameState = gameLobby.getGame().getState();
    }
}
