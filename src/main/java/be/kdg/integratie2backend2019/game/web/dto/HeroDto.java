package be.kdg.integratie2backend2019.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HeroDto {
    private String name;
    private String symbolImg;
    private String description;
    private int armor;
    private int move;
    private int maxHealthPoints;
    private int maxMagicPoints;
    private String type;
    private int inventorySpace;
    private EquipmentDTO startEquipment;
}
