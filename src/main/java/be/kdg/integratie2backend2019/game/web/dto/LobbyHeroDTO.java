package be.kdg.integratie2backend2019.game.web.dto;

import be.kdg.integratie2backend2019.userProfile.web.dto.UserProfileDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LobbyHeroDTO {
  private UserProfileDTO userProfile;
  private String heroName;
  private String heroImage;
  private String type;
}
