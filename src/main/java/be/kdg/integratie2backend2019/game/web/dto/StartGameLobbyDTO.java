package be.kdg.integratie2backend2019.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StartGameLobbyDTO {
    private String name;
    private String lobbyOwner;
    private String heroName;
    private boolean inviteOnly;
}

