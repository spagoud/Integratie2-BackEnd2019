package be.kdg.integratie2backend2019.game.web.controller;

import be.kdg.integratie2backend2019.game.persistence.model.characters.Hero;
import be.kdg.integratie2backend2019.game.service.HeroService;
import be.kdg.integratie2backend2019.game.web.dto.HeroDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/hero")
public class HeroController {

    private HeroService heroService;
    private ModelMapper mapper;

    public HeroController(HeroService heroService, ModelMapper mapper) {
        this.heroService = heroService;
        this.mapper = mapper;
    }

    @GetMapping
    public ResponseEntity<List<HeroDto>> getAllHeroes(){
        List<Hero> heroes = heroService.getAll();
        List<HeroDto> heroDtos = new ArrayList<>();
        for (Hero hero: heroes) {
            heroDtos.add(mapper.map(hero, HeroDto.class));
        }
        return new ResponseEntity<>(heroDtos, HttpStatus.OK);
    }

    @GetMapping("/{heroname}")
    public ResponseEntity<HeroDto> getHeroByName(@PathVariable String heroname){
        HeroDto heroDto = mapper.map(heroService.getHeroByName(heroname), HeroDto.class);
        return new ResponseEntity<>(heroDto, HttpStatus.OK);
    }
}
