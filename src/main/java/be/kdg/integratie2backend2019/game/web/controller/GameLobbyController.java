package be.kdg.integratie2backend2019.game.web.controller;

import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.game.service.GameReplayService;
import be.kdg.integratie2backend2019.game.web.dto.GameLobbyDTO;
import be.kdg.integratie2backend2019.game.web.dto.JoinLobbyDTO;
import be.kdg.integratie2backend2019.game.web.dto.ReplayGameDTO;
import be.kdg.integratie2backend2019.game.web.dto.StartGameLobbyDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/gameLobby")
public class GameLobbyController {

    private final GameLobbyService gameLobbyService;
    private final BroadcastService broadcastService;
    private final GameReplayService gameReplayService;

    public GameLobbyController(GameLobbyService gameLobbyService, BroadcastService broadcastService, GameReplayService gameReplayService) {
        this.gameLobbyService = gameLobbyService;
        this.broadcastService = broadcastService;
        this.gameReplayService = gameReplayService;
    }

    @PostMapping("/joinLobby/{lobbyId}")
    public ResponseEntity<GameLobbyDTO> joinLobby(@PathVariable Long lobbyId, @RequestParam String email) {
        GameLobby gameLobby = gameLobbyService.addViewer(lobbyId, email);
        GameLobbyDTO gameLobbyDTO = new GameLobbyDTO(gameLobby);
        broadcastService.sendLobbyUpdate(lobbyId, gameLobbyDTO);
        return new ResponseEntity<>(gameLobbyDTO, HttpStatus.OK);
    }

    @PostMapping("/addPlayer/{lobbyId}")
    public ResponseEntity<GameLobbyDTO> addPlayer(@PathVariable Long lobbyId, @RequestBody JoinLobbyDTO joinLobbyDTO) {
        GameLobby gameLobby = gameLobbyService.addPlayer(lobbyId, joinLobbyDTO);
        GameLobbyDTO gameLobbyDTO = new GameLobbyDTO(gameLobby);
        broadcastService.sendLobbyUpdate(lobbyId, gameLobbyDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/removeViewer/{lobbyId}")
    public ResponseEntity<?> removeViewer(@PathVariable Long lobbyId, @RequestParam String email){
        GameLobby lobby = gameLobbyService.removeViewer(lobbyId,email);
        GameLobbyDTO gameLobbyDTO = new GameLobbyDTO(lobby);
        broadcastService.sendLobbyUpdate(lobbyId, gameLobbyDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/removeLobby/{lobbyId}")
    public ResponseEntity<?> removeLobby(@PathVariable Long lobbyId){
        gameLobbyService.removeLobby(lobbyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/newGameLobby")
    public ResponseEntity<GameLobbyDTO> newGameLobby(@RequestBody StartGameLobbyDTO startGameLobbyDTO) throws Exception{
        GameLobby lobby = gameLobbyService.createLobby(startGameLobbyDTO.getLobbyOwner(), startGameLobbyDTO.getName(),startGameLobbyDTO.isInviteOnly(), startGameLobbyDTO.getHeroName());
        GameLobbyDTO lobbyDTO = new GameLobbyDTO(lobby);
        return new ResponseEntity<>(lobbyDTO, HttpStatus.OK);
    }

    @GetMapping("/serverLobby")
    public ResponseEntity<List<GameLobbyDTO>> getAllLobbies() {
        List<GameLobby> lobbies = gameLobbyService.findAll();
        List<GameLobbyDTO> lobbyDTOS = new ArrayList<>();
        for (GameLobby lobby : lobbies) {
            lobbyDTOS.add(new GameLobbyDTO(lobby));
        }
        return new ResponseEntity<>(lobbyDTOS, HttpStatus.OK);
    }

    @GetMapping("/{lobbyId}")
    public ResponseEntity<GameLobbyDTO> getLobby(@PathVariable Long lobbyId) {
        GameLobby lobby = gameLobbyService.loadGameLobby(lobbyId);
        GameLobbyDTO lobbyDTO = new GameLobbyDTO(lobby);
        return new ResponseEntity<>(lobbyDTO, HttpStatus.OK);
    }

    @GetMapping("/replay/{lobbyId}")
    public ResponseEntity<?> getGameReplay(@PathVariable Long lobbyId){
        GameLobby lobby = gameLobbyService.loadGameLobby(lobbyId);
        ReplayGameDTO replayGameDTO = gameReplayService.getReplayGameDTO(lobby);
        return new ResponseEntity<>(replayGameDTO, HttpStatus.OK);
    }
}
