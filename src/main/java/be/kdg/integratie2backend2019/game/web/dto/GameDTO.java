package be.kdg.integratie2backend2019.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameDTO {
    private List<GameHeroDTO> heroes;
    private String storyDescription;
    private List<RoomDTO> visibleRooms;
}
