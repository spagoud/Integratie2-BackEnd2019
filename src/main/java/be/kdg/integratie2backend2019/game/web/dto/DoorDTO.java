package be.kdg.integratie2backend2019.game.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoorDTO {
    private Long id;
    private String name;
    private TileDTO tile1;
    private TileDTO tile2;
    private boolean enabled;
    private boolean locked;
}
