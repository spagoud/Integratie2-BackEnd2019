package be.kdg.integratie2backend2019.game.service;

import be.kdg.integratie2backend2019.game.persistence.dao.StoryRepository;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Story;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class StoryService {

    @Autowired
    private StoryRepository storyRepository;

    public Story getStory(int storyNumber) {
        return storyRepository.findByStoryNumber(storyNumber);
    }
}
