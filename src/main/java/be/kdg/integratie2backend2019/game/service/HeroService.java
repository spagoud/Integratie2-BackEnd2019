package be.kdg.integratie2backend2019.game.service;

import be.kdg.integratie2backend2019.game.persistence.dao.HeroRepository;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Hero;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HeroService {
    private HeroRepository heroRepository;

    public HeroService(HeroRepository heroRepository) {
        this.heroRepository = heroRepository;
    }

    public List<Hero> getAll(){
        return heroRepository.findAll();
    }

    public Hero getHeroByName(String name){
        return heroRepository.findByName(name);
    }
}
