package be.kdg.integratie2backend2019.game.service;

import be.kdg.integratie2backend2019.game.persistence.dao.GameHeroRepository;
import be.kdg.integratie2backend2019.game.persistence.dao.HeroRepository;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Hero;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class GameHeroService {
    private GameHeroRepository gameHeroRepository;
    private HeroRepository heroRepository;

    public GameHeroService(GameHeroRepository gameHeroRepository, HeroRepository heroRepository) {
        this.gameHeroRepository = gameHeroRepository;
        this.heroRepository = heroRepository;
    }

    public GameHero createGameHero(String heroName, UserProfile userProfile){
        Hero hero = heroRepository.findByName(heroName);
        GameHero gameHero = new GameHero(userProfile,hero.getMaxMagicPoints(),hero.getMaxHealthPoints(), hero.getArmor(),
                new ArrayList<>(),hero.getStartEquipment(),hero.getMaxMagicPoints(),hero.getMaxHealthPoints(),
                heroName,hero.getSymbolImg(),hero.getInventorySpace(),hero.getType(), hero.getMove());
        return gameHeroRepository.save(gameHero);
    }
}
