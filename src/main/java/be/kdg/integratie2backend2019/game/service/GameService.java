package be.kdg.integratie2backend2019.game.service;

import be.kdg.integratie2backend2019.game.persistence.dao.GameRepository;
import be.kdg.integratie2backend2019.game.persistence.dao.StoryRepository;
import be.kdg.integratie2backend2019.game.persistence.dao.TreasureRepository;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.items.Potion;
import be.kdg.integratie2backend2019.game.persistence.model.items.Treasure;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Story;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameService {

    private GameRepository gameRepository;
    private StoryRepository storyRepository;
    private GameHeroService gameHeroService;
    private TreasureRepository treasureRepository;


    public GameService(GameRepository gameRepository, StoryRepository storyRepository, GameHeroService gameHeroService, TreasureRepository treasureRepository) {
        this.gameRepository = gameRepository;
        this.storyRepository = storyRepository;
        this.gameHeroService = gameHeroService;
        this.treasureRepository = treasureRepository;
    }


    public Game createGame(int storyNumber, String heroName, UserProfile userProfile){
        Story story = storyRepository.findByStoryNumber(storyNumber);
        List<GameHero> gameHeroArrayList= new ArrayList<>();
        gameHeroArrayList.add(gameHeroService.createGameHero(heroName,userProfile));
        List<Treasure> treasures = treasureRepository.findAllByStartersEquipmentFalse();

        List<Treasure> potions = treasures.stream()
                .filter(treasure -> treasure.getTreasureType()
                        .equals(Treasure.TreasureType.POTION)).collect(Collectors.toList());
        for (Treasure treasure:potions) {
            Potion potion = (Potion) treasure;
            for (int i = 0; i < potion.getAmount() -1; i++) {
                treasures.add(potion);
            }
        }
        Collections.shuffle(treasures);
        return gameRepository.save(new Game(gameHeroArrayList, story, treasures));
    }

    public Game saveGame(Game game){
        game.setLastUpdated(new Date());
        return gameRepository.save(game);
    }

}
