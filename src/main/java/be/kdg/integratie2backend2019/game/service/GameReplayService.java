package be.kdg.integratie2backend2019.game.service;

import be.kdg.integratie2backend2019.broadcast.messages.*;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.web.dto.ItemDTO;
import be.kdg.integratie2backend2019.game.web.dto.ReplayGameDTO;
import be.kdg.integratie2backend2019.game.web.dto.RoomDTO;
import be.kdg.integratie2backend2019.game.web.dto.TileDTO;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameReplayService {
    private ModelMapper modelMapper;

    public GameReplayService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public ReplayGameDTO getReplayGameDTO(GameLobby lobby){
        Game game = lobby.getGame();
        ReplayGameDTO replayGameDTO = new ReplayGameDTO();
        replayGameDTO.setInitialGameDataJson(game.getInitialGameDataJson());
        replayGameDTO.setActionMessages(transformActionsToMessages(game.getActionsPerformed()));
        return replayGameDTO;
    }

    private List<ActionMessage> transformActionsToMessages(List<Action> actions){
        List<ActionMessage> messages = new ArrayList<>();
        for (Action action : actions) {
            switch (action.getActionType()){
                case DOOR:
                    DoorAction doorAction = (DoorAction) action;
                    messages.add(new DoorMessage(doorAction.getHeroName(),
                            modelMapper.map(doorAction.getRoom(), RoomDTO.class),
                            doorAction.getDoor().getId()));
                    break;
                case MOVE:
                    MoveAction moveAction = (MoveAction) action;
                    List<TileDTO> movementPath = moveAction.getMovementPath().stream()
                            .map(t -> modelMapper.map(t, TileDTO.class)).collect(Collectors.toList());
                    messages.add(new MoveMessage(moveAction.getHeroName(),movementPath));
                    break;
                case TRAP:
                    TrapAction trapAction = (TrapAction) action;
                    messages.add(new TrapMessage(trapAction.getHeroName(), trapAction.getTile(), trapAction.getDamage()));
                    break;
                case CHEST:
                    ChestAction chestAction = (ChestAction) action;
                    messages.add(new ChestMessage(chestAction.getHeroName(),
                            modelMapper.map(chestAction.getTreasure(), ItemDTO.class), chestAction.getDamage(),
                            modelMapper.map(chestAction.getTile(), TileDTO.class)));
                    break;
                case ATTACK:
                    AttackAction attackAction = (AttackAction) action;
                    messages.add(new AttackMessage(attackAction.getHeroName(),
                            attackAction.getTargetId(), attackAction.getDamage(), attackAction.isDead()));
                    break;
                case POTION:
                    PotionAction potionAction = (PotionAction) action;
                    List<ItemDTO> itemList = potionAction.getNewItemList().stream()
                            .map(i -> modelMapper.map(i, ItemDTO.class)).collect(Collectors.toList());
                    messages.add(new PotionMessage(potionAction.getHeroName(), potionAction.getNewHealth(),
                           itemList));
                    break;
                default:
                    break;
            }
        }

        return messages;
    }
}
