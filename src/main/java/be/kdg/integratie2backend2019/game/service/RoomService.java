package be.kdg.integratie2backend2019.game.service;

import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomPrefab;
import be.kdg.integratie2backend2019.game.persistence.dao.RoomPrefabRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoomService {

    @Autowired
    private RoomPrefabRepository roomPrefabRepository;

    public RoomPrefab getRoom(String name) {
        return roomPrefabRepository.findByName(name);
    }
}
