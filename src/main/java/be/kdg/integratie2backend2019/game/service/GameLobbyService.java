package be.kdg.integratie2backend2019.game.service;

import be.kdg.integratie2backend2019.game.persistence.dao.GameLobbyRepository;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.web.error.LobbyNotFoundException;
import be.kdg.integratie2backend2019.security.service.IUserService;
import be.kdg.integratie2backend2019.game.web.dto.JoinLobbyDTO;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GameLobbyService {
    private GameLobbyRepository gameLobbyRepository;
    private IUserService userService;
    private GameService gameService;
    private GameHeroService gameHeroService;



    public GameLobbyService(GameLobbyRepository gameLobbyRepository, IUserService userService, GameService gameService, GameHeroService gameHeroService) {
        this.gameLobbyRepository = gameLobbyRepository;
        this.userService = userService;
        this.gameService = gameService;
        this.gameHeroService = gameHeroService;
    }


    public GameLobby createLobby(String userMail, String lobbyName, boolean inviteOnly, String heroName){
        UserProfile userProfile = userService.findUserByEmail(userMail).getUserProfile();
        Game game = gameService.createGame(1, heroName, userProfile);
        return gameLobbyRepository.save(new GameLobby(lobbyName,userProfile, game, inviteOnly));
    }

    public GameLobby addViewer(Long lobbyId, String email){
        GameLobby lobby = loadGameLobby(lobbyId);
        UserProfile userProfile = userService.findUserByEmail(email).getUserProfile();
        if(!lobby.getViewers().contains(userProfile)){
            lobby.addViewer(userProfile);
        }
        return gameLobbyRepository.save(lobby);
    }

    public GameLobby removeViewer(Long lobbyId, String email){
        GameLobby lobby = loadGameLobby(lobbyId);
        UserProfile userProfile = userService.findUserByEmail(email).getUserProfile();
        lobby.removeViewer(userProfile);
        return gameLobbyRepository.save(lobby);
    }

    public void removeLobby(Long lobbyId){
        gameLobbyRepository.deleteById(lobbyId);
    }

    public GameLobby addPlayer(Long lobbyId, JoinLobbyDTO joinLobbyDTO){
        GameLobby lobby = loadGameLobby(lobbyId);
        Game game = lobby.getGame();

        UserProfile userProfile = userService.findUserByEmail(joinLobbyDTO.getEmail()).getUserProfile();
        game.addGameHero(gameHeroService.createGameHero(joinLobbyDTO.getHeroName(), userProfile));
        game = gameService.saveGame(game);
        lobby.removeViewer(userProfile);
        lobby.setGame(game);
        return gameLobbyRepository.save(lobby);
    }

    public GameLobby loadGameLobby(Long lobbyId){
        Optional<GameLobby> optionalLobby = gameLobbyRepository.findById(lobbyId);
        return optionalLobby.orElseThrow(() -> new LobbyNotFoundException(Long.toString(lobbyId)));
    }


    public List<GameLobby> findAll(){
        return gameLobbyRepository.findAll();
    }

    public GameLobby createGameLobby(GameLobby gameLobby) {
        return gameLobbyRepository.save(gameLobby);
    }
}
