package be.kdg.integratie2backend2019.game.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GameConfig {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
