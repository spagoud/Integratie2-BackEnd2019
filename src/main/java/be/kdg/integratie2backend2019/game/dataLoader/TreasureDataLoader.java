package be.kdg.integratie2backend2019.game.dataLoader;

import be.kdg.integratie2backend2019.game.persistence.dao.*;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Equipment;
import be.kdg.integratie2backend2019.game.persistence.model.characters.GameCharacter;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Hero;
import be.kdg.integratie2backend2019.game.persistence.model.characters.Monster;
import be.kdg.integratie2backend2019.game.persistence.model.dices.Dice;
import be.kdg.integratie2backend2019.game.persistence.model.items.*;
import be.kdg.integratie2backend2019.game.persistence.model.stories.*;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.DoorPrefab;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomPrefab;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.TilePrefab;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Bram Verlinden on 12/02/2019.
 * Fixed by Sam Laureys on 22/02/2019
 */
@Component
public class TreasureDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private DungeonRepository dungeonRepository;
    @Autowired
    private RoomPrefabRepository roomPrefabRepository;
    @Autowired
    private DoorPrefabRepository doorPrefabRepository;
    @Autowired
    private StoryRepository storyRepository;
    @Autowired
    private DiceRepository diceRepository;
    @Autowired
    private MonsterRepository monsterRepository;
    @Autowired
    private RoomMonsterRepository roomMonsterRepository;


    @Autowired
    private CharacterRepository gameCharacterRepository;
    @Autowired
    private TreasureRepository treasureRepository;
    @Autowired
    private HeroRepository heroRepository;

    @Autowired
    private EquipmentRepository equipmentRepository;

    private Dice attackDiceY;
    private Dice attackDiceY2;
    private Dice attackDiceO;
    private Dice attackDiceO2;
    private Dice attackDiceR;
    private Dice attackDiceP;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        attackDiceY = createDice("Yellow",0,1, new ArrayList<>(Arrays.asList(1,1,1,1,0,0)));
        attackDiceY2 = createDice("Yellow",0,1, new ArrayList<>(Arrays.asList(1,1,1,1,0,0)));
        attackDiceO = createDice("Orange",1,2, new ArrayList<>(Arrays.asList(1,1,1,1,2,2)));
        attackDiceO2 = createDice("Orange",1,2, new ArrayList<>(Arrays.asList(1,1,1,1,2,2)));
        attackDiceR = createDice("Red",0,3, new ArrayList<>(Arrays.asList(0,1,2,2,2,3)));
        attackDiceP = createDice("Purple", 2,3, new ArrayList<>(Arrays.asList(2,2,2,2,3,3)));

        Weapon BTD = (Weapon) createWeapon("Balanced Throwing Dagger",  "Perfectly balanced for deadly precision.", "image", 1,"Weapon",true, Arrays.asList(attackDiceY, attackDiceY2), Arrays.asList(attackDiceO,attackDiceO2));
        Weapon BotBK = (Weapon) createWeapon("Blade of the banished kings", "A finely crafted sword with elegant gilt that still bears witness to great craftsmanship.", "image", 1,"Weapon",false, Arrays.asList(attackDiceO),null);
        Weapon BBotE = (Weapon) createWeapon("Blessed bow of the elves", "Smaller and lighter than most bows, with an unprecedented elastic bowstring of elven thread.", "image", 1,"Weapon", true, Arrays.asList(attackDiceY,attackDiceO), null);
        Weapon BC = (Weapon) createWeapon("Bone cleaver", "A merciless cleaver that goes through everything.", "image", 1,"Weapon", false, Arrays.asList(attackDiceY,attackDiceO), Arrays.asList(attackDiceY,attackDiceO,attackDiceR));
        Weapon BoF = (Weapon) createWeapon("Bow of freedom", "A longbow with magical properties.", "image", 1, "Weapon", true, Arrays.asList(attackDiceY,attackDiceO), null);
        Weapon CoF = (Weapon) createWeapon("Crossbow of faith", "Reinforced with runes from Pelor to fly straight and far.", "image", 1,"Weapon", true, Arrays.asList(attackDiceY,attackDiceY2), null);
        Weapon DHB = (Weapon) createWeapon("Double-handed broadsword",  "A heavy blade forged from the strongest steel; requires great skill and physical strength.", "image", 1,"Weapon", false, Arrays.asList(attackDiceY,attackDiceY2,attackDiceO), null);
        Weapon FAoDK = (Weapon) createWeapon("Faithful axe of dwarven kind",  "A proven dwarf weapon, renowned for its accuracy", "image", 1,"Weapon", false, Arrays.asList(attackDiceY,attackDiceR), null);
        Weapon FP = (Weapon) createWeapon("Flash pellets",  "Explode with a storm of greenish-yellow sparks that cause light damage and can blind temporarily.", "image", 1,"Weapon", true, Arrays.asList(attackDiceY,attackDiceY2), null);
        Weapon HoL = (Weapon) createWeapon("Hammer of liberty",  "This weapon has a granite core and is made by dwarfs deep underground.", "image", 1,"Weapon", false, Arrays.asList(attackDiceO,attackDiceO2), null);
        Weapon MoF = (Weapon) createWeapon("Mace of faith",  "A heavy mace handing vicious blows.", "image", 1,"Weapon", false, Arrays.asList(attackDiceY,attackDiceO), null);
        Weapon PB = (Weapon) createWeapon("Phoenix bow",  "Fires arrows with Phoenix feathers that catch fire during the flight.", "image", 1,"Weapon", true, Arrays.asList(attackDiceY,attackDiceO), null);
        Weapon PBP = (Weapon) createWeapon("Poisoned blow pipe",  "Lightweight and effective, it fires arrows with the poison of the black rider scorpion.", "image", 1,"Weapon", true, Arrays.asList(attackDiceY,attackDiceO), null);
        Weapon SBotA = (Weapon) createWeapon("Short bow of the ancients",  "Cut from the oldest yew tree in the Old Elven Forest", "image", 1,"Weapon", true,Arrays.asList(attackDiceY,attackDiceY2), null);
        Weapon SHB = (Weapon) createWeapon("Single-handed broadsword",  "A light but powerful sword with excellent characteristics, ideal for man-to-man combat.", "image", 1,"Weapon", false,Arrays.asList(attackDiceO), null);
        Weapon TMA = (Weapon) createWeapon("The master's axe",  "With a double forged razor-sharp blade for chopping and cutting.", "image", 1,"Weapon", false, Arrays.asList(attackDiceY,attackDiceO), Arrays.asList(attackDiceY,attackDiceO,attackDiceO2));
        Weapon TSoS = (Weapon) createWeapon("Tortured sword of slavery",  "This sword, originally used by slave traders, contains the souls of the afflicted, who shout when it's used.", "image", 1,"Weapon", false, Arrays.asList(attackDiceY,attackDiceO,attackDiceO2), null);

        Spell BH = (Spell) createSpell("Burning hands",  "Roaring firestorms shoot out of your fingertips and burn everything they touch.", "image", 1,"Spell", 3,4,true,true, Arrays.asList(attackDiceO,attackDiceR), false, false);
        Spell FA = (Spell) createSpell("Flame arrow",   "Flaming arrows that keep burning when they hit their target.", "image", 1,"Spell", 3,4,false,true, Arrays.asList(attackDiceY,attackDiceY2,attackDiceR), false, false);
        Spell F = (Spell) createSpell("Forcecage",   "Pure energy forms a cage with which you attack and catch a Monster.", "image", 1,"Spell", 4,5,true,true, Arrays.asList(attackDiceY,attackDiceO,attackDiceR), false, false);
        Spell GR = (Spell) createSpell("Greater restoration",   "Bring a Hero back from the dead and restore to 4 Hit Points and to 4 Spell Points if possible. Stand next to dead Hero to restore them. SINGLE USE PER ADVENTURE. JOZAN ONLY.", "image", 1,"Spell", null, 4, true, false, null, true, false);
        Spell LHC = (Spell) createSpell("Lesser healing Circle",   "This spell is cast as protective shield over all the Heroes in the room. Give each Hero +2 Hit Point of healing.", "image", 1,"Spell", 4,2,true, true, null, false, false);
        Spell MM = (Spell) createSpell("Magic missile",   "Rays of pure energy, fired from the hand.", "image", 1,"Spell", 2,3,false,true, Arrays.asList(attackDiceY,attackDiceR), false, false);
        Spell MAA = (Spell) createSpell("Melf's acid arrow",   "A poisonous arrow that burns right through armor.", "image", 1,"Spell", 3,4,false,true,Arrays.asList(attackDiceO,attackDiceR), false, false);
        Spell RoF = (Spell) createSpell("Ray of frost",  "Casts an icy beam to its target.", "image", 1,"Spell", 3,4,false,true, Arrays.asList(attackDiceY,attackDiceR), false, false);
        Spell US = (Spell) createSpell("Unseen servant",   "A mysterious hand is invoked from above.", "image", 1,"Spell", 2,3,true,true, null, false, false);

        Artifact BaC = (Artifact) createArtifact("Barkskin cloak", "A light protective cloak sewn from the bark of magic trees from Arnholm.", "image", 1, "Artifact", "Wear this to take no damage from an attack or event, then roll die, if *star* discard cloak (NYI)");
        Artifact OA = (Artifact) createArtifact("Olidammara's amulet",  "Blessed with the wisdom of many.", "image", 1, "Artifact", "Let's the wearer use the 'search' die to look for traps (NYI)");
        Artifact RoS = (Artifact) createArtifact("Ring of shadows",  "Wear it to disappear into the background darkness.", "image", 1, "Artifact","Move anywhere in the room undetected, then roll die, *star* discard ring (NYI)");
        Artifact SH = (Artifact) createArtifact("Summoner's horn",  "Blow the horn to call for help.", "image", 1, "Artifact","Move any Hero to any space next to you, then roll die, *star* discard horn (NYI)");
        Artifact YA = (Artifact) createArtifact("Yondalla's amulet",  "Engraved with magic symbols.", "image", 1, "Artifact","When you open a chest, choose whether you keep the item. If not, discard it and take another treasure. Repeat up to 3 times. If a Booby trap is opened it activates and your turn ends (NYI)");

        Equipment seRegdar = createEquipment(SHB, null, null);
        Equipment seLidda = createEquipment(BTD, YA, null);
        Equipment seJozan = createEquipment(CoF, null, GR);
        Equipment seMialee = createEquipment(SBotA, null, MM);

        // amount = amount of potions needed of each type to be in the game
        Potion SHP = (Potion) createPotion("Small health potion","Drink this potion to replenish 1 health point", "image", 1, "Potion","HP +1", 7);
        Potion HP = (Potion) createPotion("Health potion","Drink this potion to replenish 2 health points", "image", 1, "Potion","HP +2", 3);

        //Deprecated
        BoobyTrap boobyTrap = (BoobyTrap) createBoobytrap("Boobytrap", "Every hero takes 1 damage", "image", 1, "HP -1 to all heroes");

        Hero regdar = (Hero) createHero("KnightImage", "Regdar", "Regdar is a champion of good, pledging his mighty sword to the cause of justice. He adventures to put down evil and to gain skill and experience that he can use in his crusade. Regdar's task on an adventuring team is to kill the Monsters and protect his teammates.", 2, 4, 8,0, "Human Knight", 4, seRegdar);
        Hero jozan = (Hero) createHero("ClericImage", "Jozan", "Jozan is a loyal follower of Pelor, a mighty sun god devoted to the cause of good and justice. Pelor expects Jozan to serve him by fighting evil monsters whenever he finds them. Jozan's role on an adventuring team is to protect his team-mates with spells and help kill the Monsters", 2, 5, 5, 5, "Human Cleric", 5, seJozan);
        Hero lidda = (Hero) createHero("RogueImage", "Lidda", "Lidda is a halfling. Shorter than people, they tend to be sneaky. Lidda is no exception. her task on an adventuring team is to handle tricky things, such as finding and disabling traps. In combat she does well if she can sneak attack her opponent", 2, 6,5,0,"Halfling Rogue", 4, seLidda);
        Hero mialee = (Hero) createHero("MageImage", "Mialee", "Mialee is still learning how to cast spells. Her goals is to become a master wizard, so she's always eager to test her magic against enemies. Mialee's responsibility on an adventuring team is to use her powerfull spells to support her teammates and help them deal with Monsters", 2, 5,5,5,"Elven Wizard", 5, seMialee);

        Dungeon dungeonOne = dungeonOne();
        Story story = storyOne(dungeonOne);

        Monster skeleton = createMonster("SkeletonImage","Skeleton", "Skeletons are undead monsters, bones brought to life that follow the orders of their evil master.", 1,5, 0, Arrays.asList(attackDiceY,attackDiceY,attackDiceO), null, 4);
        Monster bugbear = createMonster("BugbearImage", "Bugbear", "Bugbears are large, muscular and aggressive. They are capable of anything when it comes to satisfying their gluttony.", 2, 4, 0, Arrays.asList(attackDiceY,attackDiceY,attackDiceR), null, 7);

        RoomMonster roomMonster1 = createRoomMonster("D1R6M",skeleton,1);
        RoomMonster roomMonster2 = createRoomMonster("D1R4M",skeleton,2);
        RoomMonster roomMonster3 = createRoomMonster("D1R2M",skeleton,3);

        updateRoomWithMonsters(getRoom("D1R6"), new ArrayList<>(Arrays.asList(roomMonster1)));
        updateRoomWithMonsters(getRoom("D1R4"), new ArrayList<>(Arrays.asList(roomMonster2)));
        updateRoomWithMonsters(getRoom("D1R2"), new ArrayList<>(Arrays.asList(roomMonster3)));

    }

    private Dice createDice(String type, int minimumRoll, int maximumRoll, List<Integer> faces){
        Dice dice = new Dice(type,minimumRoll,maximumRoll, faces);
        return diceRepository.save(dice);
    }


    private Dungeon dungeonOne() {

        List<RoomPrefab> roomPrefabs = new ArrayList<>();
        List<TilePrefab> listTilesRoom1 = new ArrayList<>(Arrays.asList(
                new TilePrefab(TileType.TOPLEFT, 7, 0),
                new TilePrefab(TileType.TOP,  8, 0),
                new TilePrefab(TileType.TOP,  9, 0),
                new TilePrefab(TileType.TOPRIGHT,  10, 0),

                new TilePrefab(TileType.LEFT, 7, 1),
                new TilePrefab(TileType.FLOOR,  8, 1),
                new TilePrefab(TileType.FLOOR,  9, 1),
                new TilePrefab(TileType.RIGHT,  10, 1),

                new TilePrefab(TileType.LEFT,  7, 2),
                new TilePrefab(TileType.FLOOR,  8, 2),
                new TilePrefab(TileType.FLOOR,  9, 2),
                new TilePrefab(TileType.RIGHT,  10, 2),

                new TilePrefab(TileType.TOPLEFT,  4, 3, true, false),
                new TilePrefab(TileType.TOP,  5, 3),
                new TilePrefab(TileType.TOP,  6, 3),
                new TilePrefab(TileType.FLOOR,  7, 3),
                new TilePrefab(TileType.FLOOR,  8, 3),
                new TilePrefab(TileType.FLOOR,  9, 3, TrapType.PIT),
                new TilePrefab(TileType.RIGHT,  10, 3,false,false,true),

                new TilePrefab(TileType.LEFT, 4, 4),
                new TilePrefab(TileType.FLOOR,  5, 4),
                new TilePrefab(TileType.FLOOR,  6, 4),
                new TilePrefab(TileType.FLOOR,  7, 4, TrapType.PIT),
                new TilePrefab(TileType.FLOOR,  8, 4),
                new TilePrefab(TileType.FLOOR,  9, 4),
                new TilePrefab(TileType.RIGHT,  10, 4),

                new TilePrefab(TileType.LEFT, 4,5),
                new TilePrefab(TileType.FLOOR,  5,5),
                new TilePrefab(TileType.FLOOR, 6,5),
                new TilePrefab(TileType.FLOOR,  7,5),
                new TilePrefab(TileType.FLOOR,  8,5),
                new TilePrefab(TileType.FLOOR,  9,5, TrapType.PIT),
                new TilePrefab(TileType.RIGHT,  10,5),

                new TilePrefab(TileType.BOTTOMLEFT,  4,6, true, false),
                new TilePrefab(TileType.BOTTOM,  5,6),
                new TilePrefab(TileType.BOTTOM,  6,6),
                new TilePrefab(TileType.BOTTOM,  7,6,false,false,true),
                new TilePrefab(TileType.BOTTOM,  8,6),
                new TilePrefab(TileType.BOTTOM,  9,6),
                new TilePrefab(TileType.BOTTOMRIGHT,  10,6)
        ));

        List<TilePrefab> listTilesRoom2 = new ArrayList<>(Arrays.asList(
                new TilePrefab(TileType.TOPLEFT,  11,0),
                new TilePrefab(TileType.TOP,  12,0),
                new TilePrefab(TileType.TOP,  13,0),
                new TilePrefab(TileType.TOP,  14,0),
                new TilePrefab(TileType.TOP,  15,0),
                new TilePrefab(TileType.TOPRIGHT,  16,0, false,true),

                new TilePrefab(TileType.LEFT,  11,1, false,true),
                new TilePrefab(TileType.FLOOR,  12,1),
                new TilePrefab(TileType.FLOOR,  13,1),
                new TilePrefab(TileType.FLOOR,  14,1),
                new TilePrefab(TileType.FLOOR,  15,1),
                new TilePrefab(TileType.RIGHT,  16,1),

                new TilePrefab(TileType.LEFT,  11,2),
                new TilePrefab(TileType.FLOOR,  12,2),
                new TilePrefab(TileType.FLOOR,  13,2),
                new TilePrefab(TileType.FLOOR,  14,2),
                new TilePrefab(TileType.FLOOR,  15,2),
                new TilePrefab(TileType.RIGHT,  16,2, false,true),

                new TilePrefab(TileType.LEFT,  11,3,false,false,true),
                new TilePrefab(TileType.FLOOR,  12,3),
                new TilePrefab(TileType.FLOOR,  13,3),
                new TilePrefab(TileType.FLOOR,  14,3),
                new TilePrefab(TileType.FLOOR,  15,3),
                new TilePrefab(TileType.RIGHT,  16,3),

                new TilePrefab(TileType.LEFT,  11,4),
                new TilePrefab(TileType.FLOOR,  12,4),
                new TilePrefab(TileType.FLOOR,  13,4),
                new TilePrefab(TileType.FLOOR,  14,4),
                new TilePrefab(TileType.FLOOR,  15,4),
                new TilePrefab(TileType.RIGHT,  16,4, false,true),

                new TilePrefab(TileType.LEFT,  11,5, false, true),
                new TilePrefab(TileType.FLOOR,  12,5),
                new TilePrefab(TileType.FLOOR,  13,5),
                new TilePrefab(TileType.FLOOR,  14,5),
                new TilePrefab(TileType.FLOOR,  15,5),
                new TilePrefab(TileType.RIGHT,  16,5),

                new TilePrefab(TileType.BOTTOMLEFT,  11,6),
                new TilePrefab(TileType.BOTTOM,  12,6),
                new TilePrefab(TileType.BOTTOM,  13,6),
                new TilePrefab(TileType.BOTTOM,  14,6),
                new TilePrefab(TileType.BOTTOM,  15,6),
                new TilePrefab(TileType.BOTTOMRIGHT,  16,6, false,true)
        ));

        List<TilePrefab> listTilesRoom3 = new ArrayList<>(Arrays.asList(
                new TilePrefab(TileType.TOPLEFT,  17,0),
                new TilePrefab(TileType.TOP,  18,0),
                new TilePrefab(TileType.TOP,  19,0),
                new TilePrefab(TileType.TOP,  20,0),
                new TilePrefab(TileType.TOPRIGHT,  21,0),

                new TilePrefab(TileType.LEFT,  17,1),
                new TilePrefab(TileType.FLOOR,  18,1),
                new TilePrefab(TileType.FLOOR,  19,1),
                new TilePrefab(TileType.FLOOR,  20,1),
                new TilePrefab(TileType.RIGHT,  21,1),

                new TilePrefab(TileType.LEFT,  17,2),
                new TilePrefab(TileType.FLOOR,  18,2),
                new TilePrefab(TileType.FLOOR,  19,2),
                new TilePrefab(TileType.FLOOR,  20,2),
                new TilePrefab(TileType.RIGHT,  21,2),

                new TilePrefab(TileType.LEFT,  17,3),
                new TilePrefab(TileType.FLOOR,  18,3),
                new TilePrefab(TileType.FLOOR,  19,3),
                new TilePrefab(TileType.FLOOR,  20,3),
                new TilePrefab(TileType.RIGHT,  21,3),

                new TilePrefab(TileType.BOTTOMLEFT,  17,4),
                new TilePrefab(TileType.BOTTOM,  18,4),
                new TilePrefab(TileType.BOTTOM,  19,4),
                new TilePrefab(TileType.BOTTOM,  20,4,false,false,true),
                new TilePrefab(TileType.BOTTOMRIGHT,  21,4)
        ));

        List<TilePrefab> listTilesRoom4 = new ArrayList<>(Arrays.asList(
                new TilePrefab(TileType.TOPLEFT,  0,6, false,true),
                new TilePrefab(TileType.TOP,  1,6, false,true),
                new TilePrefab(TileType.TOP,  2,6),
                new TilePrefab(TileType.TOPRIGHT,  3,6),

                new TilePrefab(TileType.LEFT,  0,7),
                new TilePrefab(TileType.FLOOR,  1,7),
                new TilePrefab(TileType.FLOOR,  2,7),
                new TilePrefab(TileType.FLOOR,  3,7),
                new TilePrefab(TileType.TOP,  4,7),
                new TilePrefab(TileType.TOP,  5,7),
                new TilePrefab(TileType.TOP,  6,7),
                new TilePrefab(TileType.TOP,  7,7,false,false,true),
                new TilePrefab(TileType.TOP,  8,7),
                new TilePrefab(TileType.TOP,  9,7),
                new TilePrefab(TileType.TOPRIGHT,  10,7),

                new TilePrefab(TileType.LEFT,  0,8),
                new TilePrefab(TileType.FLOOR,  1,8),
                new TilePrefab(TileType.FLOOR,  2,8),
                new TilePrefab(TileType.FLOOR,  3,8),
                new TilePrefab(TileType.FLOOR,  4,8),
                new TilePrefab(TileType.FLOOR,  5,8),
                new TilePrefab(TileType.FLOOR,  6,8),
                new TilePrefab(TileType.FLOOR,  7,8),
                new TilePrefab(TileType.FLOOR,  8,8),
                new TilePrefab(TileType.FLOOR,  9,8),
                new TilePrefab(TileType.RIGHT, 10,8,false,false,true),

                new TilePrefab(TileType.LEFT,  0,9),
                new TilePrefab(TileType.FLOOR,  1,9),
                new TilePrefab(TileType.FLOOR,  2,9),
                new TilePrefab(TileType.FLOOR,  3,9),
                new TilePrefab(TileType.FLOOR,  4,9),
                new TilePrefab(TileType.FLOOR,  5,9),
                new TilePrefab(TileType.FLOOR,  6,9),
                new TilePrefab(TileType.FLOOR,  7,9),
                new TilePrefab(TileType.FLOOR,  8,9),
                new TilePrefab(TileType.FLOOR,  9,9),
                new TilePrefab(TileType.RIGHT,  10,9),

                new TilePrefab(TileType.BOTTOMLEFT,  0,10),
                new TilePrefab(TileType.BOTTOM,  1,10),
                new TilePrefab(TileType.BOTTOM,  2,10),
                new TilePrefab(TileType.BOTTOM,  3,10),
                new TilePrefab(TileType.BOTTOM,  4,10),
                new TilePrefab(TileType.BOTTOM,  5,10),
                new TilePrefab(TileType.BOTTOM,  6,10),
                new TilePrefab(TileType.BOTTOM,  7,10),
                new TilePrefab(TileType.BOTTOM,  8,10),
                new TilePrefab(TileType.BOTTOM,  9,10),
                new TilePrefab(TileType.BOTTOMRIGHT,  10,10)
        ));

        List<TilePrefab> listTilesRoom5 = new ArrayList<>(Arrays.asList(
                new TilePrefab(TileType.TOPLEFT,  11,7),
                new TilePrefab(TileType.TOP,  12,7,true,false),
                new TilePrefab(TileType.TOP,  13,7),
                new TilePrefab(TileType.TOP,  14,7),
                new TilePrefab(TileType.TOP,  15,7,true,false),
                new TilePrefab(TileType.TOPRIGHT,  16,7),

                new TilePrefab(TileType.LEFT,  11,8,false,false,true),
                new TilePrefab(TileType.FLOOR,  12,8),
                new TilePrefab(TileType.FLOOR,  13,8),
                new TilePrefab(TileType.FLOOR,  14,8),
                new TilePrefab(TileType.FLOOR,  15,8),
                new TilePrefab(TileType.RIGHT,  16,8,false,false,true),

                new TilePrefab(TileType.LEFT,  11,9),
                new TilePrefab(TileType.FLOOR,  12,9),
                new TilePrefab(TileType.FLOOR,  13,9),
                new TilePrefab(TileType.FLOOR,  14,9),
                new TilePrefab(TileType.FLOOR,  15,9),
                new TilePrefab(TileType.RIGHT,  16,9),

                new TilePrefab(TileType.BOTTOMLEFT,  11,10),
                new TilePrefab(TileType.BOTTOM,  12,10),
                new TilePrefab(TileType.BOTTOM,  13,10, false,true),
                new TilePrefab(TileType.BOTTOM,  14,10, false,true),
                new TilePrefab(TileType.BOTTOM, 15,10),
                new TilePrefab(TileType.BOTTOMRIGHT,  16,10)
        ));

        List<TilePrefab> listTilesRoom6 = new ArrayList<>(Arrays.asList(
                new TilePrefab(TileType.TOPLEFT,  17,5),
                new TilePrefab(TileType.TOP,  18,5),
                new TilePrefab(TileType.TOP,  19,5),
                new TilePrefab(TileType.TOP,  20,5,false,false,true),
                new TilePrefab(TileType.TOPRIGHT,  21,5),

                new TilePrefab(TileType.LEFT,  17,6),
                new TilePrefab(TileType.FLOOR,  18,6),
                new TilePrefab(TileType.FLOOR,  19,6),
                new TilePrefab(TileType.FLOOR,  20,6),
                new TilePrefab(TileType.RIGHT,  21,6),

                new TilePrefab(TileType.LEFT, 17,7),
                new TilePrefab(TileType.FLOOR,  18,7),
                new TilePrefab(TileType.FLOOR,  19,7),
                new TilePrefab(TileType.FLOOR,  20,7),
                new TilePrefab(TileType.RIGHT,  21,7),

                new TilePrefab(TileType.LEFT,  17,8,false,false,true),
                new TilePrefab(TileType.FLOOR,  18,8),
                new TilePrefab(TileType.FLOOR,  19,8),
                new TilePrefab(TileType.FLOOR,  20,8),
                new TilePrefab(TileType.RIGHT,  21,8),

                new TilePrefab(TileType.LEFT,  17,9),
                new TilePrefab(TileType.FLOOR,  18,9),
                new TilePrefab(TileType.FLOOR,  19,9),
                new TilePrefab(TileType.FLOOR,  20,9),
                new TilePrefab(TileType.RIGHT,  21,9),

                new TilePrefab(TileType.BOTTOMLEFT,  17,10),
                new TilePrefab(TileType.BOTTOM,  18,10),
                new TilePrefab(TileType.BOTTOM,  19,10),
                new TilePrefab(TileType.BOTTOM, 20,10),
                new TilePrefab(TileType.BOTTOMRIGHT,  21,10)
        ));

        RoomPrefab roomPrefab1 = createRoomIfNotFound("D1R1", listTilesRoom1);
        RoomPrefab roomPrefab2 = createRoomIfNotFound("D1R2", listTilesRoom2);
        RoomPrefab roomPrefab3 = createRoomIfNotFound("D1R3", listTilesRoom3);
        RoomPrefab roomPrefab4 = createRoomIfNotFound("D1R4", listTilesRoom4);
        RoomPrefab roomPrefab5 = createRoomIfNotFound("D1R5", listTilesRoom5);
        RoomPrefab roomPrefab6 = createRoomIfNotFound("D1R6", listTilesRoom6);

        roomPrefab3 = setStartRoom(roomPrefab3);

        DoorPrefab doorPrefab1 = createDoor("R1R2",getDoorTile(listTilesRoom1,10,3),getDoorTile(listTilesRoom2,11,3),false,false);
        DoorPrefab doorPrefab2 = createDoor("R1R4",getDoorTile(listTilesRoom1,7,6),getDoorTile(listTilesRoom4,7,7),false,false);
        DoorPrefab doorPrefab3 = createDoor("R4R5",getDoorTile(listTilesRoom4,10,8),getDoorTile(listTilesRoom5,11,8),false,false);
        DoorPrefab doorPrefab4 = createDoor("R5R6",getDoorTile(listTilesRoom5,16,8),getDoorTile(listTilesRoom6,17,8),false,false);
        DoorPrefab doorPrefab5 = createDoor("R3R6",getDoorTile(listTilesRoom3,20,4),getDoorTile(listTilesRoom6,20,5),false,false);

        List<DoorPrefab> doors1 = new ArrayList<>(Arrays.asList(doorPrefab1, doorPrefab2));
        List<DoorPrefab> doors2 = new ArrayList<>(Arrays.asList(doorPrefab1));
        List<DoorPrefab> doors3 = new ArrayList<>(Arrays.asList(doorPrefab5));
        List<DoorPrefab> doors4 = new ArrayList<>(Arrays.asList(doorPrefab2, doorPrefab3));
        List<DoorPrefab> doors5 = new ArrayList<>(Arrays.asList(doorPrefab3, doorPrefab4));
        List<DoorPrefab> doors6 = new ArrayList<>(Arrays.asList(doorPrefab5, doorPrefab4));

        updateRoom(roomPrefab1,doors1);
        updateRoom(roomPrefab2,doors2);
        updateRoom(roomPrefab3,doors3);
        updateRoom(roomPrefab4,doors4);
        updateRoom(roomPrefab5,doors5);
        updateRoom(roomPrefab6,doors6);

        roomPrefabs.add(roomPrefab1);
        roomPrefabs.add(roomPrefab2);
        roomPrefabs.add(roomPrefab3);
        roomPrefabs.add(roomPrefab4);
        roomPrefabs.add(roomPrefab5);
        roomPrefabs.add(roomPrefab6);

        return createDungeon("D1", roomPrefabs);
    }


    private RoomMonster createRoomMonster(String name, Monster monster, int amount) {
        RoomMonster roomMonster = roomMonsterRepository.findByName(name);
        if (roomMonster == null) {
            roomMonster = new RoomMonster(name, monster, amount);
        }
        return roomMonsterRepository.save(roomMonster);
    }


    private Monster createMonster(String symbolImg, String name, String description, int armor, int move, int undeadScore, List<Dice> meleeDices, List<Dice> rangedDices, int hitPoints) {
        Monster monster = monsterRepository.findByName(name);
        if (monster == null) {
            monster = new Monster(symbolImg, name, description,armor,move,undeadScore,meleeDices,rangedDices,hitPoints);
        }
        return monsterRepository.save(monster);
    }

//    public Monster getMonster(String monsterName){
//        return monsterRepository.findByName(monsterName);
//    }

    private RoomPrefab getRoom(String roomName){
        return roomPrefabRepository.findByName(roomName);
    }

    @Transactional
    public RoomPrefab createRoomIfNotFound(final String name, final List<TilePrefab> tilePrefabs) {
        RoomPrefab roomPrefab = roomPrefabRepository.findByName(name);
        if (roomPrefab == null) {
            roomPrefab = new RoomPrefab(name);
        }
        roomPrefab.setTilePrefabs(tilePrefabs);
        roomPrefab = roomPrefabRepository.save(roomPrefab);
        return roomPrefab;
    }

    @Transactional
    public Story createStoryIfNotFound(final int storyNumber, final String name, final String description, final Goal goal, final boolean goalreached, final Dungeon dungeon) {
        Story story = storyRepository.findByStoryNumber(storyNumber);
        if (story == null) {
            story = new Story(storyNumber, name, description, goal, goalreached, dungeon);
        }
        story = storyRepository.save(story);
        return story;
    }

    @Transactional
    public Dungeon createDungeon(String name, List<RoomPrefab> listRoomPrefabs) {
        Dungeon dungeon = dungeonRepository.findByName(name);
        if (dungeon == null) {
            dungeon = new Dungeon(name, listRoomPrefabs);
        }
        dungeon = dungeonRepository.save(dungeon);
        return dungeon;
    }

    @Transactional
    public DoorPrefab createDoor(String name, TilePrefab tilePrefab1, TilePrefab tilePrefab2, boolean enabled, boolean locked) {
        DoorPrefab doorPrefab = doorPrefabRepository.findByName(name);
        if (doorPrefab == null) {
            doorPrefab = new DoorPrefab(name, tilePrefab1, tilePrefab2, enabled, locked);
        }
        doorPrefab = doorPrefabRepository.save(doorPrefab);
        return doorPrefab;
    }


    @Transactional
    public Equipment createEquipment(Weapon weapon, Artifact artifact, Item extra) {
        Equipment equipment = equipmentRepository.findByWeapon(weapon);
        if(equipment == null) {
            equipment = new Equipment(weapon,artifact,extra);
        }
        return equipmentRepository.save(equipment);
    }

    @Transactional
    public GameCharacter createHero(String symbolImg, String name, String description, int armor, int move, int maxHealthPoints, int maxMagicPoints, String type, int inventorySpace, Equipment startEquipment){
        GameCharacter hero = heroRepository.findByName(name);
        if (hero == null) {
            hero = new Hero(symbolImg,name,description,armor,move,maxHealthPoints,maxMagicPoints,type,inventorySpace,startEquipment);
        }
        return gameCharacterRepository.save(hero);
    }

    @Transactional
    public Treasure createBoobytrap(String name, String description, String img, int level, String effect) {
        Treasure boobyTrap = treasureRepository.findByName(name);
        if (boobyTrap == null) {
            boobyTrap = new BoobyTrap(name,description,img,level, effect);
        }
        return treasureRepository.save(boobyTrap);
    }

    @Transactional
    public Treasure createPotion(String name, String description, String img, int level,String type,String effect, int amount) {
        Treasure potion = treasureRepository.findByName(name);
        if (potion == null) {
            potion = new Potion(name,description,img,level,type,effect,amount);
        }
        return treasureRepository.save(potion);
    }

    @Transactional
    public Treasure createArtifact(String name, String description, String img, int level,String type,String effect) {
        Treasure artifact = treasureRepository.findByName(name);
        if (artifact == null) {
            artifact = new Artifact(name,description,img,level,type,effect);
        }
        return treasureRepository.save(artifact);
    }

    @Transactional
    public Treasure createWeapon(String name, String description, String img, int level,String type, boolean ranged, List<Dice> attackDices, List<Dice> heavyAttackDices){
        Treasure weapon = treasureRepository.findByName(name);
        if (weapon == null) {
            weapon = new Weapon(name, description, img, level,false, type, ranged, attackDices, heavyAttackDices);
        }
        return treasureRepository.save(weapon);
    }

    @Transactional
    public Treasure createSpell(String name, String description, String img, int level, String type, Integer mageSpellCost, Integer clericSpellCost, boolean melee, boolean ranged, List<Dice> attackDices, boolean onDead, boolean starterEquiment){
        Treasure spell = treasureRepository.findByName(name);
        if (spell == null) {
            spell =  new Spell(name,description,img,level,starterEquiment,type,mageSpellCost,clericSpellCost,melee,ranged,attackDices,onDead);
        }
        return treasureRepository.save(spell);
    }

    @Transactional
    public RoomPrefab updateRoom(RoomPrefab roomPrefab, List<DoorPrefab> doorPrefabs) {
        roomPrefab.setDoorPrefabs(doorPrefabs);
        return roomPrefabRepository.save(roomPrefab);
    }

    @Transactional
    public RoomPrefab updateRoomWithMonsters(RoomPrefab roomPrefab, List<RoomMonster> roomMonsters) {
        roomPrefab.setRoomMonsters(roomMonsters);
        return roomPrefabRepository.save(roomPrefab);
    }

    @Transactional
    public RoomPrefab setStartRoom(RoomPrefab roomPrefab) {
        roomPrefab.setStartRoom(true);
        return roomPrefabRepository.save(roomPrefab);
    }

    private Story storyOne(Dungeon dungeon) {
        Goal goal = new Goal(null, "Defeat all the Goblins...");
        String name = "The Goblin Bandits";
        String description = "Unease and darkness have fallen over the land of Rallion as Monsters ravage the region. Travelling through it, the Heroes have arrived at the village of Holbrook, on the edge of a forest, where Goblin attacks have left the villagers fearing for their lives. The Sheriff of Holbrook has gone in search of them, but has not returned. The Goblins must be the key to his disappearance.";
        return createStoryIfNotFound(1, name, description, goal, false, dungeon);
    }

    private TilePrefab getDoorTile(List<TilePrefab> roomtiles, int x, int y){
        Optional<TilePrefab> optionalTile = roomtiles.stream().filter(t -> t.getX() == x).filter(t -> t.getY() == y).findAny();
        return optionalTile.orElse(null);
    }
}
