package be.kdg.integratie2backend2019.broadcast.service;

import be.kdg.integratie2backend2019.broadcast.error.MyServiceUnavailableException;
import be.kdg.integratie2backend2019.broadcast.messages.ActionMessage;
import be.kdg.integratie2backend2019.broadcast.messages.ChatMessage;
import be.kdg.integratie2backend2019.broadcast.service.proxy.BroadcastServiceProxy;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.web.dto.GameDTO;
import be.kdg.integratie2backend2019.game.web.dto.GameHeroDTO;
import be.kdg.integratie2backend2019.game.web.dto.GameLobbyDTO;
import be.kdg.integratie2backend2019.game.web.dto.RoomDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.util.ArrayList;
import java.util.List;

@Service
public class BroadcastService {

    private BroadcastServiceProxy broadcastServiceProxy;
    private ModelMapper modelMapper;
    private ObjectMapper objectMapper;

    public BroadcastService(BroadcastServiceProxy broadcastServiceProxy, ModelMapper modelMapper, ObjectMapper objectMapper) {
        this.broadcastServiceProxy = broadcastServiceProxy;
        this.modelMapper = modelMapper;
        this.objectMapper = objectMapper;
    }

    public void sendEvent(Long lobbyId, ActionMessage event){
        if(event == null){
            broadcastServiceProxy.broadcastEvent(lobbyId, "test message send from game backend over broadcaster to frontend");
        }
        try{
        String eventJson = objectMapper.writeValueAsString(event);
        broadcastServiceProxy.broadcastEvent(lobbyId, eventJson);
        }
        catch (Exception e){
            throw new MyServiceUnavailableException(e);
        }
    }

    public String sendGameUpdate(Long lobbyId, Game game){
        GameDTO gameDTO = new GameDTO();
        gameDTO.setStoryDescription(game.getStory().getDescription());
        List<GameHeroDTO> gameHeroDTOS = new ArrayList<>();
        for (GameHero hero : game.getHeroes()) {
            gameHeroDTOS.add(modelMapper.map(hero, GameHeroDTO.class));
        }
        gameDTO.setHeroes(gameHeroDTOS);
        List<RoomDTO> roomDTOS = new ArrayList<>();
        for (Room room : game.getVisibleRooms()) {
            roomDTOS.add(modelMapper.map(room, RoomDTO.class));
        }
        gameDTO.setVisibleRooms(roomDTOS);

        try{
            String gameUpdateJson = objectMapper.writeValueAsString(gameDTO);
            broadcastServiceProxy.broadcastGameUpdate(lobbyId, gameUpdateJson);
            return gameUpdateJson;
        }
        catch(Exception e){

            throw new MyServiceUnavailableException(e);
        }
    }

    public void sendChatMessage(Long lobbyId, ChatMessage chatMessage) {
        try{
            String chatMessageJson = objectMapper.writeValueAsString(chatMessage);
            broadcastServiceProxy.broadcastChatMessage(lobbyId, chatMessageJson);
        }
        catch(Exception e){
            throw new MyServiceUnavailableException(e);
        }
    }

    public void sendGameStart(Long lobbyId){
        broadcastServiceProxy.broadcastGameStart(lobbyId);
    }

    public void sendLobbyUpdate(Long lobbyId, GameLobbyDTO gameLobbyDTO) {
        try {
            String lobbyUpdateJson = objectMapper.writeValueAsString(gameLobbyDTO);
            broadcastServiceProxy.broadcastLobbyUpdate(lobbyId, lobbyUpdateJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    
    public void sendGameEnd(Long lobbyId, boolean goalReached){
        broadcastServiceProxy.broadcastGameEnd(lobbyId, goalReached);
    }

    // Not yet implemented
    public void sendGamePauzed(Long lobbyId){
        broadcastServiceProxy.broadcastGamePauzed(lobbyId);
    }
}
