package be.kdg.integratie2backend2019.broadcast.service.proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "broadcaster")
public interface BroadcastServiceProxy {

    @PostMapping("/broadcast/lobby/{lobbyId}/event")
    void broadcastEvent(@PathVariable(name = "lobbyId") Long lobbyId, @RequestBody String eventJson);

    @PostMapping("/broadcast/lobby/{lobbyId}/game")
    void broadcastGameUpdate(@PathVariable(name = "lobbyId") Long lobbyId, @RequestBody String gameUpdateJson);

    @PostMapping("/chat/sendMessage/{lobbyId}")
    void broadcastChatMessage(@PathVariable(name = "lobbyId") Long lobbyId, @RequestBody String chatMessageJson);

    @PostMapping("/broadcast/lobby/{lobbyId}/gamestart")
    void broadcastGameStart(@PathVariable(name = "lobbyId") Long lobbyId);

    @PostMapping("broadcast/lobby/{lobbyId}/gameend")
    void broadcastGameEnd(@PathVariable(name = "lobbyId") Long lobbyId, @RequestBody boolean goalReached);

    @PostMapping("broadcast/lobby/{lobbyId}/gamepauzed")
    void broadcastGamePauzed(@PathVariable(name = "lobbyId") Long lobbyId);

    @PostMapping("/broadcast/lobby/{lobbyId}/lobbyUpdate")
    void broadcastLobbyUpdate(@PathVariable(name = "lobbyId") Long lobbyId, @RequestBody String lobbyUpdateJson);
}
