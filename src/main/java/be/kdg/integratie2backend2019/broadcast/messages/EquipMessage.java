package be.kdg.integratie2backend2019.broadcast.messages;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class EquipMessage extends ActionMessage {
    private long equipedItem;
    private long otherItem;



    public EquipMessage(String actor) {
        super(actor, ActionType.EQUIP);
    }
}
