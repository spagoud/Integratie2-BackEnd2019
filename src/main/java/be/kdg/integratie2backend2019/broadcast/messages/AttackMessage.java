package be.kdg.integratie2backend2019.broadcast.messages;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AttackMessage extends ActionMessage{
    private long targedId;
    private int damage;
    private boolean dead;

    public AttackMessage(String actor, long targedId, int damage, boolean dead) {
        super(actor, ActionType.ATTACK);
        this.targedId = targedId;
        this.damage = damage;
        this.dead = dead;
    }
}
