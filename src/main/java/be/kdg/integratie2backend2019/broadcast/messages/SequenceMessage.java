package be.kdg.integratie2backend2019.broadcast.messages;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SequenceMessage extends ActionMessage {
    private List<String> playerSequence;

    public SequenceMessage(String actor, List<String> playerSequence) {
        super(actor, ActionType.TURNSEQUENCE);
        this.playerSequence = playerSequence;
    }
}
