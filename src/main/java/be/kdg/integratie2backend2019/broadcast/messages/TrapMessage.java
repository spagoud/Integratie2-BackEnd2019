package be.kdg.integratie2backend2019.broadcast.messages;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TrapMessage extends ActionMessage{
    private Tile tile;
    private int damage;

    public TrapMessage(String actor, Tile tile, int damage) {
        super(actor, ActionType.TRAP);
        this.tile = tile;
        this.damage = damage;
    }
}
