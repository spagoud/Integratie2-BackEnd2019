package be.kdg.integratie2backend2019.broadcast.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActionMessage {
    private String actor;
    private ActionType actionType;

    public enum ActionType{
        MOVE,
        DOOR,
        CHEST,
        TRAP,
        ATTACK,
        TRADE,
        TURN,
        TURNSEQUENCE,
        POTION,
        EQUIP
    }
}
