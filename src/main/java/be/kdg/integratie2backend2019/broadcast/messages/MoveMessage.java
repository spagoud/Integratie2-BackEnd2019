package be.kdg.integratie2backend2019.broadcast.messages;

import be.kdg.integratie2backend2019.game.web.dto.TileDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class MoveMessage extends ActionMessage{
    private List<TileDTO> movement;

    public MoveMessage(String actor, List<TileDTO> movement) {
        super(actor, ActionType.MOVE);
        this.movement = movement;
    }
}
