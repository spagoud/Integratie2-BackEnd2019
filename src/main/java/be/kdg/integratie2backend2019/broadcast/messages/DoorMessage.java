package be.kdg.integratie2backend2019.broadcast.messages;

import be.kdg.integratie2backend2019.game.web.dto.RoomDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = false)
public class DoorMessage extends ActionMessage{
    private RoomDTO newRoom;
    private Long doorId;

    public DoorMessage(String actor, RoomDTO newRoom, Long doorId) {
        super(actor, ActionType.DOOR);
        this.newRoom = newRoom;
        this.doorId = doorId;
    }
}
