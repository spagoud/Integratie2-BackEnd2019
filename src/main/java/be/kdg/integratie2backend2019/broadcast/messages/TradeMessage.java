package be.kdg.integratie2backend2019.broadcast.messages;

import be.kdg.integratie2backend2019.game.web.dto.ItemDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class TradeMessage extends ActionMessage {
    private String receiver;
    private List<ItemDTO> toSender;
    private List<ItemDTO> toReceiver;

    public TradeMessage(String actor, String receiver, List<ItemDTO> toSender, List<ItemDTO> toReceiver) {
        super(actor, ActionType.TRADE);
        this.receiver = receiver;
        this.toSender = toSender;
        this.toReceiver = toReceiver;
    }
}
