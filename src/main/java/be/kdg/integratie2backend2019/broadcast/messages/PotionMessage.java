package be.kdg.integratie2backend2019.broadcast.messages;

import be.kdg.integratie2backend2019.game.web.dto.ItemDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class PotionMessage extends ActionMessage {
    private int newHealth;
    private List<ItemDTO> itemList;

    public PotionMessage(String actor, int newHealth, List<ItemDTO> itemList) {
        super(actor, ActionType.POTION);
        this.newHealth = newHealth;
        this.itemList = itemList;
    }
}
