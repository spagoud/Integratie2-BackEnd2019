package be.kdg.integratie2backend2019.broadcast.messages;

import be.kdg.integratie2backend2019.game.web.dto.ItemDTO;
import be.kdg.integratie2backend2019.game.web.dto.TileDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
public class ChestMessage extends ActionMessage {
    private ItemDTO treasure;
    private TileDTO tile;
    private int damage;


    public ChestMessage(String actor, ItemDTO treasure, int damage, TileDTO tileDTO) {
        super(actor,ActionType.CHEST);
        this.treasure = treasure;
        this.damage = damage;
        this.tile = tileDTO;
    }
}
