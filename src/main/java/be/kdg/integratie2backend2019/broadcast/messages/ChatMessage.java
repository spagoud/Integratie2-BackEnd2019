package be.kdg.integratie2backend2019.broadcast.messages;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
public class ChatMessage{

    private String sender;
    private ChatMessageType chatMessageType;
    private String content;

    public ChatMessage(String sender, ChatMessageType chatMessageType, String content) {
        this.sender = sender;
        this.chatMessageType = chatMessageType;
        this.content = content;
    }

    public enum ChatMessageType {
        INFO,
        CHAT,
        JOIN,
        LEAVE
    }
}
