package be.kdg.integratie2backend2019.broadcast.error;

public class MyServiceUnavailableException extends RuntimeException{
    public MyServiceUnavailableException(Exception e) {
    }

    public MyServiceUnavailableException() {
    }

    public MyServiceUnavailableException(String message) {
        super(message);
    }

    public MyServiceUnavailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyServiceUnavailableException(Throwable cause) {
        super(cause);
    }

    public MyServiceUnavailableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
