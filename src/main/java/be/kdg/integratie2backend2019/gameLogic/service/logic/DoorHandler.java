package be.kdg.integratie2backend2019.gameLogic.service.logic;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Door;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DoorHandler {
    private PositionSetter positionSetter;

    public DoorHandler(PositionSetter positionSetter) {
        this.positionSetter = positionSetter;
    }

    public Room getNextRoom(Game game, Door door){

        Tile tileRoom1 = door.getTile1();
        Tile tileRoom2 = door.getTile2();

        List<Tile> allTiles = getAllTilesFromDungeon(game.getVisibleRooms());

        Room roomToBeLoaded;
        if(allTiles.contains(tileRoom1)){
            roomToBeLoaded = game.getDungeonRooms().stream().filter(x -> x.getTiles().contains(tileRoom2)).findFirst().get();
        }
        else{
            roomToBeLoaded = game.getDungeonRooms().stream().filter(x -> x.getTiles().contains(tileRoom1)).findFirst().get();
        }
        
        if(roomToBeLoaded.getGameMonsters().size() > 0)
            roomToBeLoaded = positionSetter.setMonstersOnRandomPosition(roomToBeLoaded);

        return roomToBeLoaded;
    }

    private List<Tile> getAllTilesFromDungeon(List<Room> visibleRooms){
        List<Tile> allTiles = new ArrayList<>();
        for (Room room: visibleRooms) {
            allTiles.addAll(room.getTiles());
        }
        return allTiles;
    }

}
