package be.kdg.integratie2backend2019.gameLogic.service.ai;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.AttackAction;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.MoveAction;
import be.kdg.integratie2backend2019.gameLogic.service.ActionService;
import be.kdg.integratie2backend2019.gameLogic.service.MovementService;
import be.kdg.integratie2backend2019.gameLogic.service.ai.movementAlgorithm.AlgorithmHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class PlayerAI {
    private AlgorithmHandler handler;
    private MovementService movementService;
    private ActionService actionService;
    private GameLobbyService gameLobbyService;

    public PlayerAI(AlgorithmHandler handler, MovementService movementService,
                    GameLobbyService gameLobbyService) {
        this.handler = handler;
        this.movementService = movementService;
        this.gameLobbyService = gameLobbyService;
    }

    @Autowired
    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }

    public void performTurn(String heroName, Game game, Long lobbyId){
        GameLobby lobby = gameLobbyService.loadGameLobby(lobbyId);
        GameHero gameHero = null;
        for (GameHero hero : game.getHeroes()) {
            if(hero.getHeroName().equals(heroName))
                gameHero = hero;
        }
        // Check if there is a target next to hero to perform attack on
        if(!gameHero.isDead()) {
            Long targetId = checkForTarget(gameHero, game);
            if (targetId != null) {
                AttackAction attackAction = new AttackAction(heroName, targetId);
                attackAction.setItemId(gameHero.getEquipment().getWeapon().getId());
                actionService.handleAction(attackAction, lobby);
            }
            // If no target is available the AI will move the character
            else {
                List<Tile> range = movementService.getRangePlayerMovement(lobbyId, heroName);
                List<Tile> path = handler.executePlayer(range, game, gameHero);
                path = alterPathIfLastTileIsOccupied(path, range, game, gameHero);
                MoveAction moveAction = new MoveAction(heroName, path);
                actionService.handleAction(moveAction, lobby);
            }
        }
    }

    private Long checkForTarget(GameHero hero, Game game){
        List<GameMonster> monsters = new ArrayList<>();
        for (Room room : game.getVisibleRooms()) {
            if(room.getGameMonsters() != null)
                monsters.addAll(room.getGameMonsters());
        }
        Tile location = hero.getPosition();
        for (GameMonster monster : monsters) {
            if (!monster.isDead()) {
                Tile monsterLocation = monster.getPosition();
                if (monsterLocation.getX() - 1 == location.getX() && monsterLocation.getY() == location.getY())
                    return monster.getId();
                else if (monsterLocation.getX() + 1 == location.getX() && monsterLocation.getY() == location.getY())
                    return monster.getId();
                else if (monsterLocation.getX() == location.getX() && monsterLocation.getY() - 1 == location.getY())
                    return monster.getId();
                else if (monsterLocation.getX() == location.getX() && monsterLocation.getY() + 1 == location.getY())
                    return monster.getId();
            }
        }
        return null;
    }

    private List<Tile> alterPathIfLastTileIsOccupied(List<Tile> path, List<Tile> range, Game game, GameHero hero){
        if(path.size() > 0) {
            Tile lastTile = path.get(path.size() - 1);
            List<GameHero> heroes = game.getHeroes();
            if (path.size() > 1) {
                if (isOccupied(lastTile, heroes, hero.getHeroName())) {
                    Tile secondToLastTile = path.get(path.size() - 2);
                    Optional<Tile> optionOne = range.stream().filter(t -> t.getX() == secondToLastTile.getX() - 1 && t.getY() == secondToLastTile.getY()).findFirst();
                    Optional<Tile> optionTwo = range.stream().filter(t -> t.getX() == secondToLastTile.getX() + 1 && t.getY() == secondToLastTile.getY()).findFirst();
                    Optional<Tile> optionThree = range.stream().filter(t -> t.getX() == secondToLastTile.getX() && t.getY() == secondToLastTile.getY() - 1).findFirst();
                    Optional<Tile> optionFour = range.stream().filter(t -> t.getX() == secondToLastTile.getX() && t.getY() == secondToLastTile.getY() + 1).findFirst();
                    Tile possibleTile;
                    path.remove(lastTile);
                    if (optionOne.isPresent()) {
                        possibleTile = optionOne.get();
                        if (possibleTile != lastTile && !isOccupied(possibleTile, heroes, hero.getHeroName()) && !path.contains(possibleTile)) {
                            path.add(possibleTile);
                            return path;
                        }

                    } else if (optionTwo.isPresent()) {
                        possibleTile = optionTwo.get();
                        if (possibleTile != lastTile && !isOccupied(possibleTile, heroes, hero.getHeroName()) && !path.contains(possibleTile)) {
                            path.add(possibleTile);
                            return path;
                        }

                    } else if (optionThree.isPresent()) {
                        possibleTile = optionThree.get();
                        if (possibleTile != lastTile && !isOccupied(possibleTile, heroes, hero.getHeroName()) && !path.contains(possibleTile)) {
                            path.add(possibleTile);
                            return path;
                        }

                    } else if (optionFour.isPresent()) {
                        possibleTile = optionFour.get();
                        if (possibleTile != lastTile && !isOccupied(possibleTile, heroes, hero.getHeroName()) && !path.contains(possibleTile)) {
                            path.add(possibleTile);
                            return path;
                        }
                    } else {
                        return path;
                    }
                }
            }
            return path;
        }
        return path;
    }

    private boolean isOccupied(Tile tile, List<GameHero> heroes, String heroName){
        for (GameHero hero : heroes) {
            if(!hero.getHeroName().equals(heroName)) {
                if (hero.getPosition() == tile)
                    return true;
            }
        }
        return false;
    }
}
