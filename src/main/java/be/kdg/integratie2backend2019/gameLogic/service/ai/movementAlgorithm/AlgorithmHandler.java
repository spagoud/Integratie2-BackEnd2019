package be.kdg.integratie2backend2019.gameLogic.service.ai.movementAlgorithm;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
public class AlgorithmHandler {

    private SquareGraph createMap(List<Tile> range, Tile character, Tile target, int movementRange){
        SquareGraph graph = new SquareGraph((movementRange*2) +1 );

        int offsetX = character.getX() - movementRange;
        int offsetY = character.getY() - movementRange;

        Node n;

        for (Tile tile : range) {
            String type;
            switch (tile.getTileType()){
                case TOP: type = "TOP"; break;
                case BOTTOM: type = "BOTTOM"; break;
                case LEFT: type = "LEFT"; break;
                case RIGHT: type = "RIGHT"; break;
                case TOPLEFT: type = "TOPLEFT"; break;
                case TOPRIGHT: type = "TOPRIGHT"; break;
                case BOTTOMLEFT: type = "BOTTOMLEFT"; break;
                case BOTTOMRIGHT: type = "BOTTOMRIGHT"; break;
                case FLOOR: type = "NORMAL"; break;
                default: type = "NORMAL"; break;

            }
            n = new Node(tile.getX() - offsetX, tile.getY() - offsetY, type, tile);
            graph.setMapCell(new Point(tile.getX() - offsetX, tile.getY() - offsetY), n);
        }
        graph.setStartPosition(new Point(character.getX() - offsetX, character.getY() - offsetY));
        graph.setTargetPosition(new Point(target.getX() - offsetX, target.getY() - offsetY));

        return graph;
    }

    public List<Tile> executePlayer(List<Tile> range, Game game, GameHero hero){
        // calculating a suitable monster as target, will return null if no suitable monster is found
        GameMonster monster = calculateMonsterTarget(range, game);
        Tile target;

        // if a suitable monster is found his position will be set as target
        if (monster != null) {
            target = monster.getPosition();
        }

        // if no suitable monster is found the target will be set to a random tile within its range
        else {
            Random random = new Random();
            int location = random.nextInt(range.size());
            target = range.get(location);
        }

        return calculatePath(range, hero.getPosition(), target, hero.getMove());
    }

    public List<Tile> executeMonster(List<Tile> range, Game game, GameMonster gameMonster){
        GameHero hero = calculateGameHeroTarget(range, game);
        Tile target;

        if(hero != null){
            target = hero.getPosition();
        }
        else{
            Random random = new Random();
            int location = random.nextInt(range.size());
            target = range.get(location);
        }

        return calculatePath(range, gameMonster.getPosition(), target, gameMonster.getMove());
    }

    private GameMonster calculateMonsterTarget(List<Tile> range, Game game){
        // determine all visible monsters that are alive
        List<GameMonster> visibleMonsters = new ArrayList<>();
        for (Room room : game.getVisibleRooms()) {
            if(room.getGameMonsters() != null) {
                for (GameMonster monster : room.getGameMonsters()) {
                    if (!monster.isDead()) {
                        visibleMonsters.add(monster);
                    }
                }
            }
        }
        // if there are no visible monsters null will be returned
        if(visibleMonsters.size() != 0) {

            // determine all possible candidates that are in range
            List<GameMonster> candidates = new ArrayList<>();
            for (GameMonster monster : visibleMonsters) {
                if (range.contains(monster.getPosition())) {
                    candidates.add(monster);
                }
            }

            //determine which candidate is the most suitable (lowest health or if health is equal => random)
            List<GameMonster> mostSuitable = new ArrayList<>();
            for (GameMonster monster : candidates) {
                if (mostSuitable.size() == 0) {
                    mostSuitable.add(monster);
                }
                else {
                    GameMonster suitable = mostSuitable.get(0);
                    if (suitable.getHitPoints() > monster.getHitPoints()) {
                        mostSuitable.clear();
                        mostSuitable.add(monster);
                    } else if (suitable.getHitPoints() == monster.getHitPoints()) {
                        mostSuitable.add(monster);
                    }
                }
            }

            if (mostSuitable.size() > 1) {
                Random random = new Random();

                return mostSuitable.get(random.nextInt(mostSuitable.size()));
            }
            else if(mostSuitable.size() == 0){
                return null;
            }
            else {
                return mostSuitable.get(0);
            }
        }
        return null;
    }

    private GameHero calculateGameHeroTarget(List<Tile> range, Game game){
        List<GameHero> heroes = game.getHeroes();

        // determine all possible candidates that are in range
        List<GameHero> candidates = new ArrayList<>();
        for (GameHero hero:heroes) {
            if(range.contains(hero.getPosition())){
                candidates.add(hero);
            }
        }

        if(candidates.size() == 0){
            return null;
        }
        else if(candidates.size() > 1){
            Random random = new Random();
            return candidates.get(random.nextInt(candidates.size()));
        }
        else{
            return candidates.get(0);
        }
    }

    private List<Tile> calculatePath(List<Tile> range, Tile startPosition, Tile target, int move){
        // generating the map used in the calculation of the shortest path
        SquareGraph graph = createMap(range, startPosition, target, move);

        // calculating shortest path (A*-algorithm)
        List<Node> nodePath = graph.executeAStar();

        // retrieving tiles from calculated path
        List<Tile> path = new ArrayList<>();
        for (Node node : nodePath) {
            path.add(node.getTile());
        }
        return path;
    }
}
