package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.broadcast.messages.AttackMessage;
import be.kdg.integratie2backend2019.broadcast.messages.ChatMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.dao.GameMonsterRepository;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.dices.Dice;
import be.kdg.integratie2backend2019.game.persistence.model.items.Weapon;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.AttackAction;
import be.kdg.integratie2backend2019.gameLogic.service.WeaponService;
import be.kdg.integratie2backend2019.gameLogic.web.errors.NotInRangeException;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
public class AttackHelper {

    private BroadcastService broadcastService;
    private WeaponService weaponService;
    private GameMonsterRepository gameMonsterRepository;
    private Random random = new Random();

    public AttackHelper(BroadcastService broadcastService, WeaponService weaponService, GameMonsterRepository gameMonsterRepository) {
        this.broadcastService = broadcastService;
        this.weaponService = weaponService;
        this.gameMonsterRepository = gameMonsterRepository;
    }

    public Game performAttack(AttackAction attackAction, Long lobbyId, Game game) {
        AttackMessage message;
        ChatMessage chatMessage;
        if(StringUtils.isNumeric(attackAction.getHeroName())){
            Long monsterId = Long.parseLong(attackAction.getHeroName());
            List<GameMonster> visibleMonsters = new ArrayList<>();
            for (Room room: game.getVisibleRooms()) {
                if(room.getGameMonsters().size()> 0){
                    visibleMonsters.addAll(room.getGameMonsters());
                }
            }
            GameMonster monster = visibleMonsters.stream().filter(m -> m.getId() == monsterId).findFirst().get();
            List<Dice> dices = monster.getMeleeDices();
            int damage = calculateDamage(dices);

            GameHero gameHero = game.getHeroes().stream().filter(h -> h.getId() == attackAction.getTargetId()).findFirst().get();
            damage = damage - gameHero.getArmor();
            if(damage < 0)
                damage = 0;
            gameHero.setHealthPoints(gameHero.getHealthPoints() - damage);
            if(gameHero.getHealthPoints() <= 0) {
                gameHero.setDead(true);
            }
            attackAction.setDamage(damage);
            attackAction.setDead(gameHero.isDead());
            message = new AttackMessage(attackAction.getHeroName(), attackAction.getTargetId(), damage, gameHero.isDead());
            chatMessage = new ChatMessage("Info", ChatMessage.ChatMessageType.INFO, monster.getName() + " damaged " + gameHero.getHeroName() + "for " + damage + " points");

            if(gameHero.isDead()){
                chatMessage = new ChatMessage("Info", ChatMessage.ChatMessageType.INFO, monster.getName() + " killed " + gameHero.getHeroName());
                broadcastService.sendChatMessage(lobbyId, chatMessage);
            }

        }
        else {
            Weapon weapon = weaponService.getWeapon(attackAction.getItemId());
            List<Dice> dices = weapon.getAttackDices();

            // Temporary workaround until ranged attack is implemented
            if (!weapon.isRanged() || weapon.isRanged()) {
                if (!nextToTargetForMelee(attackAction.getHeroName(), game, attackAction.getTargetId())) {
                    //TODO: error handling of andere manier van afhandelen?
                    throw new NotInRangeException();
                }
            }

            int damage = calculateDamage(dices);

            List<GameMonster> visibleMonsters = new ArrayList<>();
            GameMonster gameMonster = null;
            for (Room room : game.getVisibleRooms()) {
                if (room.getGameMonsters().size() > 0) {
                    visibleMonsters.addAll(room.getGameMonsters());
                }
            }
            Optional<GameMonster> optionalGameMonster = visibleMonsters.stream().filter(monster -> monster.getId().equals(attackAction.getTargetId())).findFirst();
            if (optionalGameMonster.isPresent()) {
                gameMonster = optionalGameMonster.get();
                damage = damage - gameMonster.getArmor();
                if (attackAction.getHeroName().equals("Regdar") || attackAction.getHeroName().equals("RegdarAI")) {
                    if (!weapon.isRanged()) {
                        damage += 1;
                    }
                }
                if(damage < 0)
                    damage = 0;
                gameMonster.setHitPoints(gameMonster.getHitPoints() - damage);
                if (gameMonster.getHitPoints() <= 0) {
                    gameMonster.setDead(true);
                }
            }
            attackAction.setDamage(damage);
            attackAction.setDead(gameMonster.isDead());
            message = new AttackMessage(attackAction.getHeroName(), attackAction.getTargetId(), damage, gameMonster.isDead());
            chatMessage = new ChatMessage("Info", ChatMessage.ChatMessageType.INFO, attackAction.getHeroName() + " damaged " + gameMonster.getName() + "for " + damage + " points");
            if(gameMonster.isDead()){
                chatMessage = new ChatMessage("Info", ChatMessage.ChatMessageType.INFO, attackAction.getHeroName() + " killed " + gameMonster.getName());
                broadcastService.sendChatMessage(lobbyId, chatMessage);
            }
        }

        broadcastService.sendChatMessage(lobbyId, chatMessage);
        broadcastService.sendEvent(lobbyId, message);
        game.addAction(attackAction);
        return game;
    }

    private boolean nextToTargetForMelee(String heroName, Game game, Long targetId){
        GameHero gameHero = game.getHeroes().stream().filter(hero -> hero.getHeroName().equals(heroName)).findFirst().orElse(null);
        GameMonster gameMonster = gameMonsterRepository.findById(targetId).orElse(null);
        boolean valid = false;

        if(gameMonster != null && gameHero != null){
            Tile heroTile = gameHero.getPosition();
            Tile monsterTile = gameMonster.getPosition();
            if(heroTile.getX()+1 == monsterTile.getX() && heroTile.getY() == monsterTile.getY()){
                valid = true;
            }
            if(heroTile.getX()-1 == monsterTile.getX() && heroTile.getY() == monsterTile.getY()){
                valid = true;
            }
            if(heroTile.getX() == monsterTile.getX() && heroTile.getY()+1 == monsterTile.getY()){
                valid = true;
            }
            if(heroTile.getX() == monsterTile.getX() && heroTile.getY()-1 == monsterTile.getY()){
                valid = true;
            }
        }
        return valid;
    }

    private int calculateDamage(List<Dice> dices){
        int damage = 0;
        for (Dice dice : dices) {
            damage += dice.getFaces().get(random.nextInt(6));
        }
        return damage;
    }
}
