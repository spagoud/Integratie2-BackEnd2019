package be.kdg.integratie2backend2019.gameLogic.service;


import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.service.GameHeroService;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.game.service.GameService;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.Action;
import be.kdg.integratie2backend2019.gameLogic.service.ai.DungeonMasterAI;
import be.kdg.integratie2backend2019.gameLogic.service.ai.PlayerAI;
import be.kdg.integratie2backend2019.gameLogic.service.logic.GameStartup;
import be.kdg.integratie2backend2019.gameLogic.service.logic.PositionSetter;
import be.kdg.integratie2backend2019.gameLogic.service.logic.TurnSequence;
import be.kdg.integratie2backend2019.userProfile.service.UserStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
public class GameLogicService {
    private BroadcastService broadcastService;
    private GameService gameService;
    private GameLobbyService lobbyService;
    private GameStartup gameStartup;
    private GameHeroService gameHeroService;
    private TurnSequence turnSequence;
    private PositionSetter positionSetter;
    private DungeonMasterAI dungeonMasterAI;
    private PlayerAI playerAI;
    private ActionService actionService;
    private GoalService goalService;
    private UserStatisticService userStatisticService;


    public GameLogicService(BroadcastService broadcastService, GameService gameService,
                            GameLobbyService lobbyService, GameStartup gameStartup,
                            GameHeroService gameHeroService, TurnSequence turnSequence,
                            PositionSetter positionSetter, UserStatisticService userStatisticService) {
        this.broadcastService = broadcastService;
        this.gameService = gameService;
        this.lobbyService = lobbyService;
        this.gameStartup = gameStartup;
        this.gameHeroService = gameHeroService;
        this.turnSequence = turnSequence;
        this.positionSetter = positionSetter;
        this.userStatisticService = userStatisticService;
    }

    @Autowired
    public void setPlayerAI(PlayerAI playerAI) {
        this.playerAI = playerAI;
    }

    @Autowired
    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }

    @Autowired
    public void setDungeonMasterAI(DungeonMasterAI dungeonMasterAI){
        this.dungeonMasterAI = dungeonMasterAI;
    }

    @Autowired
    public void setGoalService(GoalService goalService){
        this.goalService = goalService;
    }


    public void handleAction(Action action, long lobbyId){
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        Game game = actionService.handleAction(action,lobby);

        if (game.isLastMove()) {
            endTurn(lobbyId, action.getHeroName());
        } else {
            game.setLastMove(true);
        }
        gameService.saveGame(game);
        //check if goal has been reached or all players died
        goalService.checkGoal(game, lobbyId);
    }

    private void startGame(GameLobby lobby){
        // TODO: FE lobbyId is undefined bij non lobby owner!

        Game game = lobby.getGame();
        game = gameStartup.generateGameDungeon(game);

        List<GameHero> heroes = game.getHeroes();

        if(heroes.size()<4){
            heroes = addAICharacters(heroes);
        }

        List<String> players = turnSequence.generateTurnSequence(heroes, lobby.getId());

        StringBuilder builder = new StringBuilder();
        for (String player:players) {
            builder.append(player).append(";");
        }
        game.setTurnSequence(builder.toString());
        String player = players.get(0);

        game.setCurrentPlayer(player);
        heroes = positionSetter.setStartPosition(game.getHeroes(), game.getVisibleRooms());
        game.setHeroes(heroes);

        game.setGameStarted(new Date());
        game.setState(Game.GameStateType.ACTIVE);

        String initialGameDataJson = broadcastService.sendGameUpdate(lobby.getId(), game);
        game.setInitialGameDataJson(initialGameDataJson);
        gameService.saveGame(game);
        checkForAITurn(player, lobby.getId(), game);
    }

    public void endGame(Long lobbyId, boolean goalReached){
        broadcastService.sendGameEnd(lobbyId, goalReached);
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        Game game = lobby.getGame();

        game.setState(Game.GameStateType.ENDED);

        Date start = game.getGameStarted();
        Date end = new Date();
        long diffInMillies = Math.abs(end.getTime() - start.getTime());
        long timePlayedInMinutes = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
        long totalTimePlayed = timePlayedInMinutes + (long) game.getTimePlayed();
        game.setTimePlayed((int) totalTimePlayed);

        // TODO: determine new statistics per player
        gameService.saveGame(game);
        userStatisticService.updateStatistics(game);
    }

    public void pauzeGame(Long lobbyId){
        // TODO: broadcast pauze game to players + persist current game for continuation
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        Game game = lobby.getGame();
        game.setState(Game.GameStateType.PAUZED);
        //TODO: update time played
        gameService.saveGame(game);
    }

    public void continueGame(GameLobby lobby){
        Game game = lobby.getGame();
        game.setState(Game.GameStateType.ACTIVE);
        game.setGameStarted(new Date());
        gameService.saveGame(game);
    }

    public void endTurn(long lobbyId, String player){
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        Game game = lobby.getGame();
        player = turnSequence.getNextPlayer(player, game, lobby.getId());
        game.setCurrentPlayer(player);
        game.setLastMove(false);
        gameService.saveGame(game);
        checkForAITurn(player, lobbyId, game);
    }

    public void checkForAITurn(String player, Long lobbyId, Game game){
        if(player.equals("JozanAI") || player.equals("RegdarAI") || player.equals("LiddaAI") || player.equals("MialeeAI")){
            playerAI.performTurn(player, game, lobbyId);
            endTurn(lobbyId, player);
        }
        else if(player.equals("DungeonMaster")){
            dungeonMasterAI.performTurn(game, lobbyId);
            endTurn(lobbyId, player);
        }
    }

    private List<GameHero> addAICharacters(List<GameHero> heroes){
        List<String> heroNames = new ArrayList<>();
        for (GameHero hero:heroes) {
            heroNames.add(hero.getHeroName());
        }

        if(!heroNames.contains("Mialee")){
            GameHero mialeeAI = gameHeroService.createGameHero("Mialee", null);
            mialeeAI.setHeroName("MialeeAI");
            heroes.add(mialeeAI);
        }
        if(!heroNames.contains("Regdar")){
            GameHero regdarAI = gameHeroService.createGameHero("Regdar", null);
            regdarAI.setHeroName("RegdarAI");
            heroes.add(regdarAI);
        }
        if(!heroNames.contains("Jozan")){
            GameHero jozanAI = gameHeroService.createGameHero("Jozan", null);
            jozanAI.setHeroName("JozanAI");
            heroes.add(jozanAI);
        }
        if(!heroNames.contains("Lidda")){
            GameHero liddaAI = gameHeroService.createGameHero("Lidda", null);
            liddaAI.setHeroName("LiddaAI");
            heroes.add(liddaAI);
        }
        return heroes;
    }

    public void setPlayerAmount(GameLobby lobby) {
        // broadcast to all players that the game is starting
        broadcastService.sendGameStart(lobby.getId());
        Game game = lobby.getGame();
        game.setPlayerNotReadyAmount(
                lobby.getGame().getHeroes().stream().filter(gH -> gH.getUserProfile() != null).count()
                + lobby.getViewers().size());
        gameService.saveGame(game);
    }

    public void confirmGame(GameLobby lobby) {
        Game game = lobby.getGame();
        Long notReady = game.getPlayerNotReadyAmount()-1;
        game.setPlayerNotReadyAmount(notReady);
        if (notReady == 0){
            startGame(lobby);
        }
        gameService.saveGame(game);
    }
}
