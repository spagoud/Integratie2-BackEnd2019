package be.kdg.integratie2backend2019.gameLogic.service;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoalService {
    private GameLogicService gameLogicService;

    public GoalService() {
    }

    @Autowired
    public void setGameLogicService(GameLogicService gameLogicService) {
        this.gameLogicService = gameLogicService;
    }

    public void checkGoal(Game game, Long lobbyId){
        boolean allHeroesDead = allHeroesDead(game.getHeroes());
        boolean allMonstersDead = allMonstersDead(game.getDungeonRooms());
        if(allHeroesDead)
            gameLogicService.endGame(lobbyId, false);
        else if(allMonstersDead)
            gameLogicService.endGame(lobbyId, true);

    }

    private boolean allHeroesDead(List<GameHero> heroes){
        int counter = 0;
        for (GameHero hero : heroes) {
            if(hero.isDead())
                counter++;
        }
        return counter == 4;
    }

    private boolean allMonstersDead(List<Room> dungeonRooms){
        int total = 0;
        int dead = 0;
        for (Room dungeonRoom : dungeonRooms) {
            if (dungeonRoom.getGameMonsters() != null){
                for (GameMonster monster : dungeonRoom.getGameMonsters()) {
                    total ++;
                    if(monster.isDead())
                        dead++;
                }
            }
        }
        return total == dead;
    }
}
