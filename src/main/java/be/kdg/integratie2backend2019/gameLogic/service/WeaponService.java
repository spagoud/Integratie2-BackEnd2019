package be.kdg.integratie2backend2019.gameLogic.service;

import be.kdg.integratie2backend2019.game.persistence.dao.WeaponRepository;
import be.kdg.integratie2backend2019.game.persistence.model.items.Weapon;
import org.springframework.stereotype.Service;

@Service
public class WeaponService {
    private WeaponRepository weaponRepository;

    public WeaponService(WeaponRepository weaponRepository) {
        this.weaponRepository = weaponRepository;
    }

    public Weapon getWeapon(Long weaponId){
        return weaponRepository.findById(weaponId).get();
    }

}
