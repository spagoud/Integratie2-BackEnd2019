package be.kdg.integratie2backend2019.gameLogic.service.ai;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.AttackAction;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.MoveAction;
import be.kdg.integratie2backend2019.gameLogic.service.ActionService;
import be.kdg.integratie2backend2019.gameLogic.service.MovementService;
import be.kdg.integratie2backend2019.gameLogic.service.ai.movementAlgorithm.AlgorithmHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class DungeonMasterAI {

    private AlgorithmHandler handler;
    private MovementService movementService;
    private ActionService actionService;
    private GameLobbyService gameLobbyService;

    public DungeonMasterAI(AlgorithmHandler handler, MovementService movementService, GameLobbyService gameLobbyService) {
        this.handler = handler;
        this.movementService = movementService;
        this.gameLobbyService = gameLobbyService;
    }

    @Autowired
    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }

    public void performTurn(Game game, Long lobbyId){
        GameLobby lobby = gameLobbyService.loadGameLobby(lobbyId);
        List<GameMonster> visibleMonsters = new ArrayList<>();
        for (Room room: game.getVisibleRooms()) {
            if(room.getGameMonsters() != null){
                visibleMonsters.addAll(room.getGameMonsters());
            }
        }
        for (GameMonster monster : visibleMonsters) {
            if(!monster.isDead()){
                Long heroId = checkForTarget(monster, game);
                if(heroId != null){
                    AttackAction attackAction = new AttackAction(Long.toString(monster.getId()), heroId);
                    actionService.handleAction(attackAction, lobby);
                }
                List<Tile> range = movementService.getRangeMonsterMovement(lobbyId, monster);
                List<Tile> path = handler.executeMonster(range, game, monster);
                path = alterPathIfLastTileIsOccupied(path, range, game, monster);
                MoveAction moveAction = new MoveAction(Long.toString(monster.getId()), path, true);
                actionService.handleAction(moveAction, lobby);
            }
        }

    }

    private Long checkForTarget(GameMonster monster, Game game){
        List<GameHero> heroes = game.getHeroes();
        Tile location = monster.getPosition();
        for (GameHero hero : heroes) {
            if (!hero.isDead()) {
                Tile heroLocation = hero.getPosition();
                if (heroLocation.getX() - 1 == location.getX() && heroLocation.getY() == location.getY())
                    return hero.getId();
                else if (heroLocation.getX() + 1 == location.getX() && heroLocation.getY() == location.getY())
                    return hero.getId();
                else if (heroLocation.getX() == location.getX() && heroLocation.getY() - 1 == location.getY())
                    return hero.getId();
                else if (heroLocation.getX() == location.getX() && heroLocation.getY() + 1 == location.getY())
                    return hero.getId();
            }
        }
        return null;
    }

    private List<Tile> alterPathIfLastTileIsOccupied(List<Tile> path, List<Tile> range, Game game, GameMonster monster){
        if(path.size() != 0) {
            Tile lastTile = path.get(path.size() - 1);
            List<GameMonster> visibleMonsters = new ArrayList<>();
            for (Room room : game.getVisibleRooms()) {
                if (room.getGameMonsters() != null) {
                    visibleMonsters.addAll(room.getGameMonsters());
                }
            }
            if (path.size() > 1) {
                if (isOccupied(lastTile, visibleMonsters, monster.getId())) {
                    Tile secondToLastTile = path.get(path.size() - 2);
                    Optional<Tile> optionOne = range.stream().filter(t -> t.getX() == secondToLastTile.getX() - 1 && t.getY() == secondToLastTile.getY()).findFirst();
                    Optional<Tile> optionTwo = range.stream().filter(t -> t.getX() == secondToLastTile.getX() + 1 && t.getY() == secondToLastTile.getY()).findFirst();
                    Optional<Tile> optionThree = range.stream().filter(t -> t.getX() == secondToLastTile.getX() && t.getY() == secondToLastTile.getY() - 1).findFirst();
                    Optional<Tile> optionFour = range.stream().filter(t -> t.getX() == secondToLastTile.getX() && t.getY() == secondToLastTile.getY() + 1).findFirst();
                    Tile possibleTile;
                    path.remove(lastTile);
                    if (optionOne.isPresent()) {
                        possibleTile = optionOne.get();
                        if (possibleTile != lastTile && !isOccupied(possibleTile, visibleMonsters, monster.getId()) && !path.contains(possibleTile)) {
                            path.add(possibleTile);
                            return path;
                        }

                    } else if (optionTwo.isPresent()) {
                        possibleTile = optionTwo.get();
                        if (possibleTile != lastTile && !isOccupied(possibleTile, visibleMonsters, monster.getId()) && !path.contains(possibleTile)) {
                            path.add(possibleTile);
                            return path;
                        }

                    } else if (optionThree.isPresent()) {
                        possibleTile = optionThree.get();
                        if (possibleTile != lastTile && !isOccupied(possibleTile, visibleMonsters, monster.getId()) && !path.contains(possibleTile)) {
                            path.add(possibleTile);
                            return path;
                        }

                    } else if (optionFour.isPresent()) {
                        possibleTile = optionFour.get();
                        if (possibleTile != lastTile && !isOccupied(possibleTile, visibleMonsters, monster.getId()) && !path.contains(possibleTile)) {
                            path.add(possibleTile);
                            return path;

                        }
                    } else {
                        return path;
                    }
                }
            }
            return path;
        }
        return path;
    }

    private boolean isOccupied(Tile tile, List<GameMonster> monsters, Long monsterId){
        for (GameMonster monster : monsters) {
            if(!monster.getId().equals(monsterId)) {
                if (monster.getPosition() == tile)
                    return true;
            }
        }
        return false;
    }
}
