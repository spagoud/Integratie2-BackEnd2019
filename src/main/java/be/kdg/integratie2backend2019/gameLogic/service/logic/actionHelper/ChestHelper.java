package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.broadcast.messages.ChatMessage;
import be.kdg.integratie2backend2019.broadcast.messages.ChestMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.items.BoobyTrap;
import be.kdg.integratie2backend2019.game.persistence.model.items.Item;
import be.kdg.integratie2backend2019.game.persistence.model.items.Treasure;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.web.dto.ItemDTO;
import be.kdg.integratie2backend2019.game.web.dto.TileDTO;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.ChestAction;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChestHelper {

    private ModelMapper mapper;
    private BroadcastService broadcastService;

    public ChestHelper(ModelMapper mapper, BroadcastService broadcastService) {
        this.mapper = mapper;
        this.broadcastService = broadcastService;
    }

    /**
     * - check if it's player's turn
     * - check if he has 1 or 2 actions left to perform
     * - call action service
     * - check if character is on chest tile
     * - get random treasure
     * - update character
     * - broadcast result of action
     * - persist action and link to game
     *
     * @param chestAction
     * @param lobbyId
     * @param game
     * @return updated and persisted Game object
     */
    public Game openChest(ChestAction chestAction, Long lobbyId, Game game) {
        List<GameHero> heroes = game.getHeroes();
        GameHero gameHero = heroes.stream().filter(hero -> hero.getHeroName().equals(chestAction.getHeroName())).findFirst().get();
        List<Treasure> treasures = game.getAvailableTreasures();
        List<Item> items = gameHero.getItemList();

        gameHero.setItemList(items);
        Treasure treasure = treasures.remove(0);
        chestAction.setTreasure(treasure);
        game.setAvailableTreasures(treasures);
        chestAction.setTreasure(treasure);
        Tile chestTile = chestAction.getTile();
        chestTile.setChest(false);
        TileDTO tileDTO =  mapper.map(chestAction.getTile(),TileDTO.class);
        ItemDTO itemDTO = mapper.map(treasure,ItemDTO.class);
        ChestMessage message = new ChestMessage(gameHero.getHeroName(),itemDTO,0,tileDTO);

        if(treasure.getTreasureType().equals(Treasure.TreasureType.BOOBYTRAP)){
            BoobyTrap boobyTrap = (BoobyTrap) treasure;
            switch (boobyTrap.getEffect()){
                case "HP -1 to all heroes" :
                    message.setDamage(1);
                    for (GameHero hero:heroes) {
                        hero.setHealthPoints(hero.getHealthPoints() -1);
                    }
                    game.setHeroes(heroes);
                    chestAction.setDamage(1);
                    break;
            }
        }

        broadcastService.sendEvent(lobbyId, message);
        ChatMessage chatMessage = new ChatMessage("Info", ChatMessage.ChatMessageType.INFO, gameHero.getHeroName() + " found " + treasure.getName());
        broadcastService.sendChatMessage(lobbyId, chatMessage);
        game.addAction(chestAction);
        return game;
    }

}
