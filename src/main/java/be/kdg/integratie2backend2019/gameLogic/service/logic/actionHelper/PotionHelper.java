package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.broadcast.messages.PotionMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.web.dto.ItemDTO;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.PotionAction;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class PotionHelper {

    private ModelMapper mapper;
    private BroadcastService broadcastService;

    public PotionHelper(ModelMapper mapper, BroadcastService broadcastService) {
        this.mapper = mapper;
        this.broadcastService = broadcastService;
    }

    /**
     * @param potionAction
     * @param lobbyId
     * @param game
     * @return updated and persisted Game object
     */
    public Game usePotion(PotionAction potionAction, Long lobbyId, Game game) {
        String effect = potionAction.getPotion().getEffect();

        for (GameHero hero: game.getHeroes()) {
            if(hero.getHeroName().equals(potionAction.getHeroName())){
                switch (effect){
                    case "HP +1":
                        hero.setHealthPoints(hero.getHealthPoints() + 1);
                        break;
                    case "HP +2":
                        hero.setHealthPoints(hero.getHealthPoints() + 2);
                        break;
                }
                hero.getItemList().remove(potionAction.getPotion());
                potionAction.setNewHealth(hero.getHealthPoints());
                potionAction.setNewItemList(hero.getItemList());
                PotionMessage potionMessage = new PotionMessage(hero.getHeroName(), hero.getHealthPoints(),
                        hero.getItemList().stream().map(item -> mapper.map(item, ItemDTO.class)).collect(Collectors.toList()));
                broadcastService.sendEvent(lobbyId, potionMessage);
                game.addAction(potionAction);
                return game;
            }
        }
        return null;
    }
}
