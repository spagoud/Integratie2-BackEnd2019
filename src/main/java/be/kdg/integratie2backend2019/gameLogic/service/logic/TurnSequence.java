package be.kdg.integratie2backend2019.gameLogic.service.logic;

import be.kdg.integratie2backend2019.broadcast.messages.ActionMessage;
import be.kdg.integratie2backend2019.broadcast.messages.SequenceMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.gameLogic.service.ai.PlayerAI;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static be.kdg.integratie2backend2019.broadcast.messages.ActionMessage.ActionType.TURN;

@Component
public class TurnSequence {
    private final String DM = "DungeonMaster";
    private List<String> players;

    private BroadcastService broadcastService;

    public TurnSequence(BroadcastService broadcastService) {
        this.broadcastService = broadcastService;
    }

    public List<String> generateTurnSequence(List<GameHero> heroes, Long lobbyId){
        players = new ArrayList<>();
        for (GameHero hero:heroes) {
            players.add(hero.getHeroName());
        }
        players.add(DM);
        Collections.shuffle(players);

        String player = players.get(0);

        while(player.equals("DungeonMaster")){
            Collections.shuffle(players);
            player = players.get(0);
        }

        sendSequence(players, lobbyId);
        sendPlayerTurn(player, lobbyId);

        return players;
    }

    public List<String> reshuffleSequence(String sequence, Long lobbyId){
        List<String> players = Arrays.asList(sequence.split(";"));
        Collections.shuffle(players);

        sendSequence(players, lobbyId);
        String player = players.get(0);
        sendPlayerTurn(player, lobbyId);

        return players;
    }

    public String getNextPlayer(String lastPlayer, Game game, Long lobbyId){
        String sequence = game.getTurnSequence();
        List<String> players = Arrays.asList(sequence.split(";"));
        int playerLocation = players.indexOf(lastPlayer);
        String player;
        if(playerLocation == players.size() - 1){
            player = players.get(0);
        }
        else{
            player = players.get(playerLocation + 1);
        }
        if(isDeadPlayer(game, player)){
            getNextPlayer(player, game, lobbyId);
        }
        sendPlayerTurn(player, lobbyId);
        return player;
    }

    private void sendSequence(List<String> players, Long lobbyId){
        ActionMessage message = new SequenceMessage(null, players);
        broadcastService.sendEvent(lobbyId, message);
    }

    private void sendPlayerTurn(String player, Long lobbyId){
        ActionMessage message = new ActionMessage(player, TURN);
        broadcastService.sendEvent(lobbyId, message);
    }

    private boolean isDeadPlayer(Game game, String player){
        for (GameHero hero : game.getHeroes()) {
            if(hero.getHeroName().equals(player))
                return hero.isDead();
        }
        return false;
    }
}
