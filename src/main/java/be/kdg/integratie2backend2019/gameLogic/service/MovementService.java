package be.kdg.integratie2backend2019.gameLogic.service;

import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.game.service.GameHeroService;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MovementService {
    private GameLobbyService gameLobbyService;

    public MovementService(GameLobbyService gameLobbyService) {
        this.gameLobbyService = gameLobbyService;
    }

    public List<Tile> getRangePlayerMovement(Long lobbyId, String heroName) {
        GameLobby gameLobby = gameLobbyService.loadGameLobby(lobbyId);
        Optional<GameHero> optGameHero = gameLobby.getGame().getHeroes().stream().filter(hero -> hero.getHeroName().equals(heroName)).findFirst();
        List<Room> visibleRooms = gameLobby.getGame().getVisibleRooms();
        List<Tile> moveableTiles = new ArrayList<>();
        for (Room room: visibleRooms) {
            moveableTiles.addAll(room.getTiles());
        }
        GameHero gameHero = new GameHero();
        if (optGameHero.isPresent()) {
            gameHero = optGameHero.get();
        }
        return getRange(moveableTiles,gameHero.getPosition(),gameHero.getMove());
    }

    public List<Tile> getRangeMonsterMovement(Long lobbyId, GameMonster monster){
        GameLobby gameLobby = gameLobbyService.loadGameLobby(lobbyId);
        List<Room> visibleRooms = gameLobby.getGame().getVisibleRooms();
        List<Tile> moveableTiles = new ArrayList<>();
        for(Room room: visibleRooms){
            moveableTiles.addAll(room.getTiles());
        }
        return getRange(moveableTiles, monster.getPosition(), monster.getMove());
    }

    private List<Tile> getRange(List<Tile> moveableSpace, Tile startPosition, int move) {
        Set<Tile> range = new HashSet<>();
        Set<Tile> result = calculateRange(range, moveableSpace, startPosition, move);
        return new ArrayList<>(result);
    }

    private Set<Tile> calculateRange(Set<Tile> range, List<Tile> moveableSpace, Tile startPosition, int movesLeft) {
        if (--movesLeft >= 0) {
            if (!startPosition.getTileType().toString().contains("TOP")) {
                range = addTileToRange(range, moveableSpace, startPosition, movesLeft, 0, -1);
            }
            if (!startPosition.getTileType().toString().contains("BOTTOM")) {
                range = addTileToRange(range, moveableSpace, startPosition, movesLeft, 0, 1);

            }
            if (!startPosition.getTileType().toString().contains("LEFT")) {
                range = addTileToRange(range, moveableSpace, startPosition, movesLeft, -1, 0);

            }
            if (!startPosition.getTileType().toString().contains("RIGHT")) {
                range = addTileToRange(range, moveableSpace, startPosition, movesLeft, 1, 0);
            }
        } else return null;
        return range;
    }

    private Set<Tile> addTileToRange(Set<Tile> range, List<Tile> moveableSpace, Tile startPosition, int movesLeft, int xOffset, int yOffset) {
        Optional<Tile> optionalTile = getTile(moveableSpace, startPosition.getX() + xOffset, startPosition.getY() + yOffset);
        if (optionalTile.isPresent() && !optionalTile.get().isPillar()) {
            range.add(optionalTile.get());
            calculateRange(range, moveableSpace, optionalTile.get(), movesLeft);
        }
        return range;
    }

    private Optional<Tile> getTile(List<Tile> moveableSpace, int x, int y) {
        return moveableSpace.stream().filter(t -> t.getX() == x).filter(t -> t.getY() == y).findAny();
    }

    public Optional<Tile> checkForTrap(List<Tile> walkRoute) {
        for (Tile tile : walkRoute) {
            if (tile.getTrapType() == TrapType.PIT) {
                return Optional.of(tile);
            }
        }
        return Optional.empty();
    }


}
