package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.broadcast.messages.ChatMessage;
import be.kdg.integratie2backend2019.broadcast.messages.MoveMessage;
import be.kdg.integratie2backend2019.broadcast.messages.TrapMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import be.kdg.integratie2backend2019.game.web.dto.TileDTO;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.MoveAction;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.TrapAction;
import be.kdg.integratie2backend2019.gameLogic.service.MovementService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class MoveHelper {

    private ModelMapper mapper;
    private BroadcastService broadcastService;
    private MovementService movementService;


    public MoveHelper(ModelMapper mapper, BroadcastService broadcastService, MovementService movementService) {
        this.mapper = mapper;
        this.broadcastService = broadcastService;
        this.movementService = movementService;
    }

    /**
     * @param moveAction
     * @param lobbyId
     * @param game
     * @return updated and persisted Game object
     */
    public Game moveCharacter(MoveAction moveAction, Long lobbyId, Game game) {

        if(moveAction.isMonster()){
            game = moveMonster(game, moveAction);
            game.addAction(moveAction);
            MoveMessage message = new MoveMessage(moveAction.getHeroName(), moveAction.getMovementPath().stream()
                    .map(x -> mapper.map(x, TileDTO.class))
                    .collect(Collectors.toList()));

            broadcastService.sendEvent(lobbyId, message);

            return game;
        }
        else {
            List<Tile> movementPath = moveAction.getMovementPath();
            Optional<Tile> optionalTile = movementService.checkForTrap(movementPath);
            if (optionalTile.isPresent()) {
                Tile tile = optionalTile.get();

                TrapAction trapAction = new TrapAction(moveAction.getHeroName(), tile, tile.getTrapType(), false, true);
                List<Tile> trueMovement = getTilesUpToTrap(movementPath, tile);
                MoveAction trueMoveAction = new MoveAction(moveAction.getHeroName(), trueMovement);

                tile.setTrapType(TrapType.NONE);
                TrapMessage trapMessage = new TrapMessage(moveAction.getHeroName(), tile, 1);
                MoveMessage moveMessage = new MoveMessage(moveAction.getHeroName(),
                        trueMovement.stream().map(t -> mapper.map(t, TileDTO.class)).collect(Collectors.toList()));

                game = trapActivated(game, trapAction);
                trapAction.setDamage(1);
                game = moveGameCharacter(game, moveAction);

                game.addAction(trapAction);
                game.addAction(trueMoveAction);

                ChatMessage chatMessage = new ChatMessage("Info", ChatMessage.ChatMessageType.INFO, moveAction.getHeroName() + " activated a trap.");

                broadcastService.sendEvent(lobbyId, moveMessage);
                broadcastService.sendEvent(lobbyId, trapMessage);
                broadcastService.sendChatMessage(lobbyId, chatMessage);
                return game;
            } else {

                game = moveGameCharacter(game, moveAction);
                game.addAction(moveAction);

                MoveMessage message = new MoveMessage(moveAction.getHeroName(), moveAction.getMovementPath().stream()
                        .map(x -> mapper.map(x, TileDTO.class))
                        .collect(Collectors.toList()));

                broadcastService.sendEvent(lobbyId, message);

                return game;
            }
        }
    }

    private List<Tile> getTilesUpToTrap(List<Tile> tiles, Tile trap) {
        List<Tile> upToTrap = new ArrayList<>();
        for (Tile tile : tiles) {
            upToTrap.add(tile);
            if (tile.equals(trap)) {
                return upToTrap;
            }
        }
        return upToTrap;
    }

    private Game trapActivated(Game game, TrapAction action){
        List<GameHero> heroes = game.getHeroes();
        for (GameHero hero:heroes) {
            if(hero.getHeroName().equals(action.getHeroName())){
                hero.setHealthPoints(hero.getHealthPoints() - 1);
            }
        }
        game.setHeroes(heroes);

        game.getVisibleRooms().stream()
                .map(room -> room.getTiles()
                        .stream()
                        .map(tile -> {
                            if(tile.equals(action.getTile())){
                                tile.setTrapType(TrapType.NONE);
                            }
                            return null;
                        })
                );
        return game;
    }

    private Game moveGameCharacter(Game game, MoveAction action){
        List<GameHero> heroes = game.getHeroes();
        int size = action.getMovementPath().size();

        if(size != 0) {
            Tile movedTo = action.getMovementPath().get(size - 1);
            for (GameHero hero : heroes) {
                if (hero.getHeroName().equals(action.getHeroName())) {
                    hero.setPosition(movedTo);
                }
            }
        }

        game.setHeroes(heroes);
        return game;
    }

    private Game moveMonster(Game game, MoveAction moveAction) {
        int size = moveAction.getMovementPath().size();
        if(size != 0){
            List<Room> visibleRooms = game.getVisibleRooms();
            for (Room room: visibleRooms) {
                List<GameMonster> monsters = room.getGameMonsters();
                for (GameMonster monster: monsters) {
                    if(monster.getId() == Long.valueOf(moveAction.getHeroName())){
                        monster.setPosition(moveAction.getMovementPath().get(size -1));
                    }
                }
                room.setGameMonsters(monsters);
            }
            game.setVisibleRooms(visibleRooms);
        }
        return game;
    }
}
