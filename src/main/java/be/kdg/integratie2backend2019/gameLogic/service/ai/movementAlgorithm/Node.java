package be.kdg.integratie2backend2019.gameLogic.service.ai.movementAlgorithm;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import lombok.Data;

import java.awt.*;

@Data
class Node implements Comparable<Node>{

    private enum State{
        UNVISITED, OPEN, CLOSED
    }
    private enum Type{
        NORMAL, OBSTACLE, TOP, LEFT, RIGHT, BOTTOM, TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT
    }

    private double costFromStart;
    private double costToTarget;
    private double totalCost;
    private Node parent;
    private State state;
    private Type type;
    private int x;
    private int y;
    private Tile tile;

    public Node(int x, int y, String type, Tile tile){
        this.costFromStart = 0;
        this.costToTarget = 0;
        this.totalCost = 0;
        this.parent = null;
        this.state = State.UNVISITED;
        this.type = Type.valueOf(type);
        this.x = x;
        this.y = y;
        this.tile = tile;
    }

    public Point getPosition(){
        return new Point(x, y);
    }

    public boolean isObstacle(){
        return this.type == Type.OBSTACLE;
    }

    public boolean isBlockedTop(){
        return this.type == Type.TOP;
    }

    public boolean isBlockedBottom(){
        return this.type == Type.BOTTOM;
    }

    public boolean isBlockedRight(){
        return this.type == Type.RIGHT;
    }

    public boolean isBlockedLeft(){
        return this.type == Type.LEFT;
    }

    public boolean isBlockedTopLeft(){
        return this.type == Type.TOPLEFT;
    }

    public boolean isBlockedTopRight(){
        return this.type == Type.TOPRIGHT;
    }

    public boolean isBlockedBottomLeft(){
        return this.type == Type.BOTTOMLEFT;
    }

    public boolean isBlockedBottomRight(){
        return this.type == Type.BOTTOMRIGHT;
    }

    public boolean isOpen(){
        return this.state == State.OPEN;
    }

    public boolean isClosed(){
        return this.state == State.CLOSED;
    }

    public void setOpen(){
        this.state = State.OPEN;
    }

    public void setClosed(){
        this.state = State.CLOSED;
    }


    @Override
    public int compareTo(Node n){
        if(this.getTotalCost() < n.getTotalCost()){
            return -1;
        }
        else if(this.getTotalCost() > n.getTotalCost()){
            return 1;
        }
        else{
            return 0;
        }
    }

    @Override
    public boolean equals(Object o){
        if(o == null)
            return false;
        else{
            Node n = (Node)o;
            Point firstPosition = this.getPosition();
            Point secondPosition = n.getPosition();
            return firstPosition.equals(secondPosition);
        }
    }

}
