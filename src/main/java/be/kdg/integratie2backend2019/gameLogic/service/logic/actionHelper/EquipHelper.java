package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.broadcast.messages.EquipMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.items.Item;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.EquipAction;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EquipHelper {

    private BroadcastService broadcastService;

    public EquipHelper(BroadcastService broadcastService) {
        this.broadcastService = broadcastService;
    }

    /**
     * @param equipAction
     * @param lobbyId
     * @param game
     * @return
     */
    public Game equipItem(EquipAction equipAction, Long lobbyId, Game game) {
        List<GameHero> heroes = game.getHeroes();
        GameHero gameHero = heroes.stream().filter(hero -> hero.getHeroName().equals(equipAction.getHeroName())).findFirst().get();
        Item itemToEquip = gameHero.getItemList().stream().filter(item -> item.getId().equals(equipAction.getItemId())).findFirst().get();

        EquipMessage message = new EquipMessage(equipAction.getHeroName());

        switch (itemToEquip.getType()) {
            case "Weapon":
                Item equipedWeapon = gameHero.getEquipment().getWeapon();
                gameHero.getEquipment().setWeapon(itemToEquip);
                gameHero.getItemList().remove(itemToEquip);
                if(equipedWeapon!=null){
                    gameHero.getItemList().add(equipedWeapon);
                    message.setEquipedItem(equipedWeapon.getId());
                }
                message.setEquipedItem(itemToEquip.getId());
                break;
            case "Artifact":
                Item equipedArtifact = gameHero.getEquipment().getArtifact();
                gameHero.getEquipment().setArtifact(itemToEquip);
                gameHero.getItemList().remove(itemToEquip);
                message.setEquipedItem(itemToEquip.getId());
                if(equipedArtifact!=null){
                    gameHero.getItemList().add(equipedArtifact);
                    message.setEquipedItem(equipedArtifact.getId());
                }
                break;
            case "Spell":
                //TODO: spell equip reviewen!
                break;
        }

        broadcastService.sendEvent(lobbyId, message);
        game.addAction(equipAction);
        return game;
    }

}
