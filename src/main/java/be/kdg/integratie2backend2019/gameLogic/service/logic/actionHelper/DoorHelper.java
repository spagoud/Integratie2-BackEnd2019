package be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper;

import be.kdg.integratie2backend2019.broadcast.messages.ChatMessage;
import be.kdg.integratie2backend2019.broadcast.messages.DoorMessage;
import be.kdg.integratie2backend2019.broadcast.service.BroadcastService;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Door;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.web.dto.RoomDTO;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.DoorAction;
import be.kdg.integratie2backend2019.gameLogic.service.GameLogicService;
import be.kdg.integratie2backend2019.gameLogic.service.logic.DoorHandler;
import be.kdg.integratie2backend2019.gameLogic.service.logic.TurnSequence;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class DoorHelper {

    private DoorHandler doorHandler;
    private TurnSequence turnSequence;
    private BroadcastService broadcastService;
    private GameLogicService gameLogicService;
    private ModelMapper mapper;

    public DoorHelper(DoorHandler doorHandler, TurnSequence turnSequence, BroadcastService broadcastService, ModelMapper mapper) {
        this.doorHandler = doorHandler;
        this.turnSequence = turnSequence;
        this.broadcastService = broadcastService;
        this.mapper = mapper;
    }

    /**
     * @param doorAction
     * @param lobbyId
     * @param game
     * @return updated and persisted Game object
     *
     *        Calls DoorHandler to add the new room, that's connected through the door,
     *        to the game. Afterwards the turn sequence will be reshuffled and the new sequence and the first player will be
     *        broadcasted. The updated game will be persisted.
     */
    public Game openDoor(DoorAction doorAction, Long lobbyId, Game game) {

        Room newRoom = doorHandler.getNextRoom(game, doorAction.getDoor());
        game.addVisibleRoom(newRoom);
        doorAction.setRoom(newRoom);
        for (Room visibleRoom : game.getVisibleRooms()) {
            Optional<Door> optDoor = visibleRoom.getDoors().stream().filter(d -> d.getId().equals(doorAction.getDoor().getId())).findFirst();
            optDoor.ifPresent(door -> door.setEnabled(true));
            for (Tile tile : visibleRoom.getTiles()) {
                if(tile.getId().equals(doorAction.getDoor().getTile1().getId())){
                    tile.setTileType(TileType.FLOOR);
                    tile.setDoor(false);
                }
                else if(tile.getId().equals(doorAction.getDoor().getTile2().getId())){
                    tile.setTileType(TileType.FLOOR);
                    tile.setDoor(false);
                }
            }
        }

        String playerSequence = game.getTurnSequence();
        List<String> players = turnSequence.reshuffleSequence(playerSequence, lobbyId);
        StringBuilder builder = new StringBuilder();
        for (String player : players) {
            builder.append(player).append(";");
        }

        game.setTurnSequence(builder.toString());
        game.setCurrentPlayer(players.get(0));

        game.addAction(doorAction);

        DoorMessage doorMessage = new DoorMessage(doorAction.getHeroName(), mapper.map(newRoom, RoomDTO.class), doorAction.getDoor().getId());
        ChatMessage chatMessage = new ChatMessage("Info", ChatMessage.ChatMessageType.INFO, doorAction.getHeroName() + " opened door");
        broadcastService.sendEvent(lobbyId, doorMessage);
        broadcastService.sendChatMessage(lobbyId, chatMessage);

        String player = game.getCurrentPlayer();
        gameLogicService.checkForAITurn(player, lobbyId, game);

        return game;
    }

    @Autowired
    public void setGameLogicService(GameLogicService gameLogicService) {
        this.gameLogicService = gameLogicService;
    }
}
