package be.kdg.integratie2backend2019.gameLogic.service.logic;

import be.kdg.integratie2backend2019.game.persistence.model.GameHero;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TileType;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PositionSetter {

    public List<GameHero> setStartPosition(List<GameHero> heroes, List<Room> visibleRooms){

        List<Tile> tilesInStartRoom = visibleRooms.get(0).getTiles();
        List<Tile> possibleTiles = tilesInStartRoom.stream().filter( x -> x.getTileType().equals(TileType.FLOOR)).collect(Collectors.toList());
        int i = 0;

        for (GameHero hero : heroes) {
            hero.setPosition(possibleTiles.get(i++));
        }
        return heroes;
    }

    public Room setMonstersOnRandomPosition(Room room){
        List<Tile> freeTiles = getFreeTiles(room.getTiles());
        Collections.shuffle(freeTiles);
        List<GameMonster> monsters = room.getGameMonsters();
        for (int i = 0; i < monsters.size(); i++) {
            monsters.get(i).setPosition(freeTiles.get(i));
        }
        room.setGameMonsters(monsters);
        return room;
    }

    private List<Tile> getFreeTiles(List<Tile> tilesInRoom){
        List<Tile> freeTiles = new ArrayList<>();
        for (Tile tile: tilesInRoom) {
            if(!tile.isChest() && !tile.isPillar() && tile.getTrapType().equals(TrapType.NONE) && !tile.isDoor()){
                freeTiles.add(tile);
            }
        }
        return freeTiles;
    }
}
