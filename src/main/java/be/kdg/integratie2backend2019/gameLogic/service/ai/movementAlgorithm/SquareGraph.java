package be.kdg.integratie2backend2019.gameLogic.service.ai.movementAlgorithm;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SquareGraph {

    private Node[][] map;
    private Point startPosition;
    private Point targetPosition;
    private Heap<Node> openNodes;
    private Set<Node> closedNodes;

    public SquareGraph(int mapDimension){
        map = new Node[mapDimension][mapDimension];
        startPosition = new Point();
        targetPosition = new Point();
        openNodes = new Heap<Node>();
        closedNodes = new HashSet<Node>();
    }

    public Node getMapCell(Point coord){
        return map[(int)coord.getX()][(int)coord.getY()];
    }

    public void setMapCell(Point coord, Node n){
        map[(int)coord.getX()][(int)coord.getY()] = n;
    }

    public Point getStartPosition(){
        return startPosition;
    }

    public Point getTargetPosition(){
        return targetPosition;
    }

    public void setStartPosition(Point coord){
        startPosition.setLocation(coord);
    }

    public void setTargetPosition(Point coord){
        targetPosition.setLocation(coord);
    }

    public int getDimension(){
        return map.length;
    }

    public void addToOpenNodes(Node n){
        n.setOpen();
        openNodes.add(n);
    }

    public Node popBestOpenNode(){
        return openNodes.remove();
    }

    public void addToClosedNodes(Node n){
        n.setClosed();
        closedNodes.add(n);
    }

    public boolean isInsideMap(Point p){
        return ( (p.getX() >= 0) && (p.getX() < getDimension())  && (p.getY() >= 0) && (p.getY() < getDimension()) );
    }

    public Set<Node> getNeighbours(Node n){
        Set<Node> neighbours = new HashSet<Node>();
        if(n.isBlockedBottom()){
            neighbours = addNeighbour(n, -1, 0, neighbours);
            neighbours = addNeighbour(n, 1, 0, neighbours);
            neighbours = addNeighbour(n, 0, -1, neighbours);
        }
        else if(n.isBlockedTop()){
            neighbours = addNeighbour(n, -1, 0, neighbours);
            neighbours = addNeighbour(n, 1, 0, neighbours);
            neighbours = addNeighbour(n, 0, 1, neighbours);
        }
        else if(n.isBlockedLeft()){
            neighbours = addNeighbour(n, 1, 0, neighbours);
            neighbours = addNeighbour(n, 0, -1, neighbours);
            neighbours = addNeighbour(n, 0, 1, neighbours);
        }
        else if(n.isBlockedRight()){
            neighbours = addNeighbour(n, -1, 0, neighbours);
            neighbours = addNeighbour(n, 0, -1, neighbours);
            neighbours = addNeighbour(n, 0, 1, neighbours);
        }
        else if(n.isBlockedTopLeft()){
            neighbours = addNeighbour(n, 1, 0, neighbours);
            neighbours = addNeighbour(n, 0, 1, neighbours);
        }
        else if(n.isBlockedTopRight()){
            neighbours = addNeighbour(n, -1, 0, neighbours);
            neighbours = addNeighbour(n, 0, 1, neighbours);
        }
        else if(n.isBlockedBottomLeft()){
            neighbours = addNeighbour(n, 1, 0, neighbours);
            neighbours = addNeighbour(n, 0, -1, neighbours);
        }
        else if(n.isBlockedBottomRight()){
            neighbours = addNeighbour(n, -1, 0, neighbours);
            neighbours = addNeighbour(n, 0, -1, neighbours);
        }
        else{
            neighbours = addNeighbour(n, -1, 0, neighbours);
            neighbours = addNeighbour(n, 1, 0, neighbours);
            neighbours = addNeighbour(n, 0, -1, neighbours);
            neighbours = addNeighbour(n, 0, 1, neighbours);
        }
        return neighbours;
    }

    private Set<Node> addNeighbour(Node n, int i, int j, Set<Node> neighbours){
        if (!(i == 0 && j == 0)) {
            if (isInsideMap(new Point(n.getX() + i, n.getY() + j))) {
                Node temp = getMapCell(new Point(n.getX() + i, n.getY() + j));
                if (temp != null) {
                    if (!temp.isObstacle()) {
                        neighbours.add(temp);
                    }
                }
            }
        }
        return neighbours;
    }

    private static double calculateDistance(Point from, Point to){
        return Math.pow(Math.pow(from.getX()-to.getX(), 2) + Math.pow(from.getY() - to.getY(), 2) , 0.5);
    }

    private ArrayList<Node> reconstructPath(Node target){
        ArrayList<Node> path = new ArrayList<Node>();
        Node current = target;

        while(current.getParent() != null){
            path.add(current.getParent());
            current = current.getParent();
        }
        Collections.reverse(path);
        return path;
    }

    public void printPath(ArrayList<Node> path){
        for(int i=0; i<path.size(); i++){
            Node node = path.get(i);
            System.out.println("node : (" + node.getX() + "," + node.getY() + ")");
        }
    }

    public ArrayList<Node> executeAStar(){
        Node start = getMapCell(getStartPosition());
        Node target = getMapCell(getTargetPosition());
        addToOpenNodes(start);

        start.setCostFromStart(0);
        start.setTotalCost( start.getCostFromStart() + calculateDistance(start.getPosition(), target.getPosition()) );
        while(!openNodes.isEmpty()){
            Node current = popBestOpenNode();
            if(current.equals(target)){
                return reconstructPath(target);
            }

            addToClosedNodes(current);
            Set<Node> neighbours = getNeighbours(current);
            for(Node neighbour : neighbours){
                if(!neighbour.isClosed()){
                    double tentativeCost = current.getCostFromStart() + calculateDistance(current.getPosition(), neighbour.getPosition());

                    if( (!neighbour.isOpen()) || (tentativeCost < neighbour.getCostFromStart()) ){
                        neighbour.setParent(current);
                        neighbour.setCostFromStart(tentativeCost);
                        neighbour.setTotalCost(neighbour.getCostFromStart() + calculateDistance(neighbour.getPosition(), start.getPosition()));
                        if(!neighbour.isOpen())
                            addToOpenNodes(neighbour);
                    }
                }

            }
        }

        return null;
    }

}
