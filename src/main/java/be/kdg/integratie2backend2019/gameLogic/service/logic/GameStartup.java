package be.kdg.integratie2backend2019.gameLogic.service.logic;

import be.kdg.integratie2backend2019.game.persistence.dao.DoorRepository;
import be.kdg.integratie2backend2019.game.persistence.dao.GameMonsterRepository;
import be.kdg.integratie2backend2019.game.persistence.dao.RoomRepository;
import be.kdg.integratie2backend2019.game.persistence.dao.TileRepository;
import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Door;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.DoorPrefab;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomMonster;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.RoomPrefab;
import be.kdg.integratie2backend2019.game.persistence.model.stories.prefabs.TilePrefab;
import be.kdg.integratie2backend2019.game.service.GameService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class GameStartup {
    private GameService gameService;
    private TileRepository tileRepository;
    private RoomRepository roomRepository;
    private GameMonsterRepository gameMonsterRepository;
    private DoorRepository doorRepository;

    public GameStartup(GameService gameService, TileRepository tileRepository, RoomRepository roomRepository, GameMonsterRepository gameMonsterRepository, DoorRepository doorRepository) {
        this.gameService = gameService;
        this.tileRepository = tileRepository;
        this.roomRepository = roomRepository;
        this.gameMonsterRepository = gameMonsterRepository;
        this.doorRepository = doorRepository;
    }

    public Game generateGameDungeon(Game game){
        List<RoomPrefab> prefabRooms = game.getStory().getDungeon().getRoomPrefabs();
        List<Room> dungeonRooms = generateDungeonRooms(prefabRooms);
        dungeonRooms = generateDungeonDoors(prefabRooms, dungeonRooms);
        dungeonRooms = generateDungeonMonsters(prefabRooms, dungeonRooms);
        game.setDungeonRooms(roomRepository.saveAll(dungeonRooms));
        game.setVisibleRooms(getStartRoom(dungeonRooms));
        return gameService.saveGame(game);
    }

    private List<Room> generateDungeonRooms(List<RoomPrefab> prefabRooms){
        List<Room> dungeonRooms = new ArrayList<>();
        for (RoomPrefab roomPrefab: prefabRooms) {
            Room room = new Room();
            List<Tile> roomTiles = new ArrayList<>();
            for (TilePrefab tilePrefab: roomPrefab.getTilePrefabs()) {
                Tile tile = new Tile(tilePrefab);
                roomTiles.add(tileRepository.save(tile));
            }
            room.setName(roomPrefab.getName());
            room.setTiles(roomTiles);
            room.setStarterRoom(roomPrefab.isStartRoom());
            dungeonRooms.add(room);
        }
        return dungeonRooms;
    }

    private List<Room> generateDungeonDoors(List<RoomPrefab> prefabRooms, List<Room> dungeonRooms){
        List<Tile> allTiles = getAllTilesFromDungeon(dungeonRooms);
        List<Door> createdDoors = new ArrayList<>();
        for (RoomPrefab roomPrefab: prefabRooms) {
            List<DoorPrefab> doorPrefabs = roomPrefab.getDoorPrefabs();
            List<Door> doors = new ArrayList<>();
            for (DoorPrefab doorPrefab: doorPrefabs) {

                Optional<Door> optionalDoor = createdDoors.stream().filter(d -> d.getName().equals(doorPrefab.getName())).findFirst();
                if(optionalDoor.isPresent()){
                    doors.add(optionalDoor.get());
                }
                else{
                    Door door = new Door();
                    TilePrefab tile1Prefab = doorPrefab.getTilePrefab1();
                    TilePrefab tile2Prefab = doorPrefab.getTilePrefab2();
                    Optional<Tile> optionalTile;
                    optionalTile = allTiles.stream().filter(x -> x.getX() == tile1Prefab.getX() && x.getY() == tile1Prefab.getY()).findFirst();
                    optionalTile.ifPresent(door::setTile1);
                    optionalTile = allTiles.stream().filter(x -> x.getX() == tile2Prefab.getX() && x.getY() == tile2Prefab.getY()).findFirst();
                    optionalTile.ifPresent(door::setTile2);
                    door.setName(doorPrefab.getName());
                    door = doorRepository.save(door);
                    doors.add(door);
                    createdDoors.add(door);
                }
            }

            Optional<Room> optionalRoom = dungeonRooms.stream().filter(x -> x.getName().equals(roomPrefab.getName())).findFirst();
            Room room;
            if(optionalRoom.isPresent()){
                room = optionalRoom.get();
                room.setDoors(doors);
            }
        }

        return dungeonRooms;
    }

    private List<Room> generateDungeonMonsters(List<RoomPrefab> prefabRooms, List<Room> dungeonRooms){
        GameMonster gameMonster;
        List<GameMonster> roomGameMonsters;
        Optional<Room> optionalRoom;
        for (RoomPrefab roomPrefab:prefabRooms) {
            roomGameMonsters = new ArrayList<>();
            List<RoomMonster> roomMonsters = roomPrefab.getRoomMonsters();
            if (roomMonsters != null) {
                for (RoomMonster roomMonster : roomMonsters) {
                    for (int i = 0; i < roomMonster.getAmount(); i++) {
                        gameMonster = new GameMonster(roomMonster.getMonster());
                        roomGameMonsters.add(gameMonsterRepository.save(gameMonster));
                    }
                    optionalRoom = dungeonRooms.stream().filter(x -> x.getName().equals(roomPrefab.getName())).findFirst();
                    if(optionalRoom.isPresent()){
                        optionalRoom.get().setGameMonsters(roomGameMonsters);
                    }
                }

            }

        }
        return dungeonRooms;
    }

    private List<Tile> getAllTilesFromDungeon(List<Room> dungeonRooms){
        List<Tile> allTiles = new ArrayList<>();
        for (Room room: dungeonRooms) {
            allTiles.addAll(room.getTiles());
        }
        return allTiles;
    }

    private List<Room> getStartRoom(List<Room> dungeonRooms){
        List<Room> starterRoom = new ArrayList<>();
        for (Room room:dungeonRooms) {
            if(room.isStarterRoom()){
                starterRoom.add(room);
                return starterRoom;
            }
        }
        return new ArrayList<>();
    }
}
