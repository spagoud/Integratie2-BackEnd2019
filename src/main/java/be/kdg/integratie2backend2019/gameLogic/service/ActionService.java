package be.kdg.integratie2backend2019.gameLogic.service;

import be.kdg.integratie2backend2019.game.persistence.model.Game;
import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.*;
import be.kdg.integratie2backend2019.gameLogic.service.logic.actionHelper.*;
import be.kdg.integratie2backend2019.gameLogic.web.errors.NotPlayerTurnException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@Service
public class ActionService {
    private GameLobbyService lobbyService;
    private AttackHelper attackHelper;
    private DoorHelper doorHelper;
    private EquipHelper equipHelper;
    private MoveHelper moveHelper;
    private ChestHelper chestHelper;
    private PotionHelper potionHelper;

    public ActionService(GameLobbyService lobbyService, AttackHelper attackHelper, EquipHelper equipHelper, MoveHelper moveHelper, ChestHelper chestHelper, PotionHelper potionHelper) {
        this.lobbyService = lobbyService;
        this.attackHelper = attackHelper;
        this.equipHelper = equipHelper;
        this.moveHelper = moveHelper;
        this.chestHelper = chestHelper;
        this.potionHelper = potionHelper;
    }

    public Game handleAction(Action action, GameLobby lobby) {
        Game game = lobby.getGame();
        long lobbyId = lobby.getId();
        if (isTurn(lobbyId, action.getHeroName()) || isTurn(lobbyId, "DungeonMaster")) {
            switch (action.getActionType()) {
                case ATTACK:
                    game = attackHelper.performAttack((AttackAction) action, lobbyId, game);
                    break;
                case DOOR:
                    game = doorHelper.openDoor((DoorAction) action, lobbyId, game);
                    break;
                case TRAP:
                    interactWithTrap((TrapAction) action, lobbyId, game);
                    break;
                case EQUIP:
                    game = equipHelper.equipItem((EquipAction) action, lobbyId, game);
                    break;
                case TRADE:
                    trade((TradeAction) action, lobbyId, game);
                    break;
                case MOVE:
                    game = moveHelper.moveCharacter((MoveAction) action, lobbyId, game);
                    break;
                case CHEST:
                    game = chestHelper.openChest((ChestAction) action, lobbyId, game);
                    break;
                case POTION:
                    game = potionHelper.usePotion((PotionAction) action, lobbyId, game);
                    break;
                case SWITCHITEM:
                    switchItem((SwitchItemAction) action, lobbyId, game);
                    break;
            }
        } else {
            throw new NotPlayerTurnException("You have no moves left");
        }
        return game;
    }

    private void interactWithTrap(TrapAction trapAction, Long lobbyId, Game game) {
        throw new NotImplementedException();
    }

    private void trade(TradeAction tradeAction, Long lobbyId, Game game) {
        throw new NotImplementedException();
    }

    private void switchItem(SwitchItemAction switchItemAction, Long lobbyId, Game game) {
        throw new NotImplementedException();
    }

    private boolean isTurn(Long lobbyId, String heroName) {
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        Game game = lobby.getGame();
        return game.getCurrentPlayer().equals(heroName);
    }

    @Autowired
    public void setDoorHelper(DoorHelper doorHelper) {
        this.doorHelper = doorHelper;
    }
}
