package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AttackAction extends Action{
    private Long targetId;
    private Long itemId;
    private int damage;
    private boolean dead;

    public AttackAction(String heroName , Long targetId) {
        super(heroName, ActionType.ATTACK, new Date());
        this.targetId = targetId;
    }


}
