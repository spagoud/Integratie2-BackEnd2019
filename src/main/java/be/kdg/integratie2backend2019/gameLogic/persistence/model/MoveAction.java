package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MoveAction extends Action {
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "movement_tile",
            joinColumns = @JoinColumn(name = "action_id"),
            inverseJoinColumns = @JoinColumn(name = "tile_id"))
    private List<Tile> movementPath;
    private boolean monster;

    public MoveAction(String heroName, List<Tile> movementPath) {
        super(heroName, ActionType.MOVE, new Date());
        this.movementPath = movementPath;
        this.monster = false;
    }

    public MoveAction(String monsterId, List<Tile> movementPath, boolean monster){
        super(monsterId, ActionType.MOVE, new Date());
        this.movementPath = movementPath;
        this.monster = monster;
    }
}
