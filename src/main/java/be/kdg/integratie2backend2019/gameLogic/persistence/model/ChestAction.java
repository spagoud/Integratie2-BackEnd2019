package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import be.kdg.integratie2backend2019.game.persistence.model.items.Treasure;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ChestAction extends Action{
    @OneToOne(fetch = FetchType.EAGER)
    private Tile tile;
    @OneToOne(fetch = FetchType.EAGER)
    private Treasure treasure;
    private int damage = 0;

    public ChestAction(String actor, Tile tile, Treasure treasure) {
        super(actor, ActionType.CHEST, new Date());
        this.tile = tile;
        this.treasure = treasure;
    }
}
