package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@AllArgsConstructor
@NoArgsConstructor
public class Action {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String heroName;
    private ActionType actionType;
    private Date timeActionPerformed;
    private boolean completed;

    public Action(String heroName, ActionType actionType, Date timeActionPerformed) {
        this.heroName = heroName;
        this.actionType = actionType;
        this.timeActionPerformed = timeActionPerformed;
    }

    public enum ActionType{
        MOVE,
        DOOR,
        TRADE,
        ATTACK,
        TRAP,
        CHEST,
        EQUIP,
        SWITCHITEM,
        POTION
    }
}
