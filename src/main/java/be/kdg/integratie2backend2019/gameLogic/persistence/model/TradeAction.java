package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TradeAction extends Action {
    // Not yet implemented
    public TradeAction(String heroName) {
        super(heroName, ActionType.TRADE, new Date());
    }
}
