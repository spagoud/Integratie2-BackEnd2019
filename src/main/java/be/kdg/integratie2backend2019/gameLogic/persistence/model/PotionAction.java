package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import be.kdg.integratie2backend2019.game.persistence.model.items.Item;
import be.kdg.integratie2backend2019.game.persistence.model.items.Potion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PotionAction extends Action{
    @OneToOne(fetch = FetchType.EAGER)
    private Potion potion;
    private int newHealth;
    @ElementCollection
    private List<Item> newItemList;

    public PotionAction(String heroName, Potion potion) {
        super(heroName, ActionType.POTION, new Date());
        this.potion = potion;
    }
}
