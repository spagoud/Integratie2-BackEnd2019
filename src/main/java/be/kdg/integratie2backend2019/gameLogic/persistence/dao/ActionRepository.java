package be.kdg.integratie2backend2019.gameLogic.persistence.dao;

import be.kdg.integratie2backend2019.gameLogic.persistence.model.Action;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionRepository extends JpaRepository<Action, Long> {
}
