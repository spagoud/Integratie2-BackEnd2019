package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class EquipAction extends Action {
    //TODO: discuss attributes
    private Long itemId;
    private Boolean completed;

    public EquipAction(Long itemId, String heroName) {
        super(heroName, ActionType.EQUIP, new Date());
        this.itemId = itemId;
    }
}
