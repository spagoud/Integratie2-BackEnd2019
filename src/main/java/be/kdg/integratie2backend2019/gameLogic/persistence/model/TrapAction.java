package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.persistence.model.stories.TrapType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TrapAction extends Action{
    @OneToOne(fetch = FetchType.EAGER)
    private Tile tile;
    private TrapType trapType;
    private boolean defused;
    private boolean activated;
    private int damage;

    public TrapAction(String heroName, Tile tile, TrapType trapType, boolean defused, boolean activated) {
        super(heroName, ActionType.TRAP, new Date());
        this.tile = tile;
        this.trapType = trapType;
        this.defused = defused;
        this.activated = activated;
    }
}
