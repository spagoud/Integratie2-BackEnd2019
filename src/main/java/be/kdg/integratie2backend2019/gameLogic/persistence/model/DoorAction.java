package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Door;
import be.kdg.integratie2backend2019.game.persistence.model.stories.Room;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DoorAction extends Action{
    @OneToOne(fetch = FetchType.EAGER)
    private Door door;
    @OneToOne(fetch = FetchType.EAGER)
    private Room room;

    public DoorAction(String heroName, Door door) {
        super(heroName, ActionType.DOOR, new Date());
        this.door = door;
    }
}
