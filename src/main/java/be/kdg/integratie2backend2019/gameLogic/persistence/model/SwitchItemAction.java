package be.kdg.integratie2backend2019.gameLogic.persistence.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SwitchItemAction extends Action {
    //Not yet implemented
    public SwitchItemAction(Long id, String heroName) {
        super(heroName, ActionType.SWITCHITEM, new Date());
    }
}
