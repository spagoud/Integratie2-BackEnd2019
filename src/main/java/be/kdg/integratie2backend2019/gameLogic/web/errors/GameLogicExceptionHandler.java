package be.kdg.integratie2backend2019.gameLogic.web.errors;

import be.kdg.integratie2backend2019.broadcast.error.MyServiceUnavailableException;
import be.kdg.integratie2backend2019.security.web.util.GenericResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GameLogicExceptionHandler extends ResponseEntityExceptionHandler {

    public GameLogicExceptionHandler() {
        super();
    }

    @ExceptionHandler({NotInRangeException.class})
    public ResponseEntity<Object> handleNotInRangeException(final NotInRangeException ex){
        logger.error("400 Status Code", ex);
        final GenericResponse bodyOfResponse = new GenericResponse("Target is not in range or blocked");
        return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({NotPlayerTurnException.class})
    public ResponseEntity<Object> handleNotPlayerTurnException(final NotPlayerTurnException ex){
        logger.error("400 Status Code", ex);
        final GenericResponse bodyOfResponse = new GenericResponse("It is not your turn");
        return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({InsufficientSpaceInInventoryException.class})
    public ResponseEntity<Object> handleInsufficientSpaceInInventoryException(final InsufficientSpaceInInventoryException ex){
        logger.error("400 Status Code", ex);
        final GenericResponse bodyOfResponse = new GenericResponse("There is insufficient space in your inventory");
        return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    //503
    @ExceptionHandler({MyServiceUnavailableException.class})
    public ResponseEntity<Object> handleMyServiceUnavailableException(final MyServiceUnavailableException ex){
        logger.error("503 Status Code", ex);
        final GenericResponse bodyOfResponse = new GenericResponse("The Service is unavailable");
        return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(), HttpStatus.SERVICE_UNAVAILABLE);
    }
}
