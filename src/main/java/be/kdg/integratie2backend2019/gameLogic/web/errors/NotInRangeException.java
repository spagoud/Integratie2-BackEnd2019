package be.kdg.integratie2backend2019.gameLogic.web.errors;

public final class NotInRangeException extends IllegalArgumentException{

    public NotInRangeException() {
        super();
    }

    public NotInRangeException(String s) {
        super(s);
    }

    public NotInRangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotInRangeException(Throwable cause) {
        super(cause);
    }
}
