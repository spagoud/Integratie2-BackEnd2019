package be.kdg.integratie2backend2019.gameLogic.web.errors;

public final class InsufficientSpaceInInventoryException extends RuntimeException {

    public InsufficientSpaceInInventoryException() {
    }

    public InsufficientSpaceInInventoryException(String message) {
        super(message);
    }

    public InsufficientSpaceInInventoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientSpaceInInventoryException(Throwable cause) {
        super(cause);
    }

    public InsufficientSpaceInInventoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
