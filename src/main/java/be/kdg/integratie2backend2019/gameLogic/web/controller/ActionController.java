package be.kdg.integratie2backend2019.gameLogic.web.controller;

import be.kdg.integratie2backend2019.game.persistence.model.stories.Tile;
import be.kdg.integratie2backend2019.game.web.dto.TileDTO;
import be.kdg.integratie2backend2019.gameLogic.persistence.model.*;
import be.kdg.integratie2backend2019.gameLogic.service.GameLogicService;
import be.kdg.integratie2backend2019.gameLogic.service.MovementService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/action")
public class ActionController {

    private final ModelMapper modelMapper;
    private final MovementService movementService;
    private final GameLogicService gameLogicService;

    public ActionController(ModelMapper modelMapper, MovementService movementService, GameLogicService gameLogicService) {
        this.modelMapper = modelMapper;
        this.movementService = movementService;
        this.gameLogicService = gameLogicService;
    }

    @PostMapping("/lobby/{lobbyId}/openDoor")
    public ResponseEntity<?> openDoor(@PathVariable Long lobbyId, @RequestBody DoorAction doorAction){
        gameLogicService.handleAction(doorAction, lobbyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/lobby/{lobbyId}/move")
    public ResponseEntity<?> move(@PathVariable Long lobbyId, @RequestBody MoveAction moveAction){
        gameLogicService.handleAction(moveAction, lobbyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/lobby/{lobbyId}/attack")
    public ResponseEntity<?> attack(@PathVariable Long lobbyId, @RequestBody AttackAction attackAction){
        gameLogicService.handleAction(attackAction, lobbyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/lobby/{lobbyId}/openChest")
    public ResponseEntity<?> openChest(@PathVariable Long lobbyId, @RequestBody ChestAction chestAction){
        gameLogicService.handleAction(chestAction, lobbyId);
        return new ResponseEntity<>( HttpStatus.OK);
    }

    @PostMapping("/lobby/{lobbyId}/potion")
    public ResponseEntity<?> potion(@PathVariable Long lobbyId, @RequestBody PotionAction potionAction){
        gameLogicService.handleAction(potionAction, lobbyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/lobby/{lobbyId}/trade")
    public ResponseEntity<?> trade(@PathVariable Long lobbyId, @RequestBody TradeAction tradeAction){
        gameLogicService.handleAction(tradeAction, lobbyId);
        return new ResponseEntity<>("Not yet available", HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/lobby/{lobbyId}/trap/defuse")
    public ResponseEntity<?> defuseTrap(@PathVariable Long lobbyId, @RequestBody TrapAction trapAction){
        gameLogicService.handleAction(trapAction, lobbyId);
        return new ResponseEntity<>("Not yet available", HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/lobby/{lobbyId}/equip")
    public ResponseEntity<?> equip(@PathVariable Long lobbyId, @RequestBody EquipAction equipAction){
        gameLogicService.handleAction(equipAction,lobbyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping("/lobby/{lobbyId}/range")
    public ResponseEntity<?> getRange(@PathVariable Long lobbyId, @RequestBody String heroName){
        List<Tile> range = movementService.getRangePlayerMovement(lobbyId,heroName);
        List<TileDTO> rangeDTO = new ArrayList<>();
        for (Tile tile : range) {
            rangeDTO.add(modelMapper.map(tile, TileDTO.class));
        }
        return new ResponseEntity<>(rangeDTO, HttpStatus.OK);
    }
}
