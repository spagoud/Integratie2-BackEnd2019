package be.kdg.integratie2backend2019.gameLogic.web.controller;

import be.kdg.integratie2backend2019.game.persistence.model.GameLobby;
import be.kdg.integratie2backend2019.game.service.GameLobbyService;
import be.kdg.integratie2backend2019.gameLogic.service.GameLogicService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/game")
public class GameLogicController {
    private GameLogicService gameLogicService;
    private GameLobbyService lobbyService;

    public GameLogicController(GameLogicService gameLogicService, GameLobbyService gameLobbyService) {
        this.gameLogicService = gameLogicService;
        this.lobbyService = gameLobbyService;
    }

    @GetMapping("/lobby/{lobbyId}/start")
    public ResponseEntity<?> startGame(@PathVariable Long lobbyId){
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        if(lobby != null){
            System.out.println("called start game");
            gameLogicService.setPlayerAmount(lobby);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>("Can't start game, lobby with id: " + lobbyId +" doesn't exist!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/lobby/{lobbyId}/ready")
    public ResponseEntity<?> readyGame(@PathVariable Long lobbyId){
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        if(lobby != null){
            System.out.println("called ready game");
            gameLogicService.confirmGame(lobby);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>("Can't confirm game, lobby with id: " + lobbyId +" doesn't exist!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/lobby/{lobbyId}/stop")
    public ResponseEntity<?> stopGame(@PathVariable Long lobbyId){
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        if(lobby != null){
            gameLogicService.endGame(lobbyId, false);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>("Can't stop game, lobby with id: " + lobbyId +" doesn't exist!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/lobby/{lobbyId}/pauze")
    public ResponseEntity<?> pauzeGame(@PathVariable Long lobbyId){
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        if(lobby != null){
            gameLogicService.pauzeGame(lobbyId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>("Can't pause game, lobby with id: " + lobbyId +" doesn't exist!", HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/lobby/{lobbyId}/endTurn")
    public ResponseEntity<?> endTurn(@PathVariable Long lobbyId, @RequestBody String heroName){
        GameLobby lobby = lobbyService.loadGameLobby(lobbyId);
        if(lobby != null){
            gameLogicService.endTurn(lobbyId, heroName);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>("Can't end turn, lobby with id: " + lobbyId +" doesn't exist!", HttpStatus.BAD_REQUEST);
    }
}
