package be.kdg.integratie2backend2019.gameLogic.web.errors;

public final class NotPlayerTurnException extends IllegalArgumentException {

    public NotPlayerTurnException() {
    }

    public NotPlayerTurnException(String s) {
        super(s);
    }

    public NotPlayerTurnException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotPlayerTurnException(Throwable cause) {
        super(cause);
    }
}
