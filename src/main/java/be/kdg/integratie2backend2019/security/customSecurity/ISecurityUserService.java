package be.kdg.integratie2backend2019.security.customSecurity;

public interface ISecurityUserService {

    String validatePasswordResetToken(long id, String token);

}
