package be.kdg.integratie2backend2019.security.customSecurity.social.google.service;

import org.springframework.social.google.api.plus.Person;

public interface IGoogleService {
    String googleLogin(String redirectUrl);

    String getGoogleAccessToken(String code);

    Person getGoogleUserProfile(String accessToken);
}
