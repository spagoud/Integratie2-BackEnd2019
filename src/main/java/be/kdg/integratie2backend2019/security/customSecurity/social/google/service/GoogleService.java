package be.kdg.integratie2backend2019.security.customSecurity.social.google.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.social.google.api.plus.Person;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;

@Service
@PropertySource({ "classpath:social.properties" })
public class GoogleService implements IGoogleService {

    private final String BASEURL = "http://localhost:8081/google";
    private final String PRODUCTIONURL = "https://integratie2-dev-backend2019.herokuapp.com/google";
    private final String LIVEURL = "https://integragie2-backend2019.herokuapp.com/google";

    @Value("${spring.social.google.app-id}")
    private String googleId;
    @Value("${spring.social.google.app-secret}")
    private String googleSecret;

    private GoogleConnectionFactory createGoogleConnection(){
        return new GoogleConnectionFactory(googleId, googleSecret);
    }


    @Override
    public String googleLogin(String redirectUrl) {
        OAuth2Parameters parameters = new OAuth2Parameters();
        parameters.setRedirectUri(redirectUrl);
        parameters.setScope("profile");
        return createGoogleConnection().getOAuthOperations().buildAuthenticateUrl(parameters);
    }

    @Override
    public String getGoogleAccessToken(String code) {
        return createGoogleConnection().getOAuthOperations().exchangeForAccess(code, BASEURL, null).getAccessToken();
    }

    @Override
    public Person getGoogleUserProfile(String accessToken) {
        Google google = new GoogleTemplate(accessToken);
        Person person = google.plusOperations().getGoogleProfile();
        return person;
    }
}
