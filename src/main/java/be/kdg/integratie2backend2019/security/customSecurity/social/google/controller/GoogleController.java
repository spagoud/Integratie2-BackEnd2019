package be.kdg.integratie2backend2019.security.customSecurity.social.google.controller;

import be.kdg.integratie2backend2019.security.customSecurity.social.ISocialSecurityService;
import be.kdg.integratie2backend2019.security.customSecurity.social.google.service.IGoogleService;
import be.kdg.integratie2backend2019.security.customSecurity.social.model.UserInfo;
import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.security.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.google.api.plus.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/google")
public class GoogleController {

    @Autowired
    private IGoogleService googleService;
    @Autowired
    private IUserService userService;
    @Autowired
    private ISocialSecurityService socialSecurityService;

    /**
     * @param redirectUrl url used for redirection after successful login
     * @return url to login page of facebook
     */
    @GetMapping("/login")
    public ResponseEntity<?> googleLogin(String redirectUrl){
        String url = googleService.googleLogin(redirectUrl);
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> google(@RequestParam String code, HttpServletRequest request, HttpServletResponse response){
        String accessToken = googleService.getGoogleAccessToken(code);
        linkedinProfile(accessToken, request, response);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void linkedinProfile(String accessToken, HttpServletRequest request, HttpServletResponse response){
        Person user = googleService.getGoogleUserProfile(accessToken);
        User dbUser = userService.findUserByEmail(user.getAccountEmail());
        if(dbUser == null){
            UserInfo userInfo = new UserInfo(user.getGivenName(), user.getFamilyName(), user.getAccountEmail());
            dbUser = userService.createSocialUser(userInfo);
        }
        socialSecurityService.autoLogin(dbUser.getEmail(), null, request, response);
    }
}
