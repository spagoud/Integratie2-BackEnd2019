package be.kdg.integratie2backend2019.security.customSecurity.social.linkedin.controller;

import be.kdg.integratie2backend2019.security.customSecurity.social.ISocialSecurityService;
import be.kdg.integratie2backend2019.security.customSecurity.social.linkedin.service.ILinkedinService;
import be.kdg.integratie2backend2019.security.customSecurity.social.model.UserInfo;
import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.security.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.linkedin.api.LinkedInProfileFull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/linkedin")
public class LinkedinController {

    @Autowired
    private ILinkedinService linkedinService;
    @Autowired
    private IUserService userService;
    @Autowired
    private ISocialSecurityService socialSecurityService;

    /**
     * @param redirectUrl url used for redirection after successful login
     * @return url to login page of facebook
     */
    @GetMapping("/login")
    public ResponseEntity<?> linkedinLogin(String redirectUrl){
        String url = linkedinService.linkedinLogin(redirectUrl);
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> linkedin(@RequestParam String code, HttpServletRequest request, HttpServletResponse response){
        String accessToken = linkedinService.getlinkedinAccessToken(code);
        linkedinProfile(accessToken, request, response);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void linkedinProfile(String accessToken, HttpServletRequest request, HttpServletResponse response){
        LinkedInProfileFull user = linkedinService.getLinkedinUserProfile(accessToken);
        User dbUser = userService.findUserByEmail(user.getEmailAddress());
        if(dbUser == null){
            UserInfo userInfo = new UserInfo(user.getFirstName(), user.getLastName(), user.getEmailAddress());
            dbUser = userService.createSocialUser(userInfo);
        }
        socialSecurityService.autoLogin(dbUser.getEmail(), null, request, response);
    }
}
