package be.kdg.integratie2backend2019.security.customSecurity.social.linkedin.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.social.linkedin.api.LinkedInProfileFull;
import org.springframework.social.linkedin.api.impl.LinkedInTemplate;
import org.springframework.social.linkedin.connect.LinkedInConnectionFactory;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;

@Service
@PropertySource({ "classpath:social.properties" })
public class LinkedinService implements ILinkedinService {
    private final String BASEURL = "http://localhost:8081/linkedin";
    private final String PRODUCTIONURL = "https://integratie2-dev-backend2019.herokuapp.com/linkedin";
    private final String LIVEURL = "https://integragie2-backend2019.herokuapp.com/linkedin";

    @Value("${spring.social.linkedin.app-id}")
    private String linkedinId;
    @Value("${spring.social.linkedin.app-secret}")
    private String linkedinSecret;

    private LinkedInConnectionFactory createLinkedinConnection(){
        return new LinkedInConnectionFactory(linkedinId, linkedinSecret);
    }

    @Override
    public String linkedinLogin(String redirectUrl) {
        OAuth2Parameters parameters = new OAuth2Parameters();
        parameters.setRedirectUri(redirectUrl);
        parameters.setScope("r_basicprofile,r_emailaddress");
        return createLinkedinConnection().getOAuthOperations().buildAuthenticateUrl(parameters);
    }

    @Override
    public String getlinkedinAccessToken(String code) {
        return createLinkedinConnection().getOAuthOperations().exchangeForAccess(code, BASEURL, null).getAccessToken();
    }

    @Override
    public LinkedInProfileFull getLinkedinUserProfile(String accessToken) {
        LinkedIn linkedIn = new LinkedInTemplate(accessToken);
        LinkedInProfileFull linkedInProfileFull = linkedIn.profileOperations().getUserProfileFull();
        return linkedInProfileFull;
    }
}
