package be.kdg.integratie2backend2019.security.customSecurity.social.facebook.controller;

import be.kdg.integratie2backend2019.security.customSecurity.social.ISocialSecurityService;
import be.kdg.integratie2backend2019.security.customSecurity.social.facebook.service.IFacebookService;
import be.kdg.integratie2backend2019.security.customSecurity.social.model.UserInfo;
import be.kdg.integratie2backend2019.security.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.social.facebook.api.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/facebook")
public class FacebookController {

    @Autowired
    private IFacebookService facebookService;
    @Autowired
    private IUserService userService;
    @Autowired
    private ISocialSecurityService socialSecurityService;

    /**
     * @param redirectUrl url used for redirection after successful login
     * @return url to login page of facebook
     */
    @GetMapping("/login")
    public ResponseEntity<?> facebookLogin(@RequestParam String redirectUrl){
        String url = "{\"url\": \"" + facebookService.facebookLogin(redirectUrl) + "\"}";
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> facebook(@RequestParam String code, HttpServletRequest request, HttpServletResponse response){
        String accessToken = facebookService.getFacebookAccessToken(code);
        facebookProfile(accessToken, request, response);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void facebookProfile(String accessToken, HttpServletRequest request, HttpServletResponse response){
        User user = facebookService.getFacebookUserProfile(accessToken);
        be.kdg.integratie2backend2019.security.persistence.model.User dbUser = userService.findUserByEmail(user.getEmail());
        if(dbUser == null) {
            UserInfo userInfo = new UserInfo(user.getFirstName(), user.getLastName(), user.getEmail());
            dbUser = userService.createSocialUser(userInfo);

        }
        socialSecurityService.autoLogin(dbUser.getEmail(), null, request, response);
    }
}
