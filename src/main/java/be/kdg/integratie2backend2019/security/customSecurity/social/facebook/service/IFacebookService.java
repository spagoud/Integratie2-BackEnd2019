package be.kdg.integratie2backend2019.security.customSecurity.social.facebook.service;

import org.springframework.social.facebook.api.User;

public interface IFacebookService {
    String facebookLogin(String redirectUrl);

    String getFacebookAccessToken(String code);

    User getFacebookUserProfile(String accessToken);
}
