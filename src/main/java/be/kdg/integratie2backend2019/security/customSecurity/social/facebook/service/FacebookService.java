package be.kdg.integratie2backend2019.security.customSecurity.social.facebook.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;

@Service
@PropertySource({ "classpath:social.properties" })
public class FacebookService implements IFacebookService {

    private final String BASEURL = "http://localhost:4200/facebook";
    private final String PRODUCTIONURL = "https://integratie2-dev-backend2019.herokuapp.com/facebook";
    private final String LIVEURL = "https://integragie2-backend2019.herokuapp.com/facebook";

    @Value("${spring.social.facebook.app-id}")
    private String facebookId;
    @Value("${spring.social.facebook.app-secret}")
    private String facebookSecret;

    private FacebookConnectionFactory createFacebookConnection(){
        return new FacebookConnectionFactory(facebookId, facebookSecret);
    }

    @Override
    public String facebookLogin(String redirectUrl) {
        OAuth2Parameters parameters = new OAuth2Parameters();
        parameters.setRedirectUri(redirectUrl);
        parameters.setScope("public_profile,email");
        return createFacebookConnection().getOAuthOperations().buildAuthenticateUrl(parameters);
    }

    @Override
    public String getFacebookAccessToken(String code) {
        return createFacebookConnection().getOAuthOperations().exchangeForAccess(code, BASEURL, null).getAccessToken();
    }

    @Override
    public User getFacebookUserProfile(String accessToken) {
        Facebook facebook = new FacebookTemplate(accessToken);
        String[] fields = {"id","first_name","last_name","picture","email"};
        return facebook.fetchObject("me", User.class, fields);
    }
}
