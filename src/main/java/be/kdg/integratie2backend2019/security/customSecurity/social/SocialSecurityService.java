package be.kdg.integratie2backend2019.security.customSecurity.social;

import be.kdg.integratie2backend2019.security.persistence.model.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static be.kdg.integratie2backend2019.security.loginToken.SecurityConstants.*;

@Service
@Transactional
public class SocialSecurityService implements ISocialSecurityService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public void autoLogin(String email, String password, HttpServletRequest request, HttpServletResponse response){
        Authentication authentication;
        if(StringUtils.hasText(password)){
            UserDetails details = userDetailsService.loadUserByUsername(email);
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(email, password, details.getAuthorities()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        else{
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            authentication = new UsernamePasswordAuthenticationToken(email, null, grantedAuthorities);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());

        String token = JWT.create()
                .withSubject( authentication.getPrincipal().toString())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        response.addHeader("Access-Control-Expose-Headers", HEADER_STRING + ", username");
        response.addHeader("username", (authentication.getPrincipal().toString()));
    }
}
