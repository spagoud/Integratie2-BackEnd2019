package be.kdg.integratie2backend2019.security.customSecurity.social.linkedin.service;

import org.springframework.social.linkedin.api.LinkedInProfileFull;

public interface ILinkedinService {
    String linkedinLogin(String redirectUrl);

    String getlinkedinAccessToken(String code);

    LinkedInProfileFull getLinkedinUserProfile(String accessToken);
}
