package be.kdg.integratie2backend2019.security.customSecurity.social;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ISocialSecurityService {
    void autoLogin(String email, String password, HttpServletRequest request, HttpServletResponse response);
}
