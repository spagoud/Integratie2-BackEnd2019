package be.kdg.integratie2backend2019.security.service;

import be.kdg.integratie2backend2019.security.customSecurity.social.model.UserInfo;
import be.kdg.integratie2backend2019.security.persistence.model.PasswordResetToken;
import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.security.persistence.model.VerificationToken;
import be.kdg.integratie2backend2019.security.web.dto.UserDto;
import be.kdg.integratie2backend2019.security.web.error.UserAlreadyExistException;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

@Service
public interface IUserService {

    User registerNewUserAccount(UserDto accountDto) throws UserAlreadyExistException;

    User getUser(String verificationToken);

    void saveRegisteredUser(User user);

    void deleteUser(User user);

    void createVerificationTokenForUser(User user, String token);

    VerificationToken getVerificationToken(String VerificationToken);

    VerificationToken generateNewVerificationToken(String token);

    void createPasswordResetTokenForUser(User user, String token);

    User findUserByEmail(String email);

    PasswordResetToken getPasswordResetToken(String token);

    User getUserByPasswordResetToken(String token);

    Optional<User> getUserByID(long id);

    void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(User user, String password);

    String validateVerificationToken(String token);

    String generateQRUrl(User user) throws UnsupportedEncodingException;

    User updateUser2FA(boolean use2FA);

    List<String> getUsersFromSessionRegistry();

    User createSocialUser(UserInfo userInfo);

}
