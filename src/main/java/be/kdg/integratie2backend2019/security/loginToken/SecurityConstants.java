package be.kdg.integratie2backend2019.security.loginToken;

/**
 * Constants used for JWT token generation.
 */
public class SecurityConstants {
    public static final String SECRET = "SecretKeyUsedToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
