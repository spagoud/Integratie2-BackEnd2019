package be.kdg.integratie2backend2019.security.captcha;

import be.kdg.integratie2backend2019.security.web.error.ReCaptchaInvalidException;

public interface ICaptchaService {
    void processResponse(final String response) throws ReCaptchaInvalidException;

    String getReCaptchaSite();

    String getReCaptchaSecret();
}