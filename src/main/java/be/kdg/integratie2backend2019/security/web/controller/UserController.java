package be.kdg.integratie2backend2019.security.web.controller;

import be.kdg.integratie2backend2019.security.customSecurity.ActiveUserStore;
import be.kdg.integratie2backend2019.security.service.IUserService;
import be.kdg.integratie2backend2019.security.web.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    ActiveUserStore activeUserStore;

    @Autowired
    IUserService userService;

    /**
     * Method responsible for retrieval of all active users from the ActiveUserStore in the system
     *
     * @param locale the locale of the requesting device
     * @return returns ResponseEntity containing:
     * -json body:
     * -"users" => list of active users
     * -headers:
     * -"Content-Type" => "application/json; charset=UTF-8"
     * -HttpStatus.OK
     */
    @GetMapping(value = "/loggedUsers")
    public ResponseEntity<Map<String, Object>> getLoggedUsers(final Locale locale) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();
        json.put("users", activeUserStore.getUsers());
        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }

    /**
     * Method responsible for retrieval of all users from the session registry of the system
     *
     * @param locale the locale of the requesting device
     * @return returns ResponseEntity containing:
     * -json body:
     * -"users" => all logged users from session registry
     * -headers:
     * -"Content-Type" => "application/json; charset=UTF-8"
     * -HttpStatus.OK
     */
    @GetMapping(value = "/loggedUsersFromSessionRegistry")
    public ResponseEntity<Map<String, Object>> getLoggedUsersFromSessionRegistry(final Locale locale) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();
        json.put("users", userService.getUsersFromSessionRegistry());
        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }

    @GetMapping(value = "/me")
    public ResponseEntity<Map<String, Object>> getCurrentUser(final Locale locale) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();
        UserDto userDto = new UserDto();

        json.put("user", userDto);
        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }
}
