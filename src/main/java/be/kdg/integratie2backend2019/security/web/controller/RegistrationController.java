package be.kdg.integratie2backend2019.security.web.controller;

import be.kdg.integratie2backend2019.security.customSecurity.ISecurityUserService;
import be.kdg.integratie2backend2019.security.persistence.model.Privilege;
import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.security.persistence.model.VerificationToken;
import be.kdg.integratie2backend2019.security.registration.OnRegistrationCompleteEvent;
import be.kdg.integratie2backend2019.security.service.IUserService;
import be.kdg.integratie2backend2019.security.web.dto.PasswordDto;
import be.kdg.integratie2backend2019.security.web.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class RegistrationController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private IUserService userService;

    @Autowired
    private ISecurityUserService securityUserService;

    @Autowired
    private MessageSource messages;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private Environment env;

    @Autowired
    private AuthenticationManager authenticationManager;

    public RegistrationController() {
        super();
    }

    /**
     * Method used for registering new users to the system.
     *
     * @param accountDto the dto containing the information of the user that is being registered
     * @param request    a HttpServletRequest from the caller
     * @return ResponseEntity      a ResponseEntity containing HttpStatus.OK that will be returned when registration is completed
     */
    @PostMapping("/user/registration")
    public ResponseEntity registerUserAccount(@Valid @RequestBody final UserDto accountDto, final HttpServletRequest request, @RequestParam String redirectUrl) throws UnsupportedEncodingException {
        LOGGER.debug("Registering user account with information: {}", accountDto);
        final User registered = userService.registerNewUserAccount(accountDto);
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), redirectUrl));
        if (registered.isUsing2FA()) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json; charset=UTF-8");
            Map<String, Object> json = new HashMap<>();
            json.put("QRUrl", userService.generateQRUrl(registered));
            return new ResponseEntity<>(json, headers, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Method responsible for checking the confirmation request by comparing the presented token with the
     * known token for the specific user that needs to have the registration confirmed.
     *
     * @param request a HttpServletRequest from the caller
     * @param token   the token provided within the confirmation mail that has been send to the newly registered user
     * @return on successful confirmation with a valid token a ResponseEntity will be returned containing:
     * - json body:
     * - "message" => account verification message
     * - headers:
     * - "Content-Type" => "application/json; charset=UTF8"
     * - HttpStatus.OK
     * on unsuccessful confirmation a ResponseEntity will be returned containing:
     * - json body:
     * - "message" => message about point of failure
     * - "expired" => boolean indicating if token has been expired
     * - "token" => the token presented to the method within the request
     * @throws UnsupportedEncodingException
     */
    @GetMapping("/user/registrationConfirm")
    public ResponseEntity<Map<String, Object>> confirmRegistration(final HttpServletRequest request, @RequestParam("token") final String token) throws UnsupportedEncodingException {
        Locale locale = request.getLocale();
        final String result = userService.validateVerificationToken(token);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();

        if (result.equals("valid")) {
            final User user = userService.getUser(token);
            authWithoutPassword(user);
            json.put("message", messages.getMessage("message.accountVerified", null, locale));
            return new ResponseEntity<>(json, headers, HttpStatus.OK);
        }

        json.put("message", messages.getMessage("auth.message." + result, null, locale));
        json.put("expired", "expired".equals(result));
        json.put("token", token);
        return new ResponseEntity<>(json, headers, HttpStatus.BAD_REQUEST);
    }

    /**
     * Method responsible for resending a confirmation email with a new token based on the already existing token
     * that had been send previously to the user.
     *
     * @param request       a HttpServletRequest from the caller
     * @param existingToken the token that is in existence but may be expired
     * @return on a successful sending of a new confirmation mail containing a new token
     * a ResponseEntity wil be returned containing:
     * - json body:
     * -"message" => message with confirmation of resend
     * - headers:
     * - "Content-Type" => "application/json; charset=UTF8"
     * - HttpStatus.OK
     */
    @GetMapping("/user/resendRegistrationToken")
    public ResponseEntity<Map<String, Object>> resendRegistrationToken(final HttpServletRequest request, @RequestParam("token") final String existingToken) {
        final VerificationToken newToken = userService.generateNewVerificationToken(existingToken);
        final User user = userService.getUser(newToken.getToken());
        mailSender.send(constructResendVerificationTokenEmail(getAppUrl(request), request.getLocale(), newToken, user));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();
        json.put("message", messages.getMessage("message.resendToken", null, request.getLocale()));
        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }

    /**
     * Method responsible for the sending of a password reset mail containing a token used to reset the existing password.
     *
     * @param request   a HttpServletRequest from the caller
     * @param userEmail the email address from the user that needs a password reset
     * @return if the user is correctly identified a ResponseEntity will be returned, after sending a
     * password reset mail containing the reset token, containing:
     * -json body:
     * -"message" => message confirming sending of password reset mail
     * -headers:
     * -"Content-Type" => "application/json; charset=UTF-8"
     * -HttpStatus.OK
     */
    @PostMapping("/user/resetPassword")
    public ResponseEntity<Map<String, Object>> resetPassword(final HttpServletRequest request, @RequestParam("email") final String userEmail) {
        final User user = userService.findUserByEmail(userEmail);
        if (user != null) {
            final String token = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(user, token);
            mailSender.send(constructResetTokenEmail(getAppUrl(request), request.getLocale(), token, user));
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();
        json.put("message", messages.getMessage("message.resetPasswordEmail", null, request.getLocale()));

        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }

    /**
     * Method responsible for the confirmation of the password reset token and returning a corresponding response.
     *
     * @param locale the locale of the requesting device
     * @param id     the user id linked to the user requesting the password reset
     * @param token  the token provided by the password reset mail
     * @return on successful identification of the user by the provided id and token a
     * ResponseEntity will be returned containing:
     * -HttpStatus.OK
     * If the token and id don't match with those in the system
     * a ResponseEntity will be returned containing:
     * -json body:
     * -"message" => message containing point of failure
     * -headers:
     * -"Content-Type" => "application/json; charset=UTF-8"
     * -HttpStatus.BAD_REQUEST
     */
    @GetMapping("/user/changePassword")
    public ResponseEntity<Map<String, Object>> showChangePasswordPage(final Locale locale, @RequestParam("id") final long id, @RequestParam("token") final String token) {
        final String result = securityUserService.validatePasswordResetToken(id, token);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();

        if (result != null) {
            json.put("message", messages.getMessage("auth.message." + result, null, locale));
            return new ResponseEntity<>(json, headers, HttpStatus.BAD_REQUEST); // gets redirected back to login
        }
        return new ResponseEntity<>(HttpStatus.OK); // gets redirected to change password page
    }

    /**
     * Method responsible for persisting the new password after validating the provided dto. The validation
     * happens within changeUserPassword (see below)
     *
     * @param locale      the locale of the requesting device
     * @param passwordDto the dto containing the old and new password for the user
     * @return on successfully changing the password in the system a ResponseEntity wil be returned
     * containing:
     * -json body:
     * -"message" => message with successful password reset
     * -headers:
     * -"Content-Type" => "application/json; charset=UTF-8"
     * -HttpStatus.OK
     */
    @PostMapping("/user/savePassword")
    public ResponseEntity<Map<String, Object>> savePassword(final Locale locale, @Valid @RequestBody PasswordDto passwordDto) {
        final User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.changeUserPassword(user, passwordDto.getNewPassword());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();
        json.put("message", messages.getMessage("message.resetPasswordSuc", null, locale));

        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }

    /**
     * Method responsible for persisting the new password to the corresponding user in the system, after
     * validating the old password of the corresponding user.
     *
     * @param locale      the locale of the requesting device
     * @param passwordDto the dto containing the old and new password for the user
     * @return on successfully changing the password for the user a ResponseEntity will be returned containing:
     * -json body:
     * -"message" => message confirming password update
     * -headers:
     * -"Content-Type" => "application/json; charset=UTF-8"
     * -HttpStatus.OK
     * when the old password provided is invalid a ResponseEntity will be returned containing:
     * -json body:
     * -"message" => message invalid old password
     * -headers:
     * -"Content-Type" => "application/json; charset=UTF-8"
     * -HttpStatus.BAD_REQUEST
     */
    @PostMapping("/user/updatePassword")
    public ResponseEntity<Map<String, Object>> changeUserPassword(final Locale locale, @Valid @RequestBody PasswordDto passwordDto) {
        final User user = userService.findUserByEmail(((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        Map<String, Object> json = new HashMap<>();

        if (!userService.checkIfValidOldPassword(user, passwordDto.getOldPassword())) {
            json.put("message", messages.getMessage("message.invalidOldPassword", null, locale));
            return new ResponseEntity<>(json, headers, HttpStatus.BAD_REQUEST);
        }
        userService.changeUserPassword(user, passwordDto.getNewPassword());
        json.put("message", messages.getMessage("message.updatePasswordSuc", null, locale));

        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }

    /**
     * Method responsible for enabling or disabling two factor authentication for a user.
     *
     * @param use2FA boolean containing whether or not to use two factor authentication
     * @return when use2FA equals 'false' a ResponseEntity will be returned, after updating the user,
     * containing:
     * -HttpStatus.OK
     * when use2FA equals 'true' a ResponseEntity will be returned, after updateing the user,
     * containing:
     * -json body:
     * -"QRUrl" => generated url for 2FA setup
     * -headers:
     * -"Content-Type" => "application/json; charset=UTF-8"
     * -HttpStatus.OK
     * @throws UnsupportedEncodingException
     */
    @PostMapping("/user/update/2fa")
    public ResponseEntity<Map<String, Object>> modifyUser2FA(@RequestParam("use2FA") final boolean use2FA) throws UnsupportedEncodingException {
        final User user = userService.updateUser2FA(use2FA);
        if (use2FA) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json; charset=UTF-8");
            Map<String, Object> json = new HashMap<>();
            json.put("QRUrl", userService.generateQRUrl(user));
            return new ResponseEntity<>(json, headers, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // ============== NON-API ============


    /**
     * Methods used for the creation of various types of emails used required within the controller:
     * -VerificationTokenEmail
     * -ResetTokenEmail
     */

    private SimpleMailMessage constructResendVerificationTokenEmail(final String contextPath, final Locale locale, final VerificationToken newToken, final User user) {
        final String confirmationUrl = contextPath + "/registrationConfirm.html?token=" + newToken.getToken();
        final String message = messages.getMessage("message.resendToken", null, locale);
        return constructEmail("Resend Registration Token", message + " \r\n" + confirmationUrl, user);
    }

    private SimpleMailMessage constructResetTokenEmail(final String contextPath, final Locale locale, final String token, final User user) {
        final String url = contextPath + "/user/changePassword?id=" + user.getId() + "&token=" + token;
        final String message = messages.getMessage("message.resetPassword", null, locale);
        return constructEmail("Reset Password", message + " \r\n" + url, user);
    }

    private SimpleMailMessage constructEmail(String subject, String body, User user) {
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(body);
        email.setTo(user.getEmail());
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    /**
     * Method responsible for authentication of a user without a password withing the SecurityContextHolder
     *
     * @param user an user object
     */
    public void authWithoutPassword(User user) {
        List<Privilege> privileges = user.getRoles().stream().map(role -> role.getPrivileges()).flatMap(list -> list.stream()).distinct().collect(Collectors.toList());
        List<GrantedAuthority> authorities = privileges.stream().map(p -> new SimpleGrantedAuthority(p.getName())).collect(Collectors.toList());

        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, authorities);

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}

