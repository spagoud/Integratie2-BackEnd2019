package be.kdg.integratie2backend2019.security.web.dto;

import be.kdg.integratie2backend2019.security.validation.ValidPassword;
import lombok.Data;


@Data
public class PasswordDto {

    private String oldPassword;

    @ValidPassword
    private String newPassword;

}
