package be.kdg.integratie2backend2019.security.persistence.dao;

import be.kdg.integratie2backend2019.security.persistence.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    @Override
    void delete(Role role);

}
