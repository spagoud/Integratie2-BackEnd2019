package be.kdg.integratie2backend2019.security.persistence.dao;

import be.kdg.integratie2backend2019.security.persistence.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.social.connect.Connection;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
