package be.kdg.integratie2backend2019.security.config;

import be.kdg.integratie2backend2019.security.persistence.dao.PrivilegeRepository;
import be.kdg.integratie2backend2019.security.persistence.dao.RoleRepository;
import be.kdg.integratie2backend2019.security.persistence.dao.UserRepository;
import be.kdg.integratie2backend2019.security.persistence.model.Privilege;
import be.kdg.integratie2backend2019.security.persistence.model.Role;
import be.kdg.integratie2backend2019.security.persistence.model.User;
import be.kdg.integratie2backend2019.userProfile.persistence.model.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * The data loader is used to inject data inside the used database by using the existing JPA repositories.
 */
@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    // API

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        //TODO: privileges aanpassen naar onze noden!!!

        // == create initial privileges
        final Privilege adminPrivilege = createPrivilegeIfNotFound("ADMIN_PRIVILEGE");
        final Privilege superAdminPrivilege = createPrivilegeIfNotFound("SUPERADMIN_PRIVILEGE");
        final Privilege userPrivilege = createPrivilegeIfNotFound("USER_PRIVILEGE");
        final Privilege lobbyOwnerPrivilege = createPrivilegeIfNotFound("LOBBYOWNER_PRIVILEGE");

        // == create initial roles
        final List<Privilege> superAdminPrivileges = new ArrayList<>(Arrays.asList(superAdminPrivilege, adminPrivilege, userPrivilege));
        final List<Privilege> adminPrivileges = new ArrayList<>(Arrays.asList(adminPrivilege, userPrivilege));
        final List<Privilege> userPrivileges = new ArrayList<>(Arrays.asList(userPrivilege));
        final Role superAdminRole = createRoleIfNotFound("ROLE_SUPERADMIN", superAdminPrivileges);
        final Role adminRole = createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        final Role userRole = createRoleIfNotFound("ROLE_USER", userPrivileges);

        // == create initial UserProfile
        final UserProfile superAdminProfile = new UserProfile("superAdmin");
        final UserProfile adminProfile = new UserProfile("admin");
        final UserProfile testUserProfile = new UserProfile("testUser");

        // == create initial user
        User superAdmin = createUserIfNotFound("superadmin@spagoud.com", "SuperSpa", "Goud", "superadmin", new ArrayList<>(Arrays.asList(superAdminRole)), superAdminProfile);
        User admin = createUserIfNotFound("admin@spagoud.com", "Spa", "Goud", "admin", new ArrayList<>(Arrays.asList(adminRole)), adminProfile);
        User testUser = createUserIfNotFound("testuser@spagoud.com", "Test", "User", "user", new ArrayList<>(Arrays.asList(userRole)), testUserProfile);

        superAdmin.getUserProfile().updateUsername();
        userRepository.save(superAdmin);

        alreadySetup = true;
    }

    @Transactional
    protected Privilege createPrivilegeIfNotFound(final String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilege = privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    protected Role createRoleIfNotFound(final String name, final Collection<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
        }
        role.setPrivileges(privileges);
        role = roleRepository.save(role);
        return role;
    }

    @Transactional
    protected User createUserIfNotFound(final String email, final String firstName, final String lastName, final String password, final Collection<Role> roles, final UserProfile userProfile) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setPassword(passwordEncoder.encode(password));
            user.setEmail(email);
            user.setEnabled(true);
            userProfile.setUser(user);
            user.setUserProfile(userProfile);
        }
        user.setRoles(roles);
        user = userRepository.save(user);
        return user;
    }

}
