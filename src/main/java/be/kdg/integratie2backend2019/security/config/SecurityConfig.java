package be.kdg.integratie2backend2019.security.config;

import be.kdg.integratie2backend2019.security.config.filter.CorsFilter;
import be.kdg.integratie2backend2019.security.customSecurity.CustomAuthenticationFailureHandler;
import be.kdg.integratie2backend2019.security.customSecurity.CustomRememberMeServices;
import be.kdg.integratie2backend2019.security.customSecurity.MyCustomLoginAuthenticationSuccessHandler;
import be.kdg.integratie2backend2019.security.customSecurity.MyLogoutSuccessHandler;
import be.kdg.integratie2backend2019.security.customSecurity.google2fa.CustomAuthenticationProvider;
import be.kdg.integratie2backend2019.security.customSecurity.google2fa.CustomWebAuthenticationDetailsSource;
import be.kdg.integratie2backend2019.security.loginToken.JWTAuthenticationFilter;
import be.kdg.integratie2backend2019.security.loginToken.JWTAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;


@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = { "be.kdg.integratie2backend2019.security" })
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private MyCustomLoginAuthenticationSuccessHandler myAuthenticationSuccessHandler;

    @Autowired
    private MyLogoutSuccessHandler myLogoutSuccessHandler;

    @Autowired
    private CustomAuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private CustomWebAuthenticationDetailsSource authenticationDetailsSource;

    public SecurityConfig() {
        super();
    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.addFilterBefore(new CorsFilter(), ChannelProcessingFilter.class);
        http
                .csrf().disable()
                .authorizeRequests()
                .and()
                .exceptionHandling()
                .and()
                .authorizeRequests()
                .antMatchers("/login*", "/logout*", "/google*", "/facebook*", "/linkedin*",
                        "/google/login*", "/facebook/login*", "/linkedin/login*","/signup/**",
                        "/user/registration*", "/user/registrationConfirm*", "/expiredAccount*",
                        "/badUser*", "/user/resendRegistrationToken*" , "/user/resetPassword*",
                        "/user/changePassword*",  "/resources/**",
                        "/me*", "/userprofile*").permitAll()
                .antMatchers("/invalidSession*").anonymous()
                .antMatchers("/user/updatePassword*","/user/savePassword*", "/roomPrefab*"
                        , "/loggedUsers*", "/loggedUsersFromSessionRegistry*", "/broadcast*", "/gameLobby*"
                        , "/gameLobby/newGameLobby*", "/gameLobby/serverLobby*", "/hero*", "/room*", "/room/testRoom*"
                        , "/movement*", "/movement/range*", "/game*", "/game/lobby**", "/userprofile/statistics*")
                    .hasAnyAuthority("USER_PRIVILEGE","ADMIN_PRIVILEGE")
                .anyRequest().hasAuthority("USER_PRIVILEGE")
                .and()
                    .httpBasic()
                .and()
//                .sessionManagement()
//                .invalidSessionUrl("/invalidSession.html")
//                .maximumSessions(1).sessionRegistry(sessionRegistry()).and()
//                .sessionFixation().none()
//                .and()
                    .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessHandler(myLogoutSuccessHandler)
                    .invalidateHttpSession(false)
                    .deleteCookies("JSESSIONID")
                    .permitAll()
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), userDetailsService))
                .rememberMe().rememberMeServices(rememberMeServices()).key("theKey");
                }

    // beans

    @Bean
    public DaoAuthenticationProvider authProvider() {
        final CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public RememberMeServices rememberMeServices() {
        CustomRememberMeServices rememberMeServices = new CustomRememberMeServices("theKey", userDetailsService, new InMemoryTokenRepositoryImpl());
        return rememberMeServices;
    }

}
