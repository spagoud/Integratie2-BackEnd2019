package be.kdg.integratie2backend2019.security.config;

import be.kdg.integratie2backend2019.security.customSecurity.ActiveUserStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public ActiveUserStore activeUserStore() {
        return new ActiveUserStore();
    }

}
