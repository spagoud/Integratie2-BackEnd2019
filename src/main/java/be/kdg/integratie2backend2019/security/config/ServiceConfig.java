package be.kdg.integratie2backend2019.security.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "be.kdg.integratie2backend2019.security.service" })
public class ServiceConfig {
}
