package be.kdg.integratie2backend2019.security.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ComponentScan({ "be.kdg.integratie2backend2019.security.task" })
public class SpringTaskConfig {

}
