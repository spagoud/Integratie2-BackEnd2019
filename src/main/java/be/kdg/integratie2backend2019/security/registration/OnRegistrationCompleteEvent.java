package be.kdg.integratie2backend2019.security.registration;

import be.kdg.integratie2backend2019.security.persistence.model.User;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

/**
 * Custom event used for triggering the RegistrationListener
 */
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private final String appUrl;
    private final Locale locale;
    private final User user;

    public OnRegistrationCompleteEvent(final User user, final Locale locale, final String appUrl) {
        super(user);
        this.user = user;
        this.locale = locale;
        this.appUrl = appUrl;
    }

    //

    public String getAppUrl() {
        return appUrl;
    }

    public Locale getLocale() {
        return locale;
    }

    public User getUser() {
        return user;
    }

}

