# Dungeons and dragons

This project represents a digital version of the boardgame: "Dungeons & Dragons: The Fantasy Adventure Board Game" published by Parker Brothers.
This game was made for educational purpose only.

## Getting Started

it is crucial to start the eureka server before the other projects

```
1. start up Eureka Server project
2. start Broadcaster
3. start backend
4. start frontend
```


### Prerequisites

Software needed:

```
For developing the unity build: Unity (unity.com/)
To run end to end test: (katalon.com/)
```

## Built With

* [Spring](https://spring.io/docs) - Backend java framework
* [Gradle](https://gradle.org/) - Dependency Management
* [npm](https://docs.npmjs.com/) - Package management
* [angular](https://angular.io/docs) - Web framework
* [Unity](https://unity.com/) - 3D development platform
* [eureka](https://spring.io/projects/spring-cloud-netflix) - Service discovery


## Authors

* Bram Verlinden
* Dennis De Roover
* Quinten van Assche
* Sam Laureys
* Wannes Verdonck
* Yannick Slegers

## Acknowledgments

* Coaching by Bart Vochten

## unity assets:

JSON .NET For Unity
parentElement, LLC
https://assetstore.unity.com/packages/tools/input-management/11347

Treasure Chest - PBR
Thomas K. Kerff
https://assetstore.unity.com/packages/3d/props/interior/72498

Dungeon Floor Traps
False Wisp Studios
https://assetstore.unity.com/packages/3d/props/interior/77765

Free basic Dungeon
polySoft3D
https://assetstore.unity.com/packages/3d/environments/dungeons/133905

Goblin
PolyNext
https://assetstore.unity.com/packages/3d/characters/humanoids/12131

RPG inventory icons
REXARD
https://assetstore.unity.com/packages/2d/gui/icons/56687

RPG icons
Little Sweet Daemon
https://assetstore.unity.com/packages/2d/gui/icons/89109

Simple Avatar Icons
Little Sweet Daemon
https://assetstore.unity.com/packages/2d/gui/icons/89521

PBR_Orc-Pig
Press START
https://assetstore.unity.com/packages/3d/characters/creatures/109248

Legend Hero Pack
Downrain DC
https://assetstore.unity.com/packages/3d/characters/139707

Warped Fantasy Music Pack
Andrew Isaias
https://assetstore.unity.com/packages/audio/ambient/fantasy/49914

Necromancer GUI
Enemy Giant
https://assetstore.unity.com/packages/2d/gui/366

Fantasy Monster - Skeleton
Teamjoker
https://assetstore.unity.com/packages/3d/characters/humanoids/35635

Creature: Cave Troll
Silvano Junior
https://assetstore.unity.com/packages/3d/characters/creatures/115707

Animated Spider
[prism bucket]
https://assetstore.unity.com/packages/3d/characters/animals/22986

